<?php  	
/* @Author: Charly Parpet - cparpet, IACHI Dimitri - diachi */
require("_all.php"); 

ob_start();
pDoctype("start", array("Entreprises"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

	<div id="entreprise" class="content">
		<div class="situation">
			<h2>ENTREPRISES</h2>
			<h3>Nous formons des professionnels et à ce titre, tous les acteurs de l'économie ont vocation à être nos partenaires.</h3>
		</div>
		<div class="position">
			<i class="fa fa-sitemap blue"></i>
			<a href="Accueil.html">Accueil</a>
			<i class="fa fa-angle-right"></i>
			Entreprises
		</div>
    	
    	<div class="content-text">
			<section class="content-text-entreprises">
				<h4 class="content-text-title"><span>Vous recherchez des compétences en informatique ?</span></h4>
				<div class="content-text-text content-text-points text justify">
					<table><tbody><tr>
						<td><div class="point-circle"><i class="fa fa-university"></i></div></td>
						<td>
							<p>Nous formons des professionnels et à ce titre, tous les acteurs de l'économie ont vocation à <strong>être nos partenaires</strong>. Ce partenariat avec le monde professionnel est pour nous une priorité. De plus, nos étudiants disposent de la formation adaptée pour participer au développement de votre entreprise.</p>
						</td>
					</tr></tbody></table>
				</div>
			</section>

			<section class="content-text-entreprises">
				<h4 class="content-text-title"><span>Les stages </span></h4>
				<div class="content-text-text content-text-points text justify">
					<table><tbody><tr>
						<td>
							<p>Les stages professionnels constituent un des points forts de notre relation avec les entreprises. Pour nos étudiants, le stage est l'occasion de <strong>mettre en pratique les connaissances acquises</strong> et de compléter significativement leur formation. En retour, l'expérience montre que les professionnels peuvent compter sur nos stagiaires en qui ils trouvent le plus souvent des <strong>collaborateurs compétents</strong> et rapidement <strong>opérationnels.</strong></p>
		</div>
							<p>Ils peuvent vous rejoindre en stage :</p>

							<br>

							<ul>
								<li><strong>Les stages de Licence :</strong></li>
								<ul>
									<li>après 2 ans de formation, 10 à 12 semaines de stage entre le 1er août et le 30 octobre</li>
									<li>après 3 ans de formation,...</li>
								</ul>
								<li><strong>Le stage de Master : 6 mois de stage à compter du ???</strong></li>
							</ul>

							<p><i>Si vous êtes intéressés, contactez nous au travers de la page <a href="contact.html">contact</a></i></p>
						</td>
						<td><div class="point-circle"><i class="fa fa-briefcase"></i></div></td>
					</tr></tbody></table>
				</div>
			</section>
			
			<section class="content-text-entreprises">
				<h4 class="content-text-title"><span>Participer à nos enseignements</span></h4>
				<div class="content-text-text text justify">
					<p>Vous pouvez également participer à nos enseignements. Les interventions de professionnels permettent de <strong>renforcer l’ancrage professionnel</strong> des contenus de formation et contribuent au rapprochement IUT-Entreprises.</p>
 
<p>Ces interventions par des vacataires issus du monde professionnel (chefs d’entreprise, techniciens, cadres…), <strong>très appréciées par les étudiants</strong>, représentent une part importante des heures d’enseignement dans nos formations.</p>
 
<p>L’IUT est <strong>toujours en recherche de vacataires professionnels</strong>, si vous souhaitez vous participer dans une formation, prenez contact avec nous au travers de la page <a href="contact.html">contact</a>.</p>
				</div>
			</section>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
            ob_end_clean();
htmlCleaner::make($result);
?>
