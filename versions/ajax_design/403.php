<?php  	
/* @Author: ROBIN Benoit -  */
require("_all.php"); 

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); 
        header('Status : 403 Forbidden');
        header('HTTP/1.0 403 Forbidden');
	?>	
	
    <div id="erreur" class="content">
        <div class="situation">
                <h2>Erreur 403</h2>
                <h3>Une erreur s'est produite durant le chargement de la page demandée.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Erreur 403
        </div>

        <div class="content-text">
            <section class="content-text-erreur">
                <div class="title">403</div>
                <div class="texte">Vos droits d'accès ne permettent pas d'accéder à cette ressource.</div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
