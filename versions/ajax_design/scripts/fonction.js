$(document).ready(function() {
    // Gestion du JS / No-JS :
    $("noscript").remove();
    $(".script").fadeIn(500);
    $(".script-direct").show();
    

	/* HEADER 
	 * Scroll nav & Search Effects
	 */
		// Reduce nav & icon of header on scroll :
		$(function () {
			var $window = $(window);
			$window.scroll(function () {
				if ($window.scrollTop() == 0){
					$('header').removeClass("scroll");
					$('header .scroll').removeClass("scroll");
					$('header').addClass("content");
				}
				else {
					if ($window.scrollTop() > 0) {
						$('header').removeClass("content");
						$('header').addClass("scroll");
						$('header .search').addClass("scroll");
					}
				}
			});
        });
        // Show the search bar on click on search :
        $(function () {
            $('header nav .icon-search').click(function () {
                $('header .search').slideToggle();
            });
        });
        // Show mobile navigation  :
        $(function () {
            $('header .mobile i').click(function () {
                $('header .mobile ul').slideToggle();
            });
        });
		
	/* PAGES 
	 * Général, Accueil, Entreprise, ...
	 */

		/* INIT */
        (function(){
            if($("#init .status").length){
                // delai :
                var theTimeRequired = 1900;
                
                // on change la valeur :
                var val = $("#init .status div").html();
                var val = val.split(";");
                $("#init .status div").html(val[0]);

                // on joue l'effet :
                var position = 0;
                function updateText(){
                    setInterval(function(){
                        $("#init .status div").fadeIn(500);
                        $("#init .status div").html(val[position]);
                    }, theTimeRequired);

                    setTimeout(function(){
                        $("#init .status div").fadeOut(500);
                        position = (position == 1 ? "0" : "1");
                    }, theTimeRequired-500 );
                }
                updateText()
                setInterval(updateText, theTimeRequired );
            }
        })();
	
		/* TEASER */
        (function(){
            // delai :
            var theTimeRequired = 3000;
            
            // titre :
            (function(){
                if($("#teaser .status").length){
                    // on change la valeur :
                    var val = $("#teaser .status").html();
                    var val = val.split(";");
                    $("#teaser .status").html(val[0]);

                    // on définit la taille de la partie description :
                    var hei = $("#teaser .status").height();
                    $("#teaser .status").height(hei);

                    // on joue l'effet :
                    var position = 0;
                    function updateText(){
                        setInterval(function(){
                            $("#teaser .status div").fadeIn(500);
                            $("#teaser .status div").html(val[position]);
                        }, theTimeRequired);

                        setTimeout(function(){
                            $("#teaser .status div").fadeOut(500);
                            position = (position == 1 ? "0" : "1");
                        }, theTimeRequired-500 );
                    }
                    updateText()
                    setInterval(updateText, theTimeRequired );
                }
            })();
            
            // description :
            (function(){
                if($("#teaser .description").length){
                    // on change la valeur :
                    var val = $("#teaser .description").html();
                    var val = val.split(";;");
                    $("#teaser .description").html(val[0]);

                    // on définit la taille de la partie description :
                    var hei = $("#teaser .description").height();
                    $("#teaser .description").height(hei);

                    // on joue l'effet :
                    var position = 0;

                    function updateText(){
                        setInterval(function(){
                            $("#teaser .description p").fadeIn(500);
                            $("#teaser .description p").html(val[position]);
                        }, theTimeRequired);

                        setTimeout(function(){
                            $("#teaser .description p").fadeOut(500);
                            position = (position == 1 ? "0" : "1");
                        }, theTimeRequired-500 );
                    }

                    updateText()
                    setInterval(updateText, theTimeRequired );
                }
            })();
		})();
		countdownManager = {
				// Configuration
				targetTime: new Date('Dec 15 00:00:00 2014'), // Date cible du compte à rebours (00:00:00)
				displayElement: { // Elements HTML où sont affichés les informations
					day:  null,
					hour: null,
					min:  null,
					sec:  null
				},
				
				// Initialisation du compte à rebours (à appeler 1 fois au chargement de la page)
				init: function(){
					// Récupération des références vers les éléments pour l'affichage
					// La référence n'est récupérée qu'une seule fois à l'initialisation pour optimiser les performances
					this.displayElement.day  = jQuery('#countdown_day');
					this.displayElement.hour = jQuery('#countdown_hour');
					this.displayElement.min  = jQuery('#countdown_min');
					this.displayElement.sec  = jQuery('#countdown_sec');
					
					// Lancement du compte à rebours
					this.tick(); // Premier tick tout de suite
					window.setInterval("countdownManager.tick();", 1000); // Ticks suivant, répété toutes les secondes (1000 ms)
				},
				
				// Met à jour le compte à rebours (tic d'horloge)
				tick: function(){
					// Instant présent
					var timeNow  = new Date();
					
					// On s'assure que le temps restant ne soit jamais négatif (ce qui est le cas dans le futur de targetTime)
					if( timeNow > this.targetTime ){
						timeNow = this.targetTime;
					}
					
					// Calcul du temps restant
					var diff = this.dateDiff(timeNow, this.targetTime);
					
					this.displayElement.day.text(  diff.day  );
					this.displayElement.hour.text( diff.hour );
					this.displayElement.min.text(  diff.min  );
					this.displayElement.sec.text(  diff.sec  );
				},
				
				// Calcul la différence entre 2 dates, en jour/heure/minute/seconde
				dateDiff: function(date1, date2){
					var diff = {}                           // Initialisation du retour
					var tmp = date2 - date1;

					tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
					diff.sec = tmp % 60;                    // Extraction du nombre de secondes
					tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
					diff.min = tmp % 60;                    // Extraction du nombre de minutes
					tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
					diff.hour = tmp % 24;                   // Extraction du nombre d'heures
					tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
					diff.day = tmp;

					return diff;
				}
			};
			countdownManager.init();
		
		/* GENERAL */
			// content-text-list-points
			$(function () {
				$('.content-text-list-points-title').click(function () {
					if($(this).children("i").html() == "+"){
						$(this).children("i").html("-");
                        $(this).addClass("active");
					} else {
                        $(this).children("i").html("+");
                        $(this).removeClass("active");
					}
					
					$(this).next(".content-text-list-points-content").slideToggle();
				});
			});
			// hide notification in content
			$(function () {
				$('.notify').click(function () {
					$(this).slideUp();
				});
			});
			
		/* MISSION */
			// Recap mission
            $(function () {
                $("#missions .missions-liste li").click(function() {
                    $(this).find(".contente").slideToggle();
                });
            });
			// vide la taxtearea
            $(function () {
                $("#missions .content-text-form .element textarea").click(function() {
                    $("#missions .content-text-form .element textarea").val("");
                });
            });

		/* CONTACT */
			// vide la taxtearea
            $(function () {
                $("#contact .content-text-form .element textarea").click(function() {
                    $("#contact .content-text-form .element textarea").val("");
                });
            });

    // AJAX
    var root = '/u-hochiminh.com/versions/ajax_design/'
    var isHistoryAvailable = true;
    if(typeof history.pushState === 'undefined'){
        isHistoryAvailable = false;
    }

    $("a").click(function(event){
        // Problème avec la page Accueil.php ?
        if (document.location.pathname != root)
        event.preventDefault();
        var $a = $(this);
        var url = $a.attr("href");
        var id = url.split('.');
        if(isHistoryAvailable){
             history.pushState({key : url}, 'titre', id[0]);
        }
        ajaxLoad(url);
    });

    
    function ajaxLoad(url){
        $.ajax({
            url : url,
            type: 'GET',
            dataType: 'html',

            success : function(data, statut){
                var content = $(data).find("header").next(".content").html();
                var newId = $(data).find("header").next(".content").attr("id");
                $("header").next(".content").fadeOut(500, function() {
                   $("header").next(".content").empty().attr("id", newId).html(content).fadeIn();
                });

            },
            error : function(resultat, statut, erreur){
                //A FAIRE
            },
            complete : function(resultat, statut){
                //A FAIRE
        	}
		});
	}

    window.onpopstate = function(event){
        if (event.state == null) {
            ajaxLoad('Accueil.php');
        }else{
            ajaxLoad(event.state.key);
        }
    }

//Scrolling fluide 
    //The first variable is the window object.
    //We need it both to listen it for scroll event, and to animate it.
    // The scrollTime variable will be used to define the time of the animation and the scrollDistance to calculate the amount of scroll.
    var $window = $(window);
    var scrollTime = 0.4;
    var scrollDistance = 300;
	var is_OSX = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)?true:false;
	var is_iOS = navigator.platform.match(/(iPhone|iPod|iPad)/i)?true:false;
	/*var is_Mac = navigator.platform.toUpperCase().indexOf('MAC')>=0;
	var is_iPhone = navigator.platform=="iPhone";
	var is_iPod = navigator.platform=="iPod";
	var is_iPad = navigator.platform=="iPad";*/
	
	if(!is_OSX | !is_iOS){
		//Next, we have to listen for mouse scroll event:
		$window.on("mousewheel DOMMouseScroll", function(event){
			//We use event.preventDefault() to prevent default scrolling. What we need is the event delta – it will tell us how much the user scrolled.
			event.preventDefault(); 

			//Once we know this, we’re able to calculate the new window position:
			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			var finalScroll = scrollTop - parseInt(delta*scrollDistance);

			//Let’s animate the window to this new position:
			TweenMax.to($window, scrollTime, {
				scrollTo : { y: finalScroll, autoKill:true },
					ease: Power1.easeOut,
					overwrite: 5                            
			});
		});
    }

});

