<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

    <div id="formations" class="content">
        <div class="situation">
            <h2>FORMATIONS</h2>
            <h3>Découvrez toutes nos formations universitaires aux standards internationaux en matière d'enseignement.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Formations
        </div>

        <div class="content-text">
            <section class="content-text-formations-slider">
                <img src="images/formations-amphi.png" alt="Amphithéâtres">
                <ul>
                    <li><a href="L1.html">Licence 1</a></li>
                    <li><a href="L2.html">Licence 2</a></li>
                    <li><a href="L3.html">Licence 3</a></li>
                    <li class="expand"><a href="M1.html">Master 1</a></li>
                    <li class="expand"><a href="M2.html">Master 2</a></li>
                </ul>
            </section>

            <section class="content-text-formations-presentation">
                <h4 class="content-text-title"><span>Les formations</span></h4>
                <div class="content-text-text text alinea justify">
                    <p>Le Vietnam connaît actuellement un développement économique considérable dans tous les domaines professionnels.
                        Dans ce contexte, les technologies de l’information constituent un levier essentiel dans la modernisation de l’économie vietnamienne.
                        Les entreprises installées au Vietnam, qu’elles soient publiques ou privées, vietnamiennes ou internationales, ressentent aujourd’hui un besoin crucial d’informaticiens de haut niveau. </p>
                    <p>Le PUF Ho Chi Minh a pour principale activité d'animer des formations universitaires françaises délocalisées au Vietnam. Toutes les formations suivent les principes suivants :</p>
                    <ul>
                        <li>Elles sont <strong>identiques</strong> à des formations dispensées en France, et lient la théorie et la pratique.</li>
                        <li>Elles sont assurées à 50% au moins par des enseignants de des universités françaises partenaires, ou par des professionnels qui lui sont associés.</li>
                        <li>Elles mènent à un diplôme français du schéma LMD, <strong>reconnu partout en Europe</strong> et par équivalence dans de nombreux pays.</li>
                    </ul>
                </div>
                <div class="content-text-list-points text">
                    <h5 class="content-text-list-points-title click"><i>+</i> POURQUOI UNE LICENCE INFORMATIQUE</h5>
                    <div class="content-text-list-points-content alinea justify no-display">
                        <p>L’informatique inonde notre quotidien : mails, réseaux sociaux, jeux vidéo, bureautique, NFC, systèmes embarqués, etc ... De plus, la licence, que nous dispensons, donne les bases d’une compétence informatique générale : conception, développement , innovation ...</p>
                    </div>
                    <h5 class="content-text-list-points-title click"><i>+</i> POURQUOI AU PUF</h5>
                    <div class="content-text-list-points-content alinea justify no-display">
                        <p>Vous obtiendrez simultanément deux diplômes en informatique : une licence de l’Université Pierre et Marie Curie (Paris VI Sorbonne) et le DUT de l’université de Bordeaux. Encore les dimplômes sont internationalement reconnus, théoriques et pratiques.</p>
                        <p>Ces diplômes vous offrent une insertion professionnelle immédiate sur le marché du travail. Mais, si vous préférez, vous pouvez poursuivre en Master en France, en Europe, aux USA ou au Vietnam.</p>
                    </div>
                    <h5 class="content-text-list-points-title click"><i>+</i> ORGANISATION DES FORMATIONS</h5> 
                    <div class="content-text-list-points-content justify no-display">
                        <p>Les formations sont organisés selon :</p>
                        <ul>
                            <li><strong>Condition d’accès :</strong> être titulaire du baccalauréat.</li>
                            <li><strong>Durée et rythme de la formation :</strong> 3 ans réliartis en 6 semestres.</li>
                            <li><strong>Les matières enseignées :</strong> algorithmique, programmation et programmation objets, conception objets, bases de données, systèmes d’exploitation, réseaux, mathématiques, management, expression et communication en français et en anglais, ...</li>
                            <li><strong>Projets et stages :</strong> les 6 semestres de la formation permettent de réaliser de nombreux projets et 2 stages (semestres 4 et 6).</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
