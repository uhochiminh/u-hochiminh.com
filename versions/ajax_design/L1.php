<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    <div id="formation" class="content">
        <div class="situation">
            <h2>LICENCE 1</h2>
            <h3>Découvrez notre licence universitaire de niveau 1</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
			<a href="Formations.html">Formations</a>
            <i class="fa fa-angle-right"></i>
			L1
        </div>
        
        <div class="content-text">
            <section class="content-text-formations-presentation">
                <div class="content-text-text text justify">
                    <img src="images/formation-help.png" />
                    <p>
                        La licence d'informatique s'adresse aux étudiants intéressés par l'informatique sous tous ses aspects. 
                        Elle vise à en donner les bases tant théoriques que pratiques. </p>
                    <p>
                        Les diplômés de cette licence seront immédiatement opérationnels dans des métiers nombreux et variés ou bien pourront continuer des études en master d'informatique.</p>
                    <p>
                        <strong>Trois diplômes</strong> peuvent être obtenus au sein du cycle Licence d'informatique :
                        un DUT Informatique, une Licence Professionnelle Systèmes Informatiques et Logiciels, une Licence de Sciences et Technologies mention Informatique.
                    </p>
                </div>
                
                <div class="clear"></div>
                
                <div class="content-text-list-points text">
                    <h5 class="content-text-list-points-title click"><i>+</i> POURQUOI UNE LICENCE INFORMATIQUE</h5>
                    <div class="content-text-list-points-content justify no-display">
                                <ul>
                                    <li>L’informatique inonde notre quotidien : mails, réseaux sociaux, jeux vidéo, bureautique, NFC, systèmes embarqués, etc...</li>
                                    <li>La licence donne les bases d’une compétence informatique générale : conception, développement, innovation... </li>
                                </ul>
                    </div>
                    <h5 class="content-text-list-points-title click"><i>+</i> POURQUOI AU PUF</h5>
                    <div class="content-text-list-points-content justify no-display">
                        <ul>
                            <li>Vous obtenez simultanément deux diplômes en informatique : licence de l’Université Pierre et Marie Curie (Paris VI Sorbonne) et DUT de l’université de Bordeaux.</li>
                            <li>Internationalement reconnus, théoriques et pratiques, ces diplômes vous offrent une insertion professionnelle immédiate. </li>
                            <li>Si vous préférez, vous pouvez poursuivre en Master en France, en Europe, aux USA ou au Vietnam.</li>
                        </ul>
                    </div>
                    <h5 class="content-text-list-points-title click"><i>+</i> ORGANISATION DES FORMATIONS</h5>
                    <div class="content-text-list-points-content justify no-display">
                        <p>Condition d’accès : être titulaire du baccalauréat. </p>
                        <p>Durée et rythme de la formation : 3 ans répartis en 6 semestres.</p>
                        <p>Les matières enseignées : algorithmique, programmation et programmation objets, conception objets, bases de données, systèmes d’exploitation, réseaux, mathématiques, management, expression et communication en français et en anglais, ...
                        <p>Projets et stages : les 6 semestres de la formation permettent de réaliser de nombreux projets et 2 stages (semestres 4 et 6).</p>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
