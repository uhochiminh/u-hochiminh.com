<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 


ob_start();
	pDoctype("start", array("Accueil"));
	?>
	
	<div id="teaser">
		<table valign="middle" align="center" border="0" height="100%" width="100%">
			<tbody>
				<tr height="100%">
					<td align="center" height="100%" valign="middle" width="100%">
						<div class="center">
							<img src="images/logo_teasing-init.png" alt="<?php echo $GLOBALS["site"]["nom"]; ?>" title="<?php echo $GLOBALS["site"]["nom"]; ?>" />

							<div class="status"><div>/ Coming Soon /;/ Arrive bientot /</div></div>
							
							<div class="description"><p>The French university center (PUF) from <strong>Ho Chi Minh IT</strong> specialty which prepare five 	
formation as computer and information systems expert, is coming soon to your screens.;;Le pôle universitaire français (PUF) de <strong>Ho Chi Minh</strong> spécialité INFO qui prépare 5 formations au titre d'expert en informatique et systèmes d'information arrive bientôt sur vos écrans.</p></div>

							<div class="compteur">
								<span id="countdown_day" >--</span>j
								<span id="countdown_hour">--</span>h
								<span id="countdown_min" >--</span>m
								<span id="countdown_sec" >--</span>s
							</div>

							<div class="social">
								<a href="https://www.facebook.com/uhochiminh"><i class="fa fa-facebook-square"></i></a>
								<a href="https://twitter.com/u_hochiminh"><i class="fa fa-twitter"></i></a>
							</div>

							<div class="copyright">Designed & Developed by <strong>IUT Bordeaux 1 - Groupe 3</strong>
							<br/>Copyright  &copy; 2014 - All Right Reserved - u-HoChiMinh.com</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	

	<?php
	pDoctype("end"); 


$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>