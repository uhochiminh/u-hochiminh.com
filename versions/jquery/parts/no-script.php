<noscript>
    <table valign="middle" align="center" border="0" height="100%" width="100%">
        <tbody>
            <tr height="100%">
                <td align="center" height="100%" valign="middle" width="100%">
                    <a href="#"><img src="images/logo_teasing-init.png" alt="<?php echo $GLOBALS["site"]["nom"]; ?>" title="<?php echo $GLOBALS["site"]["nom"]; ?>" /></a>

                    <div class="erreur">
                        <p>Your browser does not support javascript, thank you kindly <strong>update your browser</strong> to view the site's features .</p>
                        <p>Votre navigateur ne supporte pas le javascript, merci de bien vouloir <strong>mettre à jour votre navigateur</strong> afin d'accèder aux  fonctions du site.</p>
                    </div>

                    <div class="social">
                        <a href="#facebook"><i class="fa fa-facebook-square"></i></a>
                        <a href="#twitter"><i class="fa fa-twitter"></i></a>
                    </div>

                    <div class="copyright">Designed & Developed by <strong>IUT Bordeaux 1 - Groupe 3</strong>
                    <br/>Copyright  &copy; 2014 - All Right Reserved - u-HoChiMinh.com</div>
                </td>
            </tr>
        </tbody>
    </table>
</noscript>