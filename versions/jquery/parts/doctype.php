<?php
/**
 * Doctype
 * Intégrè facilement le Doctype à vos pages.
 *
 * @Author: IACHI Dimitri - diachi
 */

function pDoctype($type, $args = null)	{
	return ($type == "start" ? doctype_start($args[0]) : doctype_end() );
}


// FUNCTIONS :
	// Gère le Doctype de début de page :
	function doctype_start($title){
		?>
		<!DOCTYPE html>
		<html lang="fr">
		<head>
		    <meta charset="utf-8" />
		    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		    <meta http-equiv="Content-language" content="fr-FR" />
		    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		    <title><?php echo $title." - ".$GLOBALS["site"]["nom"]; ?></title>
	    
		    <meta name="description" content="<?php echo $GLOBALS["site"]["description"]; ?>" />
		    <meta name="keywords" content="<?php echo $GLOBALS["site"]["keywords"]; ?>" />
		    <meta name="author" content="<?php echo $GLOBALS["site"]["author"]; ?>">
		    <meta name="category" content="<?php echo $GLOBALS["site"]["category"]; ?>" />
		    <meta name="reply-to" content="<?php echo $GLOBALS["site"]["reply-to"]; ?>" />
		    <meta name="copyright" content="<?php echo $GLOBALS["site"]["copyright"]; ?>" />
		    <meta name="robots" content="<?php echo $GLOBALS["site"]["robots"]; ?>" />
		    <meta name="domain" content="<?php echo $GLOBALS["site"]["domaine"]; ?>" />

		    <meta name="twitter:card" content="summary_large_image">
		    <meta name="twitter:domain" content="<?php echo $GLOBALS["site"]["domaine"]; ?>">
		    <meta name="twitter:site" content="<?php echo $GLOBALS["site"]["twitter"]; ?>" />
		    <meta name="twitter:title" content="<?php echo $title." - ".$GLOBALS["site"]["nom"]; ?>" />
		    <meta name="twitter:url" content="<?php echo $GLOBALS["site"]["url"]; ?>" />
		    <meta name="twitter:description" content="<?php echo $GLOBALS["site"]["description"]; ?>" />
		    <meta name="twitter:image:src" content="" />

		    <meta property="og:title" content="<?php echo $title." - ".$GLOBALS["site"]["nom"]; ?>" />
		    <meta property="og:description" content="<?php echo $GLOBALS["site"]["description"]; ?>" />
		    <meta property="og:type" content="<?php echo $GLOBALS["site"]["category"]; ?>" />
		    <meta property="og:url" content="<?php echo $GLOBALS["site"]["url"]; ?>" />
		    <meta property="og:site_name" content="<?php echo $GLOBALS["site"]["domaine"]; ?>" />
		    <meta property="og:image" content="" />

		    <!-- Add to favicon on Desktop -->
		    <link rel="shortcut icon" href="favicon.ico">

		    <!-- Add to homescreen for Chrome on Android -->
		    <meta name="mobile-web-app-capable" content="yes" />
		    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

		    <!-- Add to homescreen for Safari on iOS -->
		    <meta name="apple-mobile-web-app-capable" content="yes" />
		    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
		    <meta name="apple-mobile-web-app-title" content="Web Starter Kit" />
		    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

		    <!-- Tile icon for Win8 (144x144 + tile color) -->
		    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png" />
		    <meta name="msapplication-TileColor" content="#3372DF" />

		    <!-- Essential JS - NOT APPEND ALL -->
		    <script type="text/javascript" src="scripts/jquery-2.1.1.js"></script>
		</head>
		<body>
		<?php
	}

	// Gère le Doctype de fin de page :
	function doctype_end(){
		?>	
			<!-- Classic CSS -->
			<link href='http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
			<link href='styles/font-awesome.css' rel='stylesheet' type='text/css'>
			<link href='styles/reset.css' rel='stylesheet' type='text/css'>
			<link href='styles/style.css' rel='stylesheet' type='text/css'>
			<link href='styles/style-mobile.css' rel='stylesheet' type='text/css'>
			<link href='styles/style-tablette.css' rel='stylesheet' type='text/css'>
			<link href='styles/animate.css' rel='stylesheet' type='text/css'>
			<!-- Classic JS -->
		   	<script type="text/javascript" src="scripts/TweenMax.min.js"></script>
		   	<script type="text/javascript" src="scripts/ScrollToPlugin.min.js"></script>
		   	<script type="text/javascript" src="scripts/placeholders.js"></script>
			<script type="text/javascript" src="scripts/fonction.js"></script>
			<script type="text/javascript" src="scripts/slider.js"></script>
            <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
		    <!-- Google Analytics -->
		    <script>
		      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		      ga('create', 'UA-55710805-1', 'auto');
		      ga('send', 'pageview');
			</script>
		</body>
		</html>
		<?php
	}
?>
