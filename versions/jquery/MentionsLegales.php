<?php  	
/* @Author: ETCHEVERRY Laetitia letchv4 */
require("_all.php"); 


ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

    <div id="mention_legales" class="content">
        <div class="situation">
            <h2>MENTIONS LEGALES</h2>
            <h3>Vous trouverez ici toutes les informations concernant la création de ce site ainsi que son contexte.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Mentions Légales
        </div>
        
        <div class="content-text">
            <section class="content-text-text text alinea justify">
                <p>Le présent site est édité par Mme Chritine UNY professeur à l'IUT de l'université de Bordeaux. Néanmoins ce site est la propriété du département Informatique du PUF (Pôle Universitaire Français) d'Ho Chi Minh.</p> 

                <p>Ce site à été développer, dans le cadre d'un projet de formation, par <strong>IACHI Dimitri Fabien</strong>, <strong>ETCHEVERRY Laetitia</strong>, <strong>GARCIA Johan</strong>, <strong>PARPET Charles-Eddy</strong> et <strong>ROBIN Benoit</strong>, étudiants en Licence Professionnelle DAWIN à l'IUT de l'université de Bordeaux.</p>

                <p>Tous les contenus du site sont couverts par la <a href="http://creativecommons.fr/licences/" title="Lien menant au site sur la licence creative commons">licence creative commons</a> qui permet :</p>
                <ul>
                    <li> de partager et faciliter l’utilisation de leur création par d’autres </li>
                    <li> d'autoriser gratuitement la reproduction et la diffusion (sous certaines conditions) </li>
                    <li> d'accorder plus de droits aux utilisateurs en complétant le droit d’auteur qui s’applique par défaut </li>
                    <li> de faire évoluer une oeuvre et enrichir le patrimoine commun (les biens communs ou Commons) </li>
                    <li> d'économiser les coûts de transaction </li>
                    <li> de légaliser le peer to peer de leurs œuvres </li>
                </ul>

                <p></p>

                <p><strong>U-HoChiMinh</strong>, ainsi que sa représentation graphique, et <a href="http://www.u-hochiminh.com/">www.u-hochiminh.com</a> sont la propriété du PUF d'Ho Chi Minh.</p>

                <p>Les codes sources utilisés pour la création de ce site internet et l’architecture du programme sont couverts par la législation française et internationale sur le droit d’auteur et la propriété intellectuelle. Tous les droits de reproduction sont réservés. Néanmoins toute reproduction du site, même partielle, est interdite sauf autorisation expresse des responsables de ce site et de ces autheurs. En cas de violation de ces dispositions la personne, morale ou physique, se soumet aux sanctions pénales et civiles prévues par la loi française.</p>

                <p>Conformément à la réglementation en vigueur depuis le 10/07/2006, la déclaration auprès de la CNIL, d’un site Internet non marchand et/ou institutionnel et/ou ne récoltant pas des données à risque, n’est pas obligatoire. Néanmoins, le PUF d'Ho Chi Minh s’engage à respecter la vie privée de ses internautes conforment à la législation française sur la protection de la vie privée et des libertés individuelles (loi n° 78-17 du 6 janvier 1978) .</p>

                <p>Ce site ne collecte donc aucune information personnel. Toute fois en application de l’article 34 de la loi informatique et libertés du 1er août 2000, toute personne ayant fait l’objet d’une collecte d’information dispose d’un droit d’accès, de rectification et d’opposition aux données personnelles la concernant soit par courrier (15, rue Naudet CS 10207 33175 Gradignan Cedex, France) ou par mail à l’adresse <a href="mailto:contact@u-HoChiMinh.com">contact@u-hochiminh.com</a>.</p>
            </section>

            <section class="content-text-text">
                <div class="element">
                    <div class="content-title"><span>Propriétaire du site</span></div>
                    <div class="content-info">Département Informatique du Pôle Universitaire Français<br/>Quartier 6, Linh Trung Ward,<br/>Thu Duc District,<br/>Ho Chi Min-Ville, Vietnam<br/><br/>Téléphone: <a href="tel:+0837242169">(08) 37 242 169</a><br/>Fax: <a href="tel:+0837242169">(08) 37 242 166</a></div>
                </div>
                <div class="element">
                    <div class="content-title"><span>Hébergeur</span></div>
                    <div class="content-info">I.U.T. Bordeaux 1<br/>15 Rue de Naudet<br/>33175 Gradignan</div>
                </div>
                <div class="element">
                    <div class="content-title"><span>Conception du site </span></div>
                    <div class="content-info">IACHI Dimitri<br/>ETCHEVERRY Laetitia<br/>GARCIA Johan<br/>PARPET Charles-Eddy<br/>ROBIN Benoit</div>
                </div>
            </section>

            <div class="clear"></div>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
