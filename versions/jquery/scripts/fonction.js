$(document).ready(function() {
	
	// ================================================================================ //
	// GENERAL 																			//
	// ================================================================================ //
    
    // Gestion du JS / No-JS :
    $(function () {
	    $("noscript").remove();								// Suppression du noScript si le JS est activé
	    $(".script").fadeIn(500);							// Effet d'apparition sur certianes page (langue, teaser)
	    $(".script-direct").show();							// Affichage direct pour les autres pages
    });
    
    // Gestion de l'AJAX pour pages affichages :
    $(function () {
    	// parametrage des urls des liens :
    	var root = '';   	//example  '/u-hochiminh.com/versions/jquery/'
    	
		if(root != ''){
			// initialisation de l'historique :
			var isHistoryAvailable = true;
			if(typeof history.pushState === 'undefined'){ isHistoryAvailable = false; }
			window.onpopstate = function(event){
				if (event.state == null) { ajaxLoad('Accueil.php'); }
				else{ ajaxLoad(event.state.key); }
			};
			
			// lors d'un click sur un lien :
			$("a").click(function(event){
				// Problème avec la page Accueil.php mais avec l'integration du PHP sous LARAVEL pas ce soucis là car les routes sont gérer différament.
				
				var url = $(this).attr("href");
				var id = url.split('.');
				
				if (!(url.indexOf("admin") > -1) && !(url.indexOf("Accueil") > -1)){
					if(isHistoryAvailable){ history.pushState({key : url}, 'titre', id[0]+".html"); }
					ajaxLoad(url);
					event.preventDefault();
				}
				else{
				console.log("sqsd");
				}
			});
		
			// fonction d'ajax :
			function ajaxLoad(url){
				$.ajax({
					url : url,
					type: 'GET',
					dataType: 'html',
		
					success : function(data, statut){
						var content = $(data).find("header").next(".content").html();
						var newId = $(data).find("header").next(".content").attr("id");
						$("header").next(".content").fadeOut(500, function() {
						   $("header").next(".content").empty().attr("id", newId).html(content).fadeIn();
						});
		
					},
					error : function(resultat, statut, erreur){
						//A FAIRE
					},
					complete : function(resultat, statut){
						//A FAIRE
					}
				});
			}
		}
    });
    
    // Gestion de l'AJAX pour pages d'administration :
    $(function () {
    	// plus tard
    });
	
	// Gestion du Scroll sur PC :
    $(function () {
		var $window = $(window);
		var scrollTime = 0.4;
		var scrollDistance = 300;
		var is_OSX = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)?true:false;
		var is_iOS = navigator.platform.match(/(iPhone|iPod|iPad)/i)?true:false;
		
		if(!is_OSX | !is_iOS){
			// Si on scroll
			$window.on("mousewheel DOMMouseScroll", function(event){
				// on annule le scroll initial par défaut :
				event.preventDefault(); 

				// On calcul la nouvelle position dans la fenaitre :
				var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
				var scrollTop = $window.scrollTop();
				var finalScroll = scrollTop - parseInt(delta*scrollDistance);

				// On fais appel au plugins de TweenMax pour se déplacer à la nouvelle postion avec un petit effet :
				TweenMax.to($window, scrollTime, {
					scrollTo : { y: finalScroll, autoKill:true },
						ease: Power1.easeOut,
						overwrite: 5                            
				});
			});
		}
    });
	
	// ================================================================================ //
	// HEADER 																			//
	// ================================================================================ //
	
	// Réduction de la navbar & des icones de l'en-tête quand on scroll :
	$(function () {
    	if (!navigator.userAgent.match(/(android|iphone|ipad|blackberry|symbian|symbianos|symbos|netfront|model-orange|javaplatform|iemobile|windows phone|samsung|htc|opera mobile|opera mobi|opera mini|presto|huawei|blazer|bolt|doris|fennec|gobrowser|iris|maemo browser|mib|cldc|minimo|semc-browser|skyfire|teashark|teleca|uzard|uzardweb|meego|nokia|bb10|playbook)/gi)) {
            if (((screen.width >= 480) && (screen.height >= 800)) || ((screen.width >= 800) && (screen.height >= 480)) || navigator.userAgent.match(/ipad/gi)) {
                var $window = $(window);
                $window.scroll(function () {
                    if ($window.scrollTop() == 0){
                        $('header').removeClass("scroll");
                        $('header .scroll').removeClass("scroll");
                        $('header').addClass("content");
                    }
                    else {
                        if ($window.scrollTop() > 0) {
                            $('header').removeClass("content");
                            $('header').addClass("scroll");
                            $('header .search').addClass("scroll");
                        }
                    }
                });
			}
        }
    });
    
    // Affiche la search bar et met le focus sur le champs de recherche :
    $(function () {
        $('header nav .icon-search').click(function () {
            $('header .search').slideToggle();
            setTimeout(function() { $('header .search input').focus(); }, 600);
        });
    });
    
    // Action sur l'icon menu mobile pour afficher le menu :
    $(function () {
        $('header .mobile i').click(function () {
            $('header .mobile ul').slideToggle();
        });
    });
		

	// ================================================================================ //
	// PAGES 																			//
	// ================================================================================ //

		/* INIT */
        $(function(){
            if($("#init .status").length){
                // delai :
                var theTimeRequired = 1900;
                
                // on change la valeur :
                var val = $("#init .status div").html();
                var val = val.split(";");
                $("#init .status div").html(val[0]);

                // on joue l'effet :
                var position = 0;
                function updateText(){
                    setInterval(function(){
                        $("#init .status div").fadeIn(500);
                        $("#init .status div").html(val[position]);
                    }, theTimeRequired);

                    setTimeout(function(){
                        $("#init .status div").fadeOut(500);
                        position = (position == 1 ? "0" : "1");
                    }, theTimeRequired-500 );
                }
                updateText();
                setInterval(updateText, theTimeRequired );
            }
        });
	
		/* TEASER */
        $(function(){
            // delai :
            var theTimeRequired = 3000;
            
            // titre :
            (function(){
                if($("#teaser .status").length){
                    // on change la valeur :
                    var val = $("#teaser .status").html();
                    var val = val.split(";");
                    $("#teaser .status").html(val[0]);

                    // on définit la taille de la partie description :
                    var hei = $("#teaser .status").height();
                    $("#teaser .status").height(hei);

                    // on joue l'effet :
                    var position = 0;
                    function updateText(){
                        setInterval(function(){
                            $("#teaser .status div").fadeIn(500);
                            $("#teaser .status div").html(val[position]);
                        }, theTimeRequired);

                        setTimeout(function(){
                            $("#teaser .status div").fadeOut(500);
                            position = (position == 1 ? "0" : "1");
                        }, theTimeRequired-500 );
                    }
                    updateText();
                    setInterval(updateText, theTimeRequired );
                }
            })();
            
            // description :
            (function(){
                if($("#teaser .description").length){
                    // on change la valeur :
                    var val = $("#teaser .description").html();
                    var val = val.split(";;");
                    $("#teaser .description").html(val[0]);

                    // on définit la taille de la partie description :
                    var hei = $("#teaser .description").height();
                    $("#teaser .description").height(hei);

                    // on joue l'effet :
                    var position = 0;

                    function updateText(){
                        setInterval(function(){
                            $("#teaser .description p").fadeIn(500);
                            $("#teaser .description p").html(val[position]);
                        }, theTimeRequired);

                        setTimeout(function(){
                            $("#teaser .description p").fadeOut(500);
                            position = (position == 1 ? "0" : "1");
                        }, theTimeRequired-500 );
                    }

                    updateText();
                    setInterval(updateText, theTimeRequired );
                }
            })();
		
			// COMPTEUR :
			countdownManager = {
				// Configuration
				targetTime: new Date('Jan 13 00:00:00 2015'), // Date cible du compte à rebours (00:00:00)
				displayElement: { // Elements HTML où sont affichés les informations
					day:  null,
					hour: null,
					min:  null,
					sec:  null
				},
				
				// Initialisation du compte à rebours (à appeler 1 fois au chargement de la page)
				init: function(){
					// Récupération des références vers les éléments pour l'affichage
					// La référence n'est récupérée qu'une seule fois à l'initialisation pour optimiser les performances
					this.displayElement.day  = jQuery('#countdown_day');
					this.displayElement.hour = jQuery('#countdown_hour');
					this.displayElement.min  = jQuery('#countdown_min');
					this.displayElement.sec  = jQuery('#countdown_sec');
					
					// Lancement du compte à rebours
					this.tick(); // Premier tick tout de suite
					window.setInterval("countdownManager.tick();", 1000); // Ticks suivant, répété toutes les secondes (1000 ms)
				},
				
				// Met à jour le compte à rebours (tic d'horloge)
				tick: function(){
					// Instant présent
					var timeNow  = new Date();
					
					// On s'assure que le temps restant ne soit jamais négatif (ce qui est le cas dans le futur de targetTime)
					if( timeNow > this.targetTime ){
						timeNow = this.targetTime;
					}
					
					// Calcul du temps restant
					var diff = this.dateDiff(timeNow, this.targetTime);
					
					this.displayElement.day.text(  diff.day  );
					this.displayElement.hour.text( diff.hour );
					this.displayElement.min.text(  diff.min  );
					this.displayElement.sec.text(  diff.sec  );
				},
				
				// Calcul la différence entre 2 dates, en jour/heure/minute/seconde
				dateDiff: function(date1, date2){
					var diff = {};                           // Initialisation du retour
					var tmp = date2 - date1;

					tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
					diff.sec = tmp % 60;                    // Extraction du nombre de secondes
					tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
					diff.min = tmp % 60;                    // Extraction du nombre de minutes
					tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
					diff.hour = tmp % 24;                   // Extraction du nombre d'heures
					tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
					diff.day = tmp;

					return diff;
				}
			};
			
			countdownManager.init();
		});
		
		/* AUTRES */
			// Gestion des listes déroulantes : 
			$(function () {
				$('.content-text-list-points-title').click(function () {
					if($(this).children("i").html() == "+"){
						$(this).children("i").html("-");
	                    $(this).addClass("active");
					} else {
	                    $(this).children("i").html("+");
	                    $(this).removeClass("active");
					}
					
					$(this).next(".content-text-list-points-content").slideToggle();
				});
			});
			
			// Cachement des notification des formulaires :
			$(function () {
			$('.notify').click(function () {
				$(this).slideUp();
			});
		});
			
		/* MISSION */
			// Affichage d'une missions en cliquant sur son en-tête : 
            $(function () {
                $("#missions .missions-liste li").click(function() {
                    $(this).find(".contente").slideToggle();
                    $(this).find("i").toggleClass("fa-plus fa-minus");
                });
            });

});
