<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 


ob_start();
pDoctype("start", array("Infos Pratiques"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

	<div id="infos_pratiques" class="content">
		<div class="situation">
			<h2>Infos Pratiques</h2>
			<h3>Vous trouverez ici toutes les informations pratiques à la vie en tant qu'étudiants (calendrier universitaire, accès aux restaurant U, liens utiles ...)</h3>
		</div>
		<div class="position">
			<i class="fa fa-sitemap blue"></i>
			<a href="Accueil.html">Accueil</a>
			<i class="fa fa-angle-right"></i>
			Infos Pratiques
		</div>

		<div class="content-text">
			<article class="content-text-post">
				<img src="images/infospratiques-posts-img.png" class="post-img left" />
				<div class="post-content right">
					<h4 class="content-text-title-left"><span>Semaine du livre</span></h4>
					<div class="info">Poster le 24/10/2014 par Christine UNY</div>
					<div class="content-text-text text justify">
						<p>Praesent commodo cursus magna, vel <strong>scelerisque nisl consectetur et</strong> mecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum.</p>
						<p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. <strong>Integer posuere erat</strong> a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur <strong>est at lobortis</strong>. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
					</div>
					<div class="content-text-button">
						<a href="InfoPratique.php" class="read_more right">Lire la suite</a>
					</div>
				</div>
                
                <div class="clear"></div>
			</article>

			<article class="content-text-post">
				<img src="images/infospratiques-posts-img.png" class="post-img left" />
				<div class="post-content right">
					<h4 class="content-text-title-left"><span>Semaine du livre</span></h4>
					<div class="info">Poster le 24/10/2014 par Christine UNY</div>
					<div class="content-text-text text justify">
						<p>Praesent commodo cursus magna, vel <strong>scelerisque nisl consectetur et</strong> mecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum.</p>
						<p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. <strong>Integer posuere erat</strong> a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur <strong>est at lobortis</strong>. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
					</div>
					<div class="content-text-button">
						<a href="InfoPratique.php" class="read_more right">Lire la suite</a>
					</div>
				</div>
                
                <div class="clear"></div>
			</article>

			<article class="content-text-post">
				<img src="images/infospratiques-posts-img.png" class="post-img left" />
				<div class="post-content right">
					<h4 class="content-text-title-left"><span>Semaine du livre</span></h4>
					<div class="info">Poster le 24/10/2014 par Christine UNY</div>
					<div class="content-text-text text justify">
						<p>Praesent commodo cursus magna, vel <strong>scelerisque nisl consectetur et</strong> mecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum.</p>
						<p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. <strong>Integer posuere erat</strong> a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur <strong>est at lobortis</strong>. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
					</div>
					<div class="content-text-button">
						<a href="InfoPratique.php" class="read_more right">Lire la suite</a>
					</div>
				</div>
                
                <div class="clear"></div>
			</article>

			<div class="clear"></div>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
