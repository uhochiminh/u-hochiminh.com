$(document).ready(function() {
    // Gestion du JS / No-JS :
    $("noscript").remove();
    $(".script").fadeIn(500);
    $(".script-direct").show();
    
	/* HEADER 
	 * Scroll nav & Search Effects
	 */
		// Reduce nav & icon of header on scroll (only on computer):
        $(function () {
        	if (!navigator.userAgent.match(/(android|iphone|ipad|blackberry|symbian|symbianos|symbos|netfront|model-orange|javaplatform|iemobile|windows phone|samsung|htc|opera mobile|opera mobi|opera mini|presto|huawei|blazer|bolt|doris|fennec|gobrowser|iris|maemo browser|mib|cldc|minimo|semc-browser|skyfire|teashark|teleca|uzard|uzardweb|meego|nokia|bb10|playbook)/gi)) {
	            if (((screen.width >= 480) && (screen.height >= 800)) || ((screen.width >= 800) && (screen.height >= 480)) || navigator.userAgent.match(/ipad/gi)) {
	                var $window = $(window);
	                $window.scroll(function () {
	                    if ($window.scrollTop() == 0){
	                        $('header').removeClass("scroll");
	                        $('header .scroll').removeClass("scroll");
	                        $('header').addClass("content");
	                    }
	                    else {
	                        if ($window.scrollTop() > 0) {
	                            $('header').removeClass("content");
	                            $('header').addClass("scroll");
	                            $('header .search').addClass("scroll");
	                        }
	                    }
	                });
				}
	        }
        });
        // Show the search bar on click on search :
        $(function () {
            $('header nav .icon-search').click(function () {
                $('header .search').slideToggle();
            });
        });
        // Show mobile navigation  :
        $(function () {
            $('header .mobile i').click(function () {
                $('header .mobile ul').slideToggle();
            });
        });
		
	/* PAGES 
	 * Général, Accueil, Entreprise, ...
	 */

		/* INIT */
        (function(){
            if($("#init .status").length){
                // delai :
                var theTimeRequired = 1900;
                
                // on change la valeur :
                var val = $("#init .status div").html();
                var val = val.split(";");
                $("#init .status div").html(val[0]);

                // on joue l'effet :
                var position = 0;
                function updateText(){
                    setInterval(function(){
                        $("#init .status div").fadeIn(500);
                        $("#init .status div").html(val[position]);
                    }, theTimeRequired);

                    setTimeout(function(){
                        $("#init .status div").fadeOut(500);
                        position = (position == 1 ? "0" : "1");
                    }, theTimeRequired-500 );
                }
                updateText()
                setInterval(updateText, theTimeRequired );
            }
        })();
	
		/* TEASER */
        (function(){
            // delai :
            var theTimeRequired = 3000;
            
            // titre :
            (function(){
                if($("#teaser .status").length){
                    // on change la valeur :
                    var val = $("#teaser .status").html();
                    var val = val.split(";");
                    $("#teaser .status").html(val[0]);

                    // on définit la taille de la partie description :
                    var hei = $("#teaser .status").height();
                    $("#teaser .status").height(hei);

                    // on joue l'effet :
                    var position = 0;
                    function updateText(){
                        setInterval(function(){
                            $("#teaser .status div").fadeIn(500);
                            $("#teaser .status div").html(val[position]);
                        }, theTimeRequired);

                        setTimeout(function(){
                            $("#teaser .status div").fadeOut(500);
                            position = (position == 1 ? "0" : "1");
                        }, theTimeRequired-500 );
                    }
                    updateText()
                    setInterval(updateText, theTimeRequired );
                }
            })();
            
            // description :
            (function(){
                if($("#teaser .description").length){
                    // on change la valeur :
                    var val = $("#teaser .description").html();
                    var val = val.split(";;");
                    $("#teaser .description").html(val[0]);

                    // on définit la taille de la partie description :
                    var hei = $("#teaser .description").height();
                    $("#teaser .description").height(hei);

                    // on joue l'effet :
                    var position = 0;

                    function updateText(){
                        setInterval(function(){
                            $("#teaser .description p").fadeIn(500);
                            $("#teaser .description p").html(val[position]);
                        }, theTimeRequired);

                        setTimeout(function(){
                            $("#teaser .description p").fadeOut(500);
                            position = (position == 1 ? "0" : "1");
                        }, theTimeRequired-500 );
                    }

                    updateText()
                    setInterval(updateText, theTimeRequired );
                }
            })();
		})();
		countdownManager = {
				// Configuration
				targetTime: new Date('Dec 15 00:00:00 2014'), // Date cible du compte à rebours (00:00:00)
				displayElement: { // Elements HTML où sont affichés les informations
					day:  null,
					hour: null,
					min:  null,
					sec:  null
				},
				
				// Initialisation du compte à rebours (à appeler 1 fois au chargement de la page)
				init: function(){
					// Récupération des références vers les éléments pour l'affichage
					// La référence n'est récupérée qu'une seule fois à l'initialisation pour optimiser les performances
					this.displayElement.day  = jQuery('#countdown_day');
					this.displayElement.hour = jQuery('#countdown_hour');
					this.displayElement.min  = jQuery('#countdown_min');
					this.displayElement.sec  = jQuery('#countdown_sec');
					
					// Lancement du compte à rebours
					this.tick(); // Premier tick tout de suite
					window.setInterval("countdownManager.tick();", 1000); // Ticks suivant, répété toutes les secondes (1000 ms)
				},
				
				// Met à jour le compte à rebours (tic d'horloge)
				tick: function(){
					// Instant présent
					var timeNow  = new Date();
					
					// On s'assure que le temps restant ne soit jamais négatif (ce qui est le cas dans le futur de targetTime)
					if( timeNow > this.targetTime ){
						timeNow = this.targetTime;
					}
					
					// Calcul du temps restant
					var diff = this.dateDiff(timeNow, this.targetTime);
					
					this.displayElement.day.text(  diff.day  );
					this.displayElement.hour.text( diff.hour );
					this.displayElement.min.text(  diff.min  );
					this.displayElement.sec.text(  diff.sec  );
				},
				
				// Calcul la différence entre 2 dates, en jour/heure/minute/seconde
				dateDiff: function(date1, date2){
					var diff = {}                           // Initialisation du retour
					var tmp = date2 - date1;

					tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
					diff.sec = tmp % 60;                    // Extraction du nombre de secondes
					tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
					diff.min = tmp % 60;                    // Extraction du nombre de minutes
					tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
					diff.hour = tmp % 24;                   // Extraction du nombre d'heures
					tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
					diff.day = tmp;

					return diff;
				}
			};
			countdownManager.init();
		
		/* GENERAL */
			// content-text-list-points
			$(function () {
				$('.content-text-list-points-title').click(function () {
					if($(this).children("i").html() == "+"){
						$(this).children("i").html("-");
                        $(this).addClass("active");
					} else {
                        $(this).children("i").html("+");
                        $(this).removeClass("active");
					}
					
					$(this).next(".content-text-list-points-content").slideToggle();
				});
			});
			// hide notification in content
			$(function () {
				$('.notify').click(function () {
					$(this).slideUp();
				});
			});
			
		/* MISSION */
			// Recap mission
            $(function () {
                $("#missions .missions-liste li").click(function() {
                    $(this).find(".contente").slideToggle();
                });
            });
			// vide la taxtearea
            $(function () {
                $("#missions .content-text-form .element textarea").click(function() {
                    $("#missions .content-text-form .element textarea").val("");
                });
            });

		/* CONTACT */
			// vide la taxtearea
            $(function () {
                $("#contact .content-text-form .element textarea").click(function() {
                    $("#contact .content-text-form .element textarea").val("");
                });
            });
});
