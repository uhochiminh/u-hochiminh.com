<?php
/**
 * Footer
 * Intégrer facilement le Footer à vos pages.
 *
 * @Author: GARCIA Johan - johgarci
 */

function pFooter()	{
?>
<footer class="content">
	<div class="widgets">
		<address class="content-address">
			<div class="title">NOTRE ADRESSE</div>
            <div class="value">
                <p>Pôle Universitaire Français de l'Université Nationale</p>
                <p>Quartier 6, Linh Trung Ward,
                Thu Duc District,
                Ho Chi Min-Ville, Vietnam</p>
                <p>Téléphone: <a href="tel:+84837242169">+84 (0)837 242 169</a><br>
                Fax: <a href="tel:+84837242166">+84 (0)837 242 166</a></p>
            </div>
		</address>
		<div class="content-links">
			<div class="title">LIENS</div>
			<ul class="value">
				<li><i class="fa fa-chevron-right"></i><a href="Accessibilite.html" tabindex="7" accesskey="0">Accessibilité</a></li>
				<li><i class="fa fa-chevron-right"></i><a href="Connexion.html">Connexion</a></li>
				<li><i class="fa fa-chevron-right"></i><a href="MentionsLegales.html" tabindex="8" accesskey="7">Mentions légales</a></li>
				<li><i class="fa fa-chevron-right"></i><a href="PlanDuSite.html" tabindex="9" accesskey="8">Plan du site</a></li>
				<li><i class="fa fa-chevron-right"></i><a href="Contact.html">Contact</a></li>
			</ul>
		</div>
		<div class="content-social-network">
			<div class="title">RESEAUX SOCIAUX</div>
			<ul class="value">
                <li><a href="#"><i class="fa fa-rss"></i><span>Flux RSS</span></a></li>
                <li><a href="https://www.facebook.com/uhochiminh" tabindex="10" accesskey="f"><i class="fa fa-facebook-square"></i><span>Facebook</span></a></li>
                <li><a href="https://twitter.com/u_hochiminh" tabindex="11" accesskey="t"><i class="fa fa-twitter"></i><span>Twitter</span></a></li>
			</ul>
		</div>
        <div class="clear"></div>
	</div>
	<div class="copyright">
		<p class="left">Designed &amp; Developed by <a href="#">IUT Bordeaux 1</a> - <a href="#">DAWIN 1 Groupe 3</a></p>
		<p class="right">Copyright &copy; 2014 - All Right Reserved - <a href="#">u-hochiminh.com</a></p>
	</div>
</footer>
<?php
}
?>