<?php
/**
 * _all
 * Fait appel à tous les fichiers requis.
 *
 * @Author: IACHI Dimitri - diachi
 */

// CONFIGS :
$GLOBALS["site"]["domaine"] = "u-hochiminh.com";
$GLOBALS["site"]["nom"] = "u-hochiminh";
$GLOBALS["site"]["description"] = "Le pôle universitaire français (PUF) de Ho Chi Minh spécialité INFO qui prépare 5 formations au titre d'expert en informatique et systèmes d'information arrive bientôt sur vos écrans.";
$GLOBALS["site"]["keywords"] = "";
$GLOBALS["site"]["author"] = "";
$GLOBALS["site"]["category"] = "";
$GLOBALS["site"]["reply-to"] = "";
$GLOBALS["site"]["copyright"] = "";
$GLOBALS["site"]["robots"] = "all";
$GLOBALS["site"]["twitter"] = "@u_hochiminh";
$GLOBALS["site"]["facebook"] = "";

$GLOBALS["site"]["url"] = "http://".$GLOBALS["site"]["domaine"];

// PARTS :
require("lib/htmlCleaner.php");
require("parts/doctype.php");
require("parts/header.php");
require("parts/footer.php");
?>