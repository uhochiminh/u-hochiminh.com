<p><strong>To : </strong> {{{ $data['your-email'] }}} </p>
<p><strong>Date : </strong> {{{ date("Y-m-d H:i:s") }}}</p>
<p><strong>Subject : </strong> {{{ $data['your-subject'] }}} </p>
<hr/>
<br/>

{{ $data['your-message'] }}

<hr/>
{{{ $data['domain'] }}}