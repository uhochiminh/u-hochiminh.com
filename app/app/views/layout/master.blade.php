<!DOCTYPE html>
<html lang="{{{Request::segment(1)}}}">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-language" content="fr-FR" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>@yield('title'){{ "&nbsp;-&nbsp;" }}{{{ $settings['domain'] }}}</title>

	<meta name="category" content="{{{ $settings['domain'] }}}" />
	<meta name="copyright" content="{{{ $settings['copyright'] }}}{{{ $settings['domain'] }}}" />
	<meta name="category" content="website" />
	<meta name="author" content="{{{ $settings['authors'] }}}">
	<meta name="domain" content="{{{ $settings['domain'] }}}" />

	@yield('meta')

	<!-- Add to favicon on Desktop -->
	<link rel="shortcut icon" href="{{{ asset('assets/images/touch/favicon.ico') }}}">

	<!-- Add to homescreen for Chrome on Android -->
	<meta name="mobile-web-app-capable" content="yes" />
	<link rel="icon" sizes="192x192" href="{{{ asset('assets/images/touch/chrome-touch-icon-192x192.png') }}}">

	<!-- Add to homescreen for Safari on iOS -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-title" content="" />
	<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/images/touch/apple-touch-icon-precomposed.png') }}}">

	<!-- Tile icon for Win8 (144x144 + tile color) -->
	<meta name="msapplication-TileImage" content="{{{ asset('assets/images/touch/ms-touch-icon-144x144-precomposed.png') }}}" />
	<meta name="msapplication-TileColor" content="#3372DF" />

	<!-- Essential JS - NOT APPEND ALL -->
	{{ HTML::script('assets/scripts/jquery-2.1.1.js') }}
</head>
<body>
 	@section('content')
    @show

	<!-- Classic CSS -->
	{{ HTML::style('http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900') }}
	{{ HTML::style('http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic') }}
	{{ HTML::style('assets/styles/font-awesome.css') }}
	{{ HTML::style('assets/styles/reset.css') }}
	{{ HTML::style('assets/styles/style.css') }}
	{{ HTML::style('assets/styles/style-mobile.css') }}
	{{ HTML::style('assets/styles/style-tablette.css') }}
	{{ HTML::style('assets/styles/animate.css') }}
	
	<!-- Classic JS -->
	{{ HTML::script('assets/scripts/TweenMax.min.js') }}
	{{ HTML::script('assets/scripts/ScrollToPlugin.min.js') }}
	{{ HTML::script('assets/scripts/placeholders.js') }}
	{{ HTML::script('assets/scripts/fonction.js') }}
	
	<!-- Google Analytics -->
	{{$settings['analytics']}}

</body>
</html>