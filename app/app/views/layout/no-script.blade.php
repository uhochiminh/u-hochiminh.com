<noscript>
    <table valign="middle" align="center" border="0" height="100%" width="100%">
        <tbody>
            <tr height="100%">
                <td align="center" height="100%" valign="middle" width="100%">
					{{ HTML::image('assets/images/logo_teasing-init.png', Lang::get('libelle.GENERAL_SITENAME'), array('title' => Lang::get('libelle.GENERAL_SITENAME') )) }}

                    <div class="erreur">
						<p>{{ Lang::get('libelle.NOSCRIPT_explaination') }}</p>
						<p>{{ Lang::get('libelle.NOSCRIPT_explication') }}</p>
                    </div>

                    <div class="social">
                        <a href="#facebook"><i class="fa fa-facebook-square"></i></a>
                        <a href="#twitter"><i class="fa fa-twitter"></i></a>
                    </div>

					<div class="copyright">{{ Lang::get('libelle.GENERAL_AUTHORS') }}</strong>
					<br/>{{ Lang::get('libelle.GENERAL_COPYRIGHT') }} {{ Lang::get('libelle.GENERAL_SITENAME') }}</div>
                </td>
            </tr>
        </tbody>
    </table>
</noscript>