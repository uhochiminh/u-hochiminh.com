<header class="content">
	<div class="header content">
		<a class="ajax" href="{{ URL::to(Request::segment(1).'/home') }}" tabindex="0" accesskey="1" class="logo" title="{{Lang::get('libelle.POSITION_HOMEPAGE')}}"><h1>{{ HTML::image('/assets/images/logo_menu.png', Lang::get('libelle.GENERAL_SITENAME')) }}</h1></a>
		<nav>
			<ul>
				<li class="{{ Request::is( Request::segment(1).'/educations' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/educations') }}" tabindex="1" accesskey="2">{{Lang::get('libelle.POSITION_TRAINING')}}</a></li>
				<li>|</li>
				<li class="{{ Request::is( Request::segment(1).'/entreprises' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/entreprises') }}" tabindex="2" accesskey="3">{{Lang::get('libelle.POSITION_COMPANIES')}}</a></li>
				<li>|</li>
				<li class="{{ Request::is( Request::segment(1).'/news' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/news') }}" tabindex="3" accesskey="4">{{Lang::get('libelle.POSITION_PRACTICAL_INFORMATION')}}</a></li>
				<li>|</li>
				<li class="{{ Request::is( Request::segment(1).'/contact' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/contact') }}" tabindex="4" accesskey="5">{{Lang::get('libelle.POSITION_CONTACT')}}</a></li>
				
				@if(Auth::check())
					<li>|</li>
					<li class="{{ Request::is( Request::segment(1).'/missions' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/missions') }}" tabindex="5" accesskey="6">{{Lang::get('libelle.POSITION_MISSIONS')}}</a></li>
					<li class="space"></li>
					
					@if(Auth::User()->_level == "admin")<li class="icon edition"><a tabindex="6" accesskey="e" title="{{Lang::get('libelle.POSITION_EDIT')}}"><i class="fa fa-tachometer"></i></a></li>@endif
					<li class="icon"><a class="ajax" href="{{ URL::to(Request::segment(1).'/user/logout') }}" tabindex="7" accesskey="7" title="{{Lang::get('libelle.POSITION_DISCONNECT')}}"><i class="fa fa-sign-out"></i></a></li>
				@else
					<li class="space"></li>
					<li class="icon {{ Request::is( Request::segment(1).'/user/login' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/user/login') }}" tabindex="5" accesskey="6" title="{{Lang::get('libelle.POSITION_CONNEXION')}}"><i class="fa fa-user"></i></a></li>
				@endif
				<li class="icon icon-search"><a tabindex="8" accesskey="r" title="{{Lang::get('libelle.POSITION_SEARCH')}}"><i class="fa fa-search"></i></a></li>
			</ul>
		</nav>

		<div class="mobile no-display click">
			<i class="fa fa-bars"></i>
			<ul>
				<li class="{{ Request::is( Request::segment(1).'/educations' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/educations') }}">{{Lang::get('libelle.POSITION_TRAINING')}}</a></li>
				<li class="{{ Request::is( Request::segment(1).'/entreprises' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/entreprises') }}">{{Lang::get('libelle.POSITION_COMPANIES')}}</a></li>
				<li class="{{ Request::is( Request::segment(1).'/news' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/news') }}">{{Lang::get('libelle.POSITION_PRACTICAL_INFORMATION')}}</a></li>
				<li class="{{ Request::is( Request::segment(1).'/contact' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/contact') }}">{{Lang::get('libelle.POSITION_CONTACT')}}</a></li>
				@if(Auth::check())
					<li class="{{ Request::is( Request::segment(1).'/missions' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/missions') }}">{{Lang::get('libelle.POSITION_MISSIONS')}}</a></li>
					<li><a class="ajax" href="{{ URL::to(Request::segment(1).'/user/logout') }}">{{Lang::get('libelle.POSITION_DISCONNECT')}}</a></li>
				@else
					<li class="{{ Request::is( Request::segment(1).'/user/login' ) ? 'active' : '' }}"><a class="ajax" href="{{ URL::to(Request::segment(1).'/user/login') }}">{{Lang::get('libelle.POSITION_CONNEXION')}}</a></li>
				@endif
			</ul>
		</div>
		<div class="clear"></div>	
	</div>
	<div class="search content no-display">
		<div class="content">
			<form action="{{ URL::to(Request::segment(1).'/search') }}" method="GET">
				<i class="fa fa-search"></i>
				<input type="text" name="q" placeholder="{{Lang::get('libelle.SEARCH_PLACEHOLDER')}}">
				<button class="no-display" type="submit">{{Lang::get('libelle.SEARCH_BUTTON')}}</button>
				<div class="clear"></div>
			</form>
		</div>
	</div>
</header>