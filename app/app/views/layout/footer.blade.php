<footer class="content">
	<div class="widgets">
		<address class="content-address">
			<div class="title">{{{ Lang::get('libelle.LOCATION_TITLE') }}}</div>
            <div class="value">
                @foreach(explode("<br/>", $settings['adress']) as $addr)<p>{{$addr}}</p>@endforeach
                
                <p>{{Lang::get('libelle.LOCATION_PHONE')}}: <a href="tel:{{$settings['phone']}}">{{$settings['phone']}}</a><br>
                {{Lang::get('libelle.LOCATION_FAX')}}: <a href="tel:{{$settings['fax']}}">{{$settings['fax']}}</a></p>
            </div>
		</address>
		<div class="content-links">
			<div class="title">{{Lang::get('libelle.LINKS_TITLE')}}</div>
			<ul class="value">
				<li><i class="fa fa-chevron-right"></i><a class="ajax" href="{{ URL::to(Request::segment(1).'/accessibility') }}" tabindex="8" accesskey="0">{{Lang::get('libelle.LINKS_ACCESSIBILITY')}}</a></li>
				@if(!Auth::check())
                    <li><i class="fa fa-chevron-right"></i><a class="ajax" href="{{ URL::to(Request::segment(1).'/user/login') }}">{{Lang::get('libelle.LINKS_CONNEXION')}}</a></li>
                @else
                    <li><i class="fa fa-chevron-right"></i><a class="ajax" href="{{ URL::to(Request::segment(1).'/user/logout') }}">{{Lang::get('libelle.LINKS_DECONNEXION')}}</a></li>
                @endif
				<li><i class="fa fa-chevron-right"></i><a class="ajax" href="{{ URL::to(Request::segment(1).'/legal-notice') }}" tabindex="9" accesskey="9">{{Lang::get('libelle.LINKS_LEGAL_NOTICES')}}</a></li>
				<li><i class="fa fa-chevron-right"></i><a class="ajax" href="{{ URL::to(Request::segment(1).'/sitemap') }}" tabindex="10" accesskey="s">{{Lang::get('libelle.LINKS_SITE_MAP')}}</a></li>
				<li><i class="fa fa-chevron-right"></i><a class="ajax" href="{{ URL::to(Request::segment(1).'/contact') }}">{{Lang::get('libelle.LINKS_CONTACT')}}</a></li>
			</ul>
		</div>
		<div class="content-social-network">
			<div class="title">{{Lang::get('libelle.SOCIAL_NETWORKS_TITLE')}}</div>
			<ul class="value">
                <li><a href="{{ URL::to(Request::segment(1).'/news/rss') }}" target="_blank"><i class="fa fa-rss"></i><span>{{Lang::get('libelle.SOCIAL_NETWORKS_RSS')}}</span></a></li>
                <li><a href="{{$settings['link-facebook']}}" tabindex="11" accesskey="f" target="_blank"><i class="fa fa-facebook-square"></i><span>{{Lang::get('libelle.SOCIAL_NETWORKS_FACEBOOK')}}</span></a></li>
                <li><a href="{{$settings['link-twitter']}}" tabindex="12" accesskey="t" target="_blank"><i class="fa fa-twitter"></i><span>{{Lang::get('libelle.SOCIAL_NETWORKS_TWITTER')}}</span></a></li>
			</ul>
		</div>
        <div class="clear"></div>
	</div>
	<div class="copyright">
		<p class="left">{{Lang::get('libelle.GENERAL_AUTHORS')}}</p>
		<p class="right">{{Lang::get('libelle.GENERAL_COPYRIGHT')}}<a href="{{ URL::to(Request::segment(1).'/home') }}">{{Lang::get('libelle.GENERAL_SITENAME')}}</a></p>
	</div>
</footer>