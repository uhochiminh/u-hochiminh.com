<div class="position">
	<i class="fa fa-sitemap blue"></i>
	<a class="ajax" href="{{ URL::to(Request::segment(1).'/home') }}">{{ strtoupper(Lang::get('libelle.POSITION_HOMEPAGE')) }}</a>&nbsp; 
	<i class="fa fa-angle-right"></i>&nbsp;
		
	@foreach ($data['position'] as $key => $value)
		@if(!empty($value))
			<a class="ajax" href="{{{ $value }}}">{{{ strtoupper($key) }}}</a>&nbsp; 
			<i class="fa fa-angle-right"></i>
		@else
			&nbsp; {{{ strtoupper($key) }}} &nbsp; 
      	@endif
      @endforeach
</div>