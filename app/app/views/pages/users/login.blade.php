@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
<div class="script-direct">
	@include('layout.header')

	<div id="connexion" class="content">
		@include('layout.section_situation')
		@include('layout.section_position')

		<div class="content-text">

			<div class="box">
				<div class="icon"><i class="fa fa-lock"></i></div>

				@if($message) <div class="notify red">
					@if(is_object($message))
						@foreach ($message->all() as $msg)
						    <p>{{ $msg }}</p>
						@endforeach
					@else
						<p>{{ $message }}</p>
					@endif
				</div>@endif

				<form action="{{ Request::url() }}" method="POST" class="submit login">
					<div class="element">
						<label>{{ Lang::get('libelle.CONNEXION_LOGIN') }} :</label>
						<input type="text" name="your-login" size="60" class="your-login" />
					</div>

					<div class="element">
						<label>{{ Lang::get('libelle.CONNEXION_PASSWORD') }} :</label>
						<input type="password" name="your-password" size="180" class="your-password" />
					</div>

					<div class="element button">
						<input type="submit" value="{{ Lang::get('libelle.CONNEXION_CONNEXION') }}" class="click" />
					</div>
					<div class="clear"></div>
				</form>
			</div>

			<div class="clear"></div>
			
		</div>
	</div>

	@include('layout.footer')
</div>

@stop