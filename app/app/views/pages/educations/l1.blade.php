@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
    <div class="script-direct">
        @include('layout.header')

        <div id="formation" class="content">
            @include('layout.section_situation')
            @include('layout.section_position')

            <div class="content-text">
                <section class="content-text-formations-presentation">
                    <div class="content-text-text text justify">
                        <img src="{{{ asset('assets/images/formation-help.png') }}}" />
                        @foreach($page->modules as $mod)
                            @if($mod->_type == "text-block")
                                {{ $mod->_content }}
                            @endif
                        @endforeach
                    </div>
                    
                    <div class="clear"></div>
                    
                    @foreach($page->modules as $mod)
                    @if($mod->_type == "list")
                        <section class="content-text-presentation-list">
                            <div class="content-text-list-points text">
                                @foreach(explode("<----separator---->", $mod->_content) as $element)
                                    <?php $element_separator = explode("<--separator-->", $element); ?>
                                    <h5 class="content-text-list-points-title click"><i>+</i> {{{ trim($element_separator["0"]) }}} </h5>
                                    <div class="content-text-list-points-content no-display">{{ trim($element_separator["1"]) }}</div>
                                    <?php unset($element_separator); ?>
                                @endforeach
                            </div>
                            <div class="clear"></div>
                        </section>
                    @endif
                @endforeach
                </section>
            </div>
        </div>

        @include('layout.footer')
    </div>
@stop