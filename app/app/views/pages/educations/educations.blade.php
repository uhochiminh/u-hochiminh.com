@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
    <div class="script-direct">
        @include('layout.header')

        <div id="formations" class="content">
            @include('layout.section_situation')
            @include('layout.section_position')

            <div class="content-text">
                <section class="content-text-formations-slider">
                    <img src="{{{ asset('assets/images/formations-amphi.png') }}}" alt="Amphithéâtres">
                    <ul>
                        <li><a class="ajax" href="{{ URL::to(Request::segment(1).'/education/l1') }}">{{Lang::get('libelle.TRAINING_CONTENT_LICENCE_1')}}</a></li>
                        <li><a class="ajax" href="{{ URL::to(Request::segment(1).'/education/l2') }}">{{Lang::get('libelle.TRAINING_CONTENT_LICENCE_2')}}</a></li>
                        <li><a class="ajax" href="{{ URL::to(Request::segment(1).'/education/l3') }}">{{Lang::get('libelle.TRAINING_CONTENT_LICENCE_3')}}</a></li>
                        <li class="expand"><a class="ajax" href="{{ URL::to(Request::segment(1).'/education/m1') }}">{{Lang::get('libelle.TRAINING_CONTENT_MASTER_1')}}</a></li>
                        <li class="expand"><a class="ajax" href="{{ URL::to(Request::segment(1).'/education/m2') }}">{{Lang::get('libelle.TRAINING_CONTENT_MASTER_2')}}</a></li>
                    </ul>
                </section>

				@foreach($page->modules as $mod)
                	@if($mod->_type == "text-block")
	                <section class="content-text-formations-presentation">
	                    <h4 class="content-text-title"><span>{{{ $mod->_title }}}</span></h4>
	                    <div class="content-text-text text alinea justify">
	                        {{ $mod->_content }}
	                    </div>
	                </section>
                @endif
            @endforeach
            </div>
        </div>

        @include('layout.footer')
    </div>
@stop