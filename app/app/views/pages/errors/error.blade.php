@extends('layout.master')

@section('title') {{ $data["situation"]["title"] }} @stop

@section('content')
	<div class="script-direct">
		@include('layout.header')

		<div id="erreur" class="content">
			@include('layout.section_situation')
			@include('layout.section_position')

			<div class="content-text">
				<section class="content-text-erreur">
					<div class="title">{{{ $data["content"]["title"] }}}</div>
					<div class="texte">{{{ $data["content"]["texte"] }}}</div>
				</section>
			</div>
		</div>

		@include('layout.footer')
	</div>
@stop