@extends('layout.master')

@section('title') {{ $page->_title }} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
    <div class="script-direct">
        @include('layout.header')

        <div id="entreprise" class="content">
            @include('layout.section_situation')
            @include('layout.section_position')

            <div class="content-text">
            	@if(isset($page->modules[0]))
                <section class="content-text-entreprises">
                    <h4 class="content-text-title"><span>{{{$page->modules[0]->_title}}}</span></h4>
                    <div class="content-text-text content-text-points text justify">
                        <table><tbody><tr>
                                <td><div class="point-circle"><i class="fa fa-university"></i></div></td>
                                <td>
                                    {{$page->modules[0]->_content}}
                                </td>
                            </tr></tbody></table>
                    </div>
                </section>
                @endif

                @if(isset($page->modules[1]))
                <section class="content-text-entreprises">
                    <h4 class="content-text-title"><span>{{{$page->modules[1]->_title}}}</span></h4>
                    <div class="content-text-text content-text-points text justify">
                        <table><tbody><tr>
                                <td>
                                    {{$page->modules[1]->_content}}
                                </td>
                                <td><div class="point-circle"><i class="fa fa-briefcase"></i></div></td>
                            </tr></tbody></table>
                    </div>
                </section>
                @endif

                @if(isset($page->modules[2]))
                <section class="content-text-entreprises">
                    <h4 class="content-text-title"><span>{{{$page->modules[2]->_title}}}</span></h4>
                    <div class="content-text-text text justify">
                        {{$page->modules[2]->_content}}
                    </div>
                </section>
                @endif
            </div>
        </div>

        @include('layout.footer')
    </div>
@stop