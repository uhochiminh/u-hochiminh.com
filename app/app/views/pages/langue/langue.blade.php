@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
	@include('layout.no-script')
	<div id="init" class="script">
		<table valign="middle" align="center" border="0" height="100%" width="100%">
			<tbody>
				<tr height="100%">
					<td align="center" height="100%" valign="middle" width="100%">
						<div class="script center" style="display:none;">
							<h1>{{ HTML::image('assets/images/logo_teasing-init.png', Lang::get('libelle.GENERAL_SITENAME'), array('title' => Lang::get('libelle.GENERAL_SITENAME') )) }}</h1>

							<div class="status"><div>/ {{ Lang::get('libelle.INIT_ENGLISHLIBELLE') }} /;/ {{ Lang::get('libelle.INIT_FRENCHLIBELLE') }} /</div></a></div>

						<div class="language">
							<a href="{{ URL::to('en/home') }}">{{ HTML::image('assets/images/drapeau-english.png', Lang::get('libelle.INIT_ENGLISHFLAG'), array('title' => Lang::get('libelle.INIT_ENGLISHFLAG') )) }}</a>
							<a href="{{ URL::to('fr/home') }}">{{ HTML::image('assets/images/drapeau-french.png', Lang::get('libelle.INIT_ENGLISHFLAG'), array('title' => Lang::get('libelle.INIT_FRENCHFLAG') )) }}</a>
						</div>

						<div class="copyright">{{ Lang::get('libelle.GENERAL_AUTHORS') }}</strong>
							<br/>{{ Lang::get('libelle.GENERAL_COPYRIGHT') }} {{ Lang::get('libelle.GENERAL_SITENAME') }}</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
@stop