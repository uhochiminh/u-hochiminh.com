@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="http://{{{ $settings['twitter_id'] }}}/" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
<div class="script-direct">
    @include('layout.header')
    <div id="accessibilite" class="content">
        @include('layout.section_situation')
        @include('layout.section_position')
        
        <div class="content-text">
            @foreach($page->modules as $mod)
                @if($mod->_type == "text-block")
                <section class="content-text-accessibility">
                    <h4 class="content-text-title"><span>{{$mod->_title}}</span></h4>
                    <div class="content-text-text text justify">
                        {{$mod->_content}}
                    </div>
                </section>
                @endif
            @endforeach
        </div>
    </div>
    @include('layout.footer')
</div>
@stop