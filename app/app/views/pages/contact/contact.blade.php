@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
<div class="script-direct">
    @include('layout.header')
    <div id="contact" class="content">
        @include('layout.section_situation')
        @include('layout.section_position')
        
        <div class="content-text">
            @foreach($page->modules as $mod)
                @if($mod->_type == "maps")
                <section class="content-text-maps">
                    <div class="content-text-text">
                        {{$mod->_content}}
                    </div>
                </section>
                @endif
            @endforeach

            <div class="clear"></div>

            <section class="content-text-form">
                <h4 class="content-text-title-left"><span>{{{ Lang::get("libelle.CONTACT_FORM_TITLE") }}}</span></h4>
                <div class="content-text-text">
                    @if(isset($result["success"]))
                        <div class="notify blue">{{{$result["success"]}}}</div>
                    @elseif(isset($result["errors"]))
                    	<div class="notify red">
                        @foreach ($result["errors"]->all() as $msg)
                            {{ $msg }}<br/>
                        @endforeach
                        </div>
                    @endif
                    
                    <form action="{{{Request::url()}}}" method="POST">
                        <div class="element">
                            <label>{{{ Lang::get("libelle.CONTACT_FORM_LAST_NAME_FIRST_NAME") }}} :</label>
                            <input type="text" name="your-name" size="40" 
								@if(isset($inputs)) value="{{$inputs['your-name']}}" @endif />
                        </div>

                        <div class="element">
                            <label>{{{ Lang::get("libelle.CONTACT_FORM_EMAIL") }}} :</label>
                            <input type="email" name="your-email" size="40" 
								@if(isset($inputs)) value="{{$inputs['your-email']}}" @endif />
                        </div>

                        <div class="element">
                            <label>{{{ Lang::get("libelle.CONTACT_FORM_OBJECT") }}} :</label>
                            <input type="text" name="your-subject" size="70" 
								@if(isset($inputs)) value="{{$inputs['your-subject']}}" @endif />
                        </div>

                        <div class="element">
                            <label>{{{ Lang::get("libelle.CONTACT_FORM_MESSAGE") }}} :</label>
                            <textarea name="your-message" cols="40" rows="10">@if(isset($inputs)){{$inputs['your-message']}}@endif</textarea>
                        </div>

                        <div class="element button">
                            <input type="submit" value="{{ Lang::get("libelle.FORM_SEND") }}" class="click" />
                            <input type="reset" value="{{ Lang::get("libelle.FORM_DELETE") }}" class="click" />
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </section>

            <section class="content-text-sidebar">
                <h4 class="content-text-title-right"><span>{{{ Lang::get("libelle.CONTACT_ADDRESS") }}}</span></h4>
                <div class="content-text-text">
                    <div class="element address">
                        <i class="fa fa-home"></i>
                        @foreach(explode("<br/>", $settings['adress']) as $addr)
                            <address class="value">{{$addr}}</address>
                        @endforeach
                        <div class="clear"></div>
                    </div>

                    <div class="element email">
                        <i class="fa fa-envelope"></i>
                        <a class="value" href="mailto:{{$settings['mail']}}">{{$settings['mail']}}</a>
                        <div class="clear"></div>
                    </div>

                    <div class="element telephone">
                        <i class="fa fa-phone"></i>
                        <a class="value" href="tel:{{$settings['phone']}}">{{$settings['phone']}}</a>
                        <div class="clear"></div>
                    </div>

                    <div class="element fax">
                        <i class="fa fa-fax"></i>
                        <a class="value" href="tel:{{$settings['fax']}}">{{$settings['fax']}}</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </section>
            
            <div class="clear"></div>
        </div>
    </div>
    @include('layout.footer')
</div>
@stop