@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
<div class="script-direct">
	@include('layout.header')
	<div id="missions" class="content">
		@include('layout.section_situation')
		@include('layout.section_position')
		
		<div class="content-text">
			<section class="content-text-view">
				<div class="content-text-text text">
					<div class="action right no-display">
						<i class="missions-calendar fa fa-calendar"></i>
						<i class="missions-menu fa fa-bars"></i>
					</div>
					@if(count($missions) > 0)
					<ul class="missions-liste">
						@foreach($missions as $mission)
						<li class="click">
							<div class="header">
								<span class="code">{{date("d/m/Y", strtotime(str_replace('-','/', $mission->_dateStart)))}} - {{date("d/m/Y", strtotime(str_replace('-','/', $mission->_dateEnd)))}}</span>
								<span class="professor">{{$mission->_profName}}</span>
								<i class="fa fa-plus"></i>
							</div>
							<div class="contente">
								<div>
									<p>Chargé de TD : {{$mission->_ctdName}} -  <a href="mailto:{{$mission->_ctdMail}}">{{$mission->_ctdMail}}</a></p>
								</div>
								<div>
									{{$mission->_description}}
								</div>
								
								@if($mission->_file != NULL)
									<br/>
									<a href="{{ URL::to(Request::segment(1).'/mission/'.$mission->_id)}}" class="button">{{Lang::get('libelle.MISSIONS_SHOW_DOWNLOAD')}}</a>
								@endif
								<div class="clear"></div>
							</div>
						</li>
						@endforeach
					</ul>
					@else
					<br/><br/><br/>
						<center>{{ Lang::get('libelle.MISSIONS_SHOW_NONE') }}</center>
					<br/><br/><br/>
					@endif
				</div>
			</section>
			
			
			@if(Auth::check() && Auth::User()->_level == "admin")
			<section class="content-text-form">
				<h4 class="content-text-title"><span>{{Lang::get('libelle.MISSIONS_FORM_TITLE')}}</span></h4>
				<div class="content-text-text text justify">
					<p>{{Lang::get('libelle.MISSIONS_FORM_EXPLAINATION')}}</p>
					
					@if(isset($result) && count($result)>0)
						@if(isset($result['errors']))
							<div class="notify red">
								@foreach($result['errors']->all() as $error)<p>{{$error}}</p>@endforeach
							</div>
						@else<div class="notify blue">{{$result['success']}}</div> @endif
					@endif
					
					<form action="{{ Request::url() }}" method="POST" enctype="multipart/form-data">
						<div class="left">
							<div class="element">
								<label>{{Lang::get('libelle.MISSIONS_FORM_PROFESSOR_LAST_NAME_FIRST_NAME')}} :</label>
								<input type="text" name="professor-name" size="60"
										@if(isset($inputs)) value="{{$inputs['professor-name']}}" @endif />
							</div>
							<div class="element">
								<label>{{Lang::get('libelle.MISSIONS_FORM_PROFESSOR_EMAIL')}} :</label>
								<input type="email" name="professor-email"size="60" 
										@if(isset($inputs)) value="{{$inputs['professor-email']}}" @endif />
							</div>
						</div>
						<div class="right">
							<div class="element">
								<label>{{Lang::get('libelle.MISSIONS_FORM_MANAGER_LAST_NAME_FIRST_NAME')}} :</label>
								<input type="text" name="manager-name" size="60"
										@if(isset($inputs)) value="{{$inputs['manager-name']}}" @endif />
							</div>
							<div class="element">
								<label>{{Lang::get('libelle.MISSIONS_FORM_MANAGER_EMAIL')}} :</label>
								<input type="email" name="manager-email"size="60" 
										@if(isset($inputs)) value="{{$inputs['manager-email']}}" @endif />
							</div>
						</div>
						<div class="element left">
							<label>{{Lang::get('libelle.MISSIONS_FORM_FROM')}} :</label>
							<input type="date" name="professor-mission-start" 
										@if(isset($inputs)) value="{{$inputs['professor-mission-start']}}" @endif />
						</div>
						<div class="element right">
							<label>{{Lang::get('libelle.MISSIONS_FORM_TO')}} :</label>
							<input type="date" name="professor-mission-end" 
										@if(isset($inputs)) value="{{$inputs['professor-mission-end']}}" @endif />
						</div>
						<div class="element">
							<label>{{Lang::get('libelle.MISSIONS_FORM_MESSAGE')}} :</label>
							<textarea name="your-message" cols="40" rows="10">@if(isset($inputs)){{$inputs['your-message']}} @endif</textarea>
						</div>
						<div class="element">
							<label>{{Lang::get('libelle.MISSIONS_FORM_LIAISON_FILE')}} :</label>
							<input type="file" name="liaison-file" />
						</div>
						<div class="element button">
							<input type="submit" value="{{Lang::get('libelle.FORM_SEND')}}" class="click" />
							<input type="reset" value="{{Lang::get('libelle.FORM_DELETE')}}" class="click" />
						</div>
						<div class="clear"></div>
					</form>
				</div>
			</section>
			@endif
		</div>
	</div>
	@include('layout.footer')
</div>
@stop