{{ $header }}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

	@foreach ($pages as $page)
	<url>
		<loc>{{ URL::to(Request::segment($page->_public)."/".$page->_name) }}</loc>
		<changefreq>{{ $page->_title }}</changefreq>
		<priority>{{ $page->_priority}}</priority>
	</url>
	@endforeach

</urlset>
