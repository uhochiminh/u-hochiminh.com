@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
<div class="script-direct">
    @include('layout.header')
    <div id="plan_du_site" class="content">
        @include('layout.section_situation')
        @include('layout.section_position')
        
        <div class="content-text">
			<h4 class="content-text-title-left"><span> {{{ Lang::get('libelle.LINKS_SITE_MAP') }}} :</span></h4>
			<br/><br/>
        
			<ul>
				@foreach($pages as $link)
				
					@if($link->_name == "user/login" && Auth::check())
					<li><i class="fa fa-angle-right"></i> <a class="ajax" href="{{{ URL::to(Request::segment(1).'/user/logout') }}}" title="{{{ Lang::get('libelle.LINKS_DECONNEXION') }}}"> {{{ Lang::get('libelle.LINKS_DECONNEXION') }}}</a></li>
					
					@elseif($link->_name == "news")
					<li><i class="fa fa-angle-right"></i> <a class="ajax" href="{{{ URL::to(Request::segment(1).'/'.$link->_name) }}}" title="{{{$link->_title}}}">{{{$link->_title}}}</a></li>
					@if (count($news)>0)
						<ul>
							@foreach($news as $new)
								<li><i class="fa fa-angle-right"></i><a class="ajax" href="{{{ URL::to(Request::segment(1).'/news/'.$new->_id.'-'.urlencode(strtolower($new->_title))) }}}" title="{{{$new->_title}}}">{{{$new->_title}}}</a></li>
							@endforeach
						</ul>
					@endif
					
					@else
					<li><i class="fa fa-angle-right"></i> <a class="ajax" href="{{{ URL::to(Request::segment(1).'/'.$link->_name) }}}" title="{{{$link->_title}}}">{{{$link->_title}}}</a></li>
					@endif
					
					@if (count($link->parentPage)>0)
						<ul>
							@foreach($link->parentPage as $sublink)
								<li><i class="fa fa-angle-right"></i><a class="ajax" href="{{{ URL::to(Request::segment(1).'/'.$sublink->_name) }}}" title="{{{$sublink->_title}}}">{{{$sublink->_title}}}</a></li>
							@endforeach
						</ul>
					@endif
					
				@endforeach
			</ul>
				
			<br/>
			<br/>
			
			<div class="clear"></div>
		</div>
    </div>
    @include('layout.footer')
</div>
@stop