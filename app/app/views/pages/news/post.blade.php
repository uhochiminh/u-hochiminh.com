@extends('layout.master')

@section('title') {{$new->_title}} @stop
@section('meta') 
	<meta name="description" content="{{{ trim(preg_replace('/\s\s+/', ' ', strip_tags($new->_extract))) }}}" />
	<meta name="keywords" content="{{{ $new->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $new->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ trim(preg_replace('/\s\s+/', ' ', strip_tags($new->_extract))) }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $new->_title }}}" />
	<meta property="og:description" content="{{{ trim(preg_replace('/\s\s+/', ' ', strip_tags($new->_extract))) }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
<div class="script-direct">
	@include('layout.header')

	<div id="info_pratique" class="content">
		@include('layout.section_situation')
		@include('layout.section_position')

		<div class="content-text">
			<article class="content-text-post">
				@if(!empty($new->_imageThumb))
					<img src="{{{ asset('app/storage/uploads/images/'.$new->_imageOriginal ) }}}" class="post-img" />
				@else
					<img src="{{{ asset('assets/images/infospratiques-post-img.png') }}}" class="post-img" />
				@endif
				<div class="post-content">
					<h4 class="content-text-title-left"><span>{{ $new->_title }}</span></h4>
					<div class="info">
						{{ Lang::get('libelle.PRACTICAL_INFORMATION_POSTED_ON') }} {{ date("d M Y",strtotime($new->_date)) }} 
						{{ "&nbsp;" }}{{ Lang::get('libelle.PRACTICAL_INFORMATION_BY') }} {{ $new->user->_name }} {{ $new->user->_surname }}
					</div>
					<div class="content-text-text alinea text justify">
						{{ $new->_content }}
					</div>
				</div>

				<div class="clear"></div>
			</article>

		
			<!-- pagination -->
			<!--<nav class="content-text-nav no-display">
				<div class="left"><a href="InfoPratique.html"><i class="fa fa-angle-left"></i><span> Salon du jeux vidéos</span></a></div>
				<div class="right"><a href="InfoPratique.html"><span>Les magnifestions contre la réforme des PUF </span><i class="fa fa-angle-right"></i></a></div>
				<div class="clear"></div>
			</nav>-->
			

		
			<div class="clear"></div>
		</div>
	</div>

	@include('layout.footer')
</div>

@stop