{{ $header }}
<rss version="2.0">
<channel>
<title>Flux RSS - u-hochiminh.com</title>
<link>http://u-hochiminh.com</link>
<description></description>
<copyright>{{ Lang::get('libelle.GENERAL_COPYRIGHT') }} {{ Lang::get('libelle.GENERAL_SITENAME') }}</copyright>
<category>News</category>

	@foreach ($news as $new)
	<item>
		<title>{{ $new->_title }}</title>
		<language>{{ $new->language->_url }}</language>
		<pubDate>{{ date('D, d M Y H:i:s T', strtotime(str_replace('-','/', $new->_date))) }}</pubDate>
		<description><![CDATA[ {{ $new->_extract }} ]]></description>
		<content><![CDATA[ {{ $new->_content }} ]]></content>
		<image>
			<url>{{ $new->_imageOriginal }}</url> 
		</image>
	</item>
	@endforeach

</channel>
</rss>