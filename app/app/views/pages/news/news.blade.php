@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
	<div class="script-direct">
		@include('layout.header')

		<div id="infos_pratiques" class="content">
			@include('layout.section_situation')
			@include('layout.section_position')

			<div class="content-text">
				@if(count($news)>0)
					@foreach ($news as $new)
					<article class="content-text-post">
						@if(!empty($new->_imageThumb))
							<img src="{{{ asset('app/storage/uploads/images/'.$new->_imageThumb ) }}}" class="post-img left" />
						@else
							<img src="{{{ asset('assets/images/infospratiques-posts-img.png') }}}" class="post-img left" />
						@endif
						<div class="post-content right">
							<h4 class="content-text-title-left"><span>{{ $new->_title }}</span></h4>
							<div class="info">
							{{ Lang::get('libelle.PRACTICAL_INFORMATION_POSTED_ON') }} {{ date("d M Y",strtotime($new->_date)) }} 
							{{ "&nbsp;" }}{{ Lang::get('libelle.PRACTICAL_INFORMATION_BY') }} {{ $new->user->_name }} {{ $new->user->_surname }}</div>
							<div class="content-text-text text justify">
								{{ $new->_extract }}
							</div>
							<div class="content-text-button">
								<a href="{{ URL::to(Request::segment(1).'/news/'.$new->_id.'-'.urlencode(strtolower($new->_title))) }}" class="read_more right ajax">{{ Lang::get('libelle.PRACTICAL_INFORMATION_READ_NEXT_BUTTON') }}</a>
							</div>
						</div>
	
						<div class="clear"></div>
					</article>
					@endforeach
				@else
				<br/><br/><br/>
					<center>{{{ Lang::get('libelle.PRACTICAL_INFORMATION_NOARTICLE') }}}</center>
				<br/><br/><br/>
				@endif
				<div class="clear"></div>
				
				
				<!-- pagination -->
				
				
				<div class="clear"></div>
			</div>
		</div>

		@include('layout.footer')
	</div>
	
@stop