@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
    <meta name="description" content="{{{ $page->_description }}}" />
    <meta name="keywords" content="{{{ $page->_keywords }}}" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
    <meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
    <meta name="twitter:title" content="{{{ $page->_title }}}" />
    <meta name="twitter:url" content="{{{ Request::url() }}}" />
    <meta name="twitter:description" content="{{{ $page->_description }}}" />
    <meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
    <meta property="og:title" content="{{{ $page->_title }}}" />
    <meta property="og:description" content="{{{ $page->_description }}}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{{ Request::url() }}}" />
    <meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
    <meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
    <div class="script-direct">
        @include('layout.header')

        <div id="accueil" class="content">
            <div class="situation no-display">
                <h2>{{{ $page->_title }}}</h2>
                <h3>{{{ $page->_description }}}</h3>
            </div>

            @foreach($page->modules as $mod)
                @if($mod->_type == "slider")
                    @foreach(explode(";", $mod->_content) as $imagekey => $imageValue)
                        @if($imagekey == 1 && !empty($imageValue))
                        <div class="slider-fixed no-display">
                            <img src="{{{ asset('app/storage/uploads/images/'.trim($imageValue) ) }}}" />
                        </div>
                        @endif
                    @endforeach
                
                    <div class="slider">
                        @foreach(explode(";", $mod->_content) as $imagekey => $imageValue)
                            @if(!empty($imageValue))
                            <div id="slide{{$imagekey+1}}" class="slide">
                                <img src="{{{ asset('app/storage/uploads/images/'.trim($imageValue) ) }}}"/>
                            </div>
                            @endif
                        @endforeach
                        <div id="prec">
                            <img src="{{{ asset('assets/images/slider-fleche-gauche.png') }}}" class="fleche-gauche" alt="Précédent" />
                        </div>
                        <div id="suiv">
                            <img src="{{{ asset('assets/images/slider-fleche-droite.png') }}}" class="fleche-droite" alt="Suivant" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    {{ HTML::script('assets/scripts/slider.js') }}
                @endif
            @endforeach

            <div class="content-text">
                
                @foreach($page->modules as $mod)                
                    @if($mod->_type == "text-block")
                        <section class="content-text-presentation">
                            <h4 class="content-text-title"><span> {{{$mod->_title}}} </span></h4>
                            <div class="content-text-text text alinea justify">
                                {{$mod->_content}}
                            </div>
                        </section>
                    @endif
                @endforeach
                            
                @foreach($page->modules as $mod)
                    @if($mod->_type == "list")
                        <section class="content-text-presentation-list">
                            <div class="content-text-list-points text">
                                @foreach(explode("<----separator---->", $mod->_content) as $element)
                                    <?php $element_separator = explode("<--separator-->", $element); ?>
                                    <h5 class="content-text-list-points-title click"><i>+</i> {{{ trim($element_separator["0"]) }}} </h5>
                                    <div class="content-text-list-points-content no-display">{{ trim($element_separator["1"]) }}</div>
                                    <?php unset($element_separator); ?>
                                @endforeach
                            </div>
                            <div class="clear"></div>
                        </section>
                    @endif
                @endforeach
                
                @foreach($page->modules as $mod)                
                    @if($mod->_type == "parteners")
                        <section class="content-text-parteners">
                            <h4 class="content-text-title"><span> {{{$mod->_title}}} </span></h4>
                            <ul class="content-text-text">
                                @if($mod->_content)
                                    @foreach(explode(";;;;", $mod->_content) as $partners)
                                        @if(!empty($partners))
                                            <?php $partners_separator = explode(";;;", $partners); ?>
                                            <li><a href="{{{ $partners_separator['0'] }}}" target="_blank">
                                                    <img src="{{{ asset('app/storage/uploads/images/'.$partners_separator['1'] ) }}}" alt="{{{ $partners_separator['2'] }}}"></a></li>
                                            <?php unset($partners_separator); ?>
                                        @endif
                                    @endforeach
                                @endif
                                </ul>
                        </section>
                    @endif
                @endforeach
            </div>
        </div>

        @include('layout.footer')
    </div>
@stop