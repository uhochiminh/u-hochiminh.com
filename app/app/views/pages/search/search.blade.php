@extends('layout.master')

@section('title') {{{ $page->_title }}} @stop
@section('meta') 
	<meta name="description" content="{{{ $page->_description }}}" />
	<meta name="keywords" content="{{{ $page->_keywords }}}" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:domain" content="{{{ $settings['domain'] }}}">
	<meta name="twitter:site" content="{{{ $settings['twitter_id'] }}}" />
	<meta name="twitter:title" content="{{{ $page->_title }}}" />
	<meta name="twitter:url" content="{{{ Request::url() }}}" />
	<meta name="twitter:description" content="{{{ $page->_description }}}" />
	<meta name="twitter:image:src" content="{{{ asset('assets/images/logo_cards.png') }}}" />
	<meta property="og:title" content="{{{ $page->_title }}}" />
	<meta property="og:description" content="{{{ $page->_description }}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{{ Request::url() }}}" />
	<meta property="og:site_name" content="{{{ $settings['domain'] }}}" />
	<meta property="og:image" content="{{{ asset('assets/images/logo_cards.png') }}}" />
@stop

@section('content')
	<div class="script-direct">
		@include('layout.header')

		<div id="search" class="content">
			@include('layout.section_situation')
			@include('layout.section_position')

			<div class="content-text">
				<h4 class="content-text-title-left"><span>{{{ Lang::get("libelle.SEARCH_SITUATION_LIBELLE") }}} : "<span>{{{ $request }}}</span>"</span></h4>
				<br/><br/>
				@if(isset($results["pages"]) or isset($results["news"]))
					@if (isset($results["pages"]))
						@foreach($results["pages"] as $result)
							@if($result->_language == $language->_id)
								<a class="ajax" href="{{ URL::to(Request::segment(1).'/'.$result->_name) }}"><div class="result-{{$result->_id}}">
									<strong>{{$result->_title}}</strong>
									<p>{{ substr(strip_tags($result->_description), 0, 200) }} ...</p>
								</div>
								</a>
								<br/>
							@endif
						@endforeach
					@endif
					
					@if (isset($results["news"]))
						@foreach($results["news"] as $result)
							@if($result->_langue == $language->_id)
								<a class="ajax" href="{{ URL::to(Request::segment(1).'/news/'.$result->_id.'-'.urlencode(strtolower($result->_title))) }}"><div class="result-{{$result->_id}}">
									<strong>{{$result->_title}}</strong>
									<p>{{ substr(strip_tags($result->_extract), 0, 200) }} ...</p>
								</div></a>
								<br/>
							@endif
						@endforeach
					@endif
				@else
					Aucun résultat trouvé
				@endif
				
				<br/>
				<br/>
				
				<div class="clear"></div>
			</div>
		</div>

		@include('layout.footer')
	</div>
	
@stop