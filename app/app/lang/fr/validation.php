<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| L'attributfollowing language lines contain L'attributdefault error messages used by
	| L'attributvalidator class. Some of these rules have multiple versions such
	| as L'attributsize rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "L'attribut :attribute doit être accepté.",
	"active_url"           => "L'attribut :attribute n'est pas une URl valide.",
	"after"                => "L'attribut :attribute doit être une date après :date.",
	"alpha"                => "L'attribut :attribute doit contenir que des lettres.",
	"alpha_dash"           => "L'attribut :attribute peut contenir des lettres, chiffres, et des tirets.",
	"alpha_num"            => "L'attribut :attribute peut contenir des lettres et des chiffres.",
	"array"                => "L'attribut :attribute doit être un tableau.",
	"before"               => "L'attribut :attribute doit être une date avant :date.",
	"between"              => array(
		"numeric" => "L'attribut :attribute doit être entre :min et :max.",
		"file"    => "L'attribut :attribute doit être entre :min et :max kilobytes.",
		"string"  => "L'attribut :attribute doit être entre :min et :max caractères.",
		"array"   => "L'attribut :attribute doit avoir entre :min et :max objets.",
	),
	"boolean"              => "L'attribut :attribute doit être vrai ou faux.",
	"confirmed"            => "La confirmation de :attribute n'a pas pu être validée.",
	"date"                 => "L'attribut :attribute n'est pas une date valide.",
	"date_format"          => "L'attribut :attribute ne correspond pas au format :format.",
	"different"            => "L'attribut :attribute et :other doivent être différents.",
	"digits"               => "L'attribut :attribute doit avoir :digits chriffres.",
	"digits_between"       => "L'attribut :attribute doit avoir entre :min et :max chiffres.",
	"email"                => "L'attribut :attribute doit être une adresse e-mail valide.",
	"exists"               => "L'attribut sélectionné :attribute est invalide.",
	"image"                => "L'attribut :attribute doit être une image.",
	"in"                   => "L'attribut sélectionné :attribute est invalide.",
	"integer"              => "L'attribut :attribute doit être un entier.",
	"ip"                   => "L'attribut :attribute doit être une adresse IP valide.",
	"max"                  => array(
		"numeric" => "L'attribut :attribute ne doit pas être plus grand que :max.",
		"file"    => "L'attribut :attribute ne doit pas être plus grand que :max kilobytes.",
		"string"  => "L'attribut :attribute ne doit pas avoir plus de :max caractères.",
		"array"   => "L'attribut :attribute ne doit pas contenir plus de :max objets.",
	),
	"mimes"                => "L'attribut :attribute doit être un ficher de type: :values.",
	"min"                  => array(
		"numeric" => "L'attribut :attribute doit être au minimum :min.",
		"file"    => "L'attribut :attribute doit être d'au minimum :min kilobytes.",
		"string"  => "L'attribut :attribute doit contenir au minimum :min caractères.",
		"array"   => "L'attribut :attribute doit avoir au minimim :min objets.",
	),
	"not_in"               => "L'attribut sélectionné :attribute est invalide.",
	"numeric"              => "L'attribut :attribute doit être un chiffre.",
	"regex"                => "Le format de :attribute est invalide.",
	"required"             => "Le champs :attribute est requis.",
	"required_if"          => "Le champs :attribute est requis quand :other a :value.",
	"required_with"        => "Le champs :attribute est requis :values is present.",
	"required_with_all"    => "Le champs :attribute est requis quand :values est présent.",
	"required_without"     => "Le champs :attribute est requis quand :values n'est pas présent.",
	"required_without_all" => "Le champs :attribute est requis quand aucunes des valeurs :values sont présentes.",
	"same"                 => "L'attribut :attribute et :other doivent correspondre.",
	"size"                 => array(
		"numeric" => "L'attribut :attribute doit être de la taille :size.",
		"file"    => "L'attribut :attribute doit être de la taille :size kilobytes.",
		"string"  => "L'attribut :attribute doit être de la taille :size caractères.",
		"array"   => "L'attribut :attribute doit contenir :size objets.",
	),
	"unique"               => "L'attribut :attribute a déjà été utilisé.",
	"url"                  => "Le format de :attribute est invalide.",
	"timezone"             => "L'attribut :attribute doit être une zone valide.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name L'attributlines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| L'attributfollowing language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
