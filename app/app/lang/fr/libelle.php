<?php
return array(
//--------------------------
// GENERAL
//--------------------------

'GENERAL_SITENAME' => 'u-hochiminh.com',
'GENERAL_AUTHORS' => 'Designed and Developed by IUT Bordeaux 1 - <strong>DAWIN 1 Groupe 3</strong>',
'GENERAL_COPYRIGHT' => 'Copyright © 2014 - All Right Reserved - ',

//--------------------------
// META
//--------------------------

'META_DOMAIN' => 'u-hochiminh.com',
'META_DESCRIPTION' => 'Le pôle universitaire français (PUF) de Ho Chi Minh spécialité INFO prépare 5 formations au titre d\'expert en informatique et systèmes d\'information.',
'META_KEYWORDS' => '',
'META_AUTHOR' => 'IUT Bordeaux 1 - DAWIN 1 Groupe 3',
'META_CATEGORY' => 'IT Schools',
'META_COPYRIGHT' => 'Copyright © 2014 - All Right Reserved - u-hochiminh.com',

//--------------------------
// Init
//--------------------------

'INIT_ENGLISHFLAG' => 'English Language',
'INIT_FRENCHFLAG' => 'Langue Française',
'INIT_FRENCHLIBELLE' => 'CHOISIR VOTRE LANGUE',
'INIT_ENGLISHLIBELLE' => 'CHOOSE YOUR LANGUAGE',

//--------------------------
// Position
//--------------------------

'POSITION_HOMEPAGE' => 'Accueil',
'POSITION_PRACTICAL_INFORMATION' => 'Infos Pratiques',
'POSITION_TRAINING' => 'Formations',
'POSITION_COMPANIES' => 'Entreprises',
'POSITION_CONTACT' => 'Contact',
'POSITION_EDIT' => 'Edition',
'POSITION_DISCONNECT' => 'Déconnexion',
'POSITION_CONNEXION' => 'Connexion',
'POSITION_SEARCH' => 'Rechercher',
'POSITION_ACCESSIBILITY' => 'Accessibilité',
'POSITION_LEGAL_NOTICES' => 'Mentions légales',
'POSITION_SITE_MAP' => 'Plan du site',
'POSITION_MISSIONS' => 'Missions',
'POSITION_500' => 'Error 500',
'POSITION_404' => 'Error 404',
'POSITION_401' => 'Error 401',
'POSITION_403' => 'Error 403',

//--------------------------
// Formulaire
//--------------------------

'FORM_SEND' => 'Envoyer',
'FORM_DELETE' => 'Effacer',

//--------------------------
// header.php
//--------------------------

'SEARCH_BUTTON' => 'Rechercher',
'SEARCH_PLACEHOLDER' => 'Saisir votre recherche...',

//--------------------------
// footer.php
//--------------------------

'LOCATION_TITLE' => 'Notre adresse',
'LOCATION_PHONE' => 'Téléphone',
'LOCATION_FAX' => 'Fax',

'LINKS_TITLE' => 'Liens',
'LINKS_ACCESSIBILITY' => 'Accessibilité',
'LINKS_CONNEXION' => 'Connexion',
'LINKS_DECONNEXION' => 'Déconnexion',
'LINKS_LEGAL_NOTICES' => 'Mentions Légales',
'LINKS_SITE_MAP' => 'Plan du site',
'LINKS_CONTACT' => 'Contact',

'SOCIAL_NETWORKS_TITLE' => 'Réseaux sociaux',
'SOCIAL_NETWORKS_RSS' => 'Flux RSS',
'SOCIAL_NETWORKS_FACEBOOK' => 'Facebook',
'SOCIAL_NETWORKS_TWITTER' => 'Twitter',
	
//--------------------------
// no-script.php
//--------------------------

'NOSCRIPT_explaination' => 'Your browser does not support javascript, thank you kindly <strong>update your browser</strong> to view the site\'s features.',
'NOSCRIPT_explication' => 'Votre navigateur ne supporte pas le javascript, merci de bien vouloir <strong>mettre à jour votre navigateur</strong> afin d\'accéder aux  fonctions du site.',

//--------------------------
// Formations.php
//--------------------------

'TRAINING_CONTENT_LICENCE' => 'Licence',
'TRAINING_SITUATION_TITLE_LICENCE_1' => 'Licence 1',
'TRAINING_SITUATION_TITLE_LICENCE_2' => 'Licence 2',
'TRAINING_SITUATION_TITLE_LICENCE_3' => 'Licence 3',
'TRAINING_CONTENT_MASTER' => 'Master',
'TRAINING_SITUATION_TITLE_MASTER_1' => 'Master 1',
'TRAINING_SITUATION_TITLE_MASTER_2' => 'Master 2',

//--------------------------
// InfosPratiques.php
//--------------------------

'PRACTICAL_INFORMATION_NOARTICLE' => 'Il n\'y a pas d\'article présent dans les informations pratiques.',
'PRACTICAL_INFORMATION_READ_NEXT_BUTTON' => 'Lire la suite',
'PRACTICAL_INFORMATION_POSTED_ON' => 'Posté le',
'PRACTICAL_INFORMATION_BY' => 'par',

//--------------------------
// Contact.php
//--------------------------

'CONTACT_ADDRESS' => 'Nos coordonnées',
'CONTACT_FORM_TITLE' => 'Nous contacter',
'CONTACT_FORM_LAST_NAME_FIRST_NAME' => 'Nom & Prénom :',
'CONTACT_FORM_EMAIL' => 'Mail',
'CONTACT_FORM_OBJECT' => 'Objet',
'CONTACT_FORM_MESSAGE' => 'Message',
'CONTACT_FORM_ERROR_YOUR_NAME_RQ' =>'Votre nom est requis.',
'CONTACT_FORM_ERROR_YOUR_NAME_MAX' =>'Votre nom ne peut dépasser 60 caractères.',
'CONTACT_FORM_ERROR_YOUR_EMAIL_RQ' =>'Vous devez saisir une adresse mail.',
'CONTACT_FORM_ERROR_YOUR_EMAIL_INV' =>'Votre adresse mail est invalide.',
'CONTACT_FORM_ERROR_SUBJECT_RQ' =>'Vous devez saisir un objet.',
'CONTACT_FORM_ERROR_SUBJECT_MAX' =>'Votre objet ne peut dépasser 60 caractères.',
'CONTACT_FORM_ERROR_MESSAGE_RQ' =>'Vous devez saisir un message.',

//--------------------------
// Connexion.php
//--------------------------

'CONNEXION_LOGIN' => 'Identifiant',
'CONNEXION_PASSWORD' => 'Mot de passe',
'CONNEXION_CONNEXION' => 'Connexion',
'CONNEXION_ERROR' => 'Identifiant ou mot de passe invalide.',
'CONNEXION_ERROR_IDENTIFIANT_RQ' => 'Un identifiant est requis.',
'CONNEXION_ERROR_IDENTIFIANT_INV' => 'L\'identifiant est invalide.',
'CONNEXION_ERROR_PASSWORD_RQ' => 'Un mot de passe est requis.',


//--------------------------
// Recherche.php
//--------------------------

'SEARCH_SITUATION_LIBELLE' => 'Résultat de recherche pour',

//--------------------------
// MentionsLegales.php
//--------------------------

'LEGAL_NOTICES_OWNER' => 'Propriétaire du site',
'LEGAL_NOTICES_HOST_PROVIDER' => 'Hébergeur',
'LEGAL_NOTICES_DESIGN' => 'Conception du site',

//--------------------------
// Missions.php
//--------------------------

'MISSIONS_SHOW_NONE' => 'Il n\'y a pas de mission en cours.',
'MISSIONS_SHOW_DOWNLOAD' => 'Télécharger la fiche de liaison',
'MISSIONS_FORM_TITLE' => 'Ajouter une formation',
'MISSIONS_FORM_SUCCESS' => 'La mission saisie a bien été enregistrée.',
'MISSIONS_FORM_ERROR' => 'Une erreur est survenue lors de l\'enregistrement de la mission.',
'MISSIONS_FORM_ERROR_PROFESSEUR_NAME_RQ' => 'Le nom du professeur est requis.',
'MISSIONS_FORM_ERROR_PROFESSEUR_NAME_MAX' => 'Le nom du professeur ne peut dépasser 60 caractères.',
'MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_RQ' => 'L\'adresse e-mail du professeur est requise.',
'MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_INV' => 'L\'adresse e-mail du professeur est invalide.',
'MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_MAX' => 'L\'adresse e-mail du professeur ne peut dépasser 60 caractères.',
'MISSIONS_FORM_ERROR_MANAGER_NAME_RQ' => 'Le nom du chargé de TD est requis.',
'MISSIONS_FORM_ERROR_MANAGER_NAME_MAX' => 'Le nom du chargé de TD ne peut dépasser 60 caractères.',
'MISSIONS_FORM_ERROR_MANAGER_EMAIL_RQ' => 'L\'adresse e-mail du chargé de TD est requise.',
'MISSIONS_FORM_ERROR_MANAGER_EMAIL_INV' => 'L\'adresse e-mail du chargé de TD est invalide.',
'MISSIONS_FORM_ERROR_MANAGER_EMAIL_MAX' => 'L\'adresse e-mail du chargé ne peut dépasser 60 caractères.',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_RQ' => 'La date de début est requise.',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_RQ' => 'La date de fin est requise.',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_INV' => 'La date de début n\'est pas valide, elle doit être au moins égale à la date d\'aujourd\'hui et être au format "jj/mm/aaaa".',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_INV' => 'La date de fin n\'est pas valide, elle doit être supérieur à la date de début et être au format "jj/mm/aaaa".',
'MISSIONS_FORM_ERROR_MESSAGE' => 'Un message expliquant la mission est nécessaire.',
'MISSIONS_FORM_ERROR_LIAISON_FILE' => 'La fiche de liaison doit être au format "jpg", "jpeg", "png", "doc", "docx" ou "pdf".',
'MISSIONS_FORM_EXPLAINATION' => 'Vous pouvez ajouter facilement une mission sur le planning des missions, à l\'aide du formulaire ci-dessous', 
'MISSIONS_FORM_PROFESSOR_LAST_NAME_FIRST_NAME' => 'Enseignant - Nom & Prénom',
'MISSIONS_FORM_PROFESSOR_EMAIL' => 'Enseignant - Mail',
'MISSIONS_FORM_MANAGER_LAST_NAME_FIRST_NAME' => 'Chargé de TD - Nom & Prénom',
'MISSIONS_FORM_MANAGER_EMAIL' => 'Chargé de TD - Mail',
'MISSIONS_FORM_FROM' => 'Période du',
'MISSIONS_FORM_TO' => 'Au',
'MISSIONS_FORM_MESSAGE' => 'Message',
'MISSIONS_FORM_LIAISON_FILE' => 'Fiche de liaison',

//--------------------------
// 500.php
//--------------------------

'ERROR_500_SITUATION_TITLE' => 'Erreur 500',
'ERROR_500_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_500_CONTENT_TITLE' => '500',
'ERROR_500_CONTENT_TEXTE' => 'Notre service est momentanément indisponible.',

//--------------------------
// 404.php
//--------------------------

'ERROR_404_SITUATION_TITLE' => 'Erreur 404',
'ERROR_404_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_404_CONTENT_TITLE' => '404',
'ERROR_404_CONTENT_TEXTE' => 'La page demandée n\'est pas ou plus accessible.',

//--------------------------
// 403.php
//--------------------------

'ERROR_403_SITUATION_TITLE' => 'Erreur 403',
'ERROR_403_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_403_CONTENT_TITLE' => '403',
'ERROR_403_CONTENT_TEXTE' => 'Vos droits d\'accès ne permettent pas d\'accéder à cette ressource.',

//--------------------------
// 401.php
//--------------------------

'ERROR_401_SITUATION_TITLE' => 'Erreur 401',
'ERROR_401_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_401_CONTENT_TITLE' => '401',
'ERROR_401_CONTENT_TEXTE' => 'Une authentification est nécessaire pour accéder à la ressource.'
);