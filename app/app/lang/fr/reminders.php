<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Les mots de passes doivent être d'au moins six caractères et correspondre à la confirmation.",

	"user" => "Nous n'avons pas trouvé d'utilisateur avec cette adresse e-mail.",

	"token" => "Le jeton de restauration de mot de passe est invalide.",

	"sent" => "Le rappel de mot de passe a été envoyé!",

	"reset" => "Le mot de passe a été changé!",

);
