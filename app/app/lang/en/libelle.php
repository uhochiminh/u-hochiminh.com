<?php
return array(
//--------------------------
// GENERAL
//--------------------------

'GENERAL_SITENAME' => 'u-hochiminh.com',
'GENERAL_AUTHORS' => 'Designed and Developed by IUT Bordeaux 1 - <strong>DAWIN 1 Groupe 3</strong>',
'GENERAL_COPYRIGHT' => 'Copyright © 2014 - All Right Reserved - ',

//--------------------------
// META
//--------------------------

'META_DOMAIN' => 'u-hochiminh.com',
'META_DESCRIPTION' => 'Le pôle universitaire français (PUF) de Ho Chi Minh spécialité INFO prépare 5 formations au titre d\'expert en informatique et systèmes d\'information.',
'META_KEYWORDS' => '',
'META_AUTHOR' => 'IUT Bordeaux 1 - DAWIN 1 Groupe 3',
'META_CATEGORY' => 'IT Schools',
'META_COPYRIGHT' => 'Copyright © 2014 - All Right Reserved - u-hochiminh.com',

//--------------------------
// Init
//--------------------------

'INIT_TITLE' => 'Language/Langue',
'INIT_ENGLISHFLAG' => 'English Language',
'INIT_FRENCHFLAG' => 'Langue Française',
'INIT_FRENCHLIBELLE' => 'CHOISIR VOTRE LANGUE',
'INIT_ENGLISHLIBELLE' => 'CHOOSE YOUR LANGUAGE',

//--------------------------
// Position
//--------------------------

'POSITION_HOMEPAGE' => 'Homepage',
'POSITION_PRACTICAL_INFORMATION' => 'Practical Information',
'POSITION_TRAINING' => 'Training',
'POSITION_COMPANIES' => 'Companies',
'POSITION_CONTACT' => 'Contact',
'POSITION_EDIT' => 'Edition',
'POSITION_DISCONNECT' => 'Log out',
'POSITION_CONNEXION' => 'Log in',
'POSITION_SEARCH' => 'Search',
'POSITION_ACCESSIBILITY' => 'Accessibility',
'POSITION_LEGAL_NOTICES' => 'Legal Notices',
'POSITION_SITE_MAP' => 'Site map',
'POSITION_MISSIONS' => 'Missions',
'POSITION_500' => 'Error 500',
'POSITION_404' => 'Error 404',
'POSITION_401' => 'Error 401',
'POSITION_403' => 'Error 403',

//--------------------------
// Formulaire
//--------------------------

'FORM_SEND' => 'Send',
'FORM_DELETE' => 'Delete',

//--------------------------
// header.php
//--------------------------

'SEARCH_BUTTON' => 'Search',
'SEARCH_PLACEHOLDER' => 'Type in your research...',

//--------------------------
// footer.php
//--------------------------

'LOCATION_TITLE' => 'Our address',
'LOCATION_PHONE' => 'Phone',
'LOCATION_FAX' => 'Fax',

'LINKS_TITLE' => 'Links',
'LINKS_ACCESSIBILITY' => 'Accessibility',
'LINKS_CONNEXION' => 'Connection',
'LINKS_LEGAL_NOTICES' => 'Legal Notices',
'LINKS_SITE_MAP' => 'Site map',
'LINKS_CONTACT' => 'Contact',

'SOCIAL_NETWORKS_TITLE' => 'Social networks',
'SOCIAL_NETWORKS_RSS' => 'RSS Feed',
'SOCIAL_NETWORKS_FACEBOOK' => 'Facebook',
'SOCIAL_NETWORKS_TWITTER' => 'Twitter',

//--------------------------
// no-script.php
//--------------------------

'NOSCRIPT_explaination' => 'Your browser does not support javascript, thank you kindly <strong>update your browser</strong> to view the site\'s features.',
'NOSCRIPT_explication' => 'Votre navigateur ne supporte pas le javascript, merci de bien vouloir <strong>mettre à jour votre navigateur</strong> afin d\'accèder aux  fonctions du site.',
	
//--------------------------
// Formations.php
//--------------------------

'TRAINING_CONTENT_LICENCE' => 'Licence',
'TRAINING_CONTENT_LICENCE_1' => 'Licence 1',
'TRAINING_CONTENT_LICENCE_2' => 'Licence 2',
'TRAINING_CONTENT_LICENCE_3' => 'Licence 3',
'TRAINING_CONTENT_MASTER' => 'Master',
'TRAINING_CONTENT_MASTER_1' => 'Master 1',
'TRAINING_CONTENT_MASTER_2' => 'Master 2',
'TRAINING_CONTENT_PRESENTATION_TITLE' => 'Training',

//--------------------------
// InfosPratiques.php
//--------------------------

'PRACTICAL_INFORMATION_NOARTICLE' => 'There is no post in this practical information.',
'PRACTICAL_INFORMATION_READ_NEXT_BUTTON' => 'Read next',
'PRACTICAL_INFORMATION_POSTED_ON' => 'Posted on',
'PRACTICAL_INFORMATION_BY' => 'by',

//--------------------------
// Contact.php
//--------------------------

'CONTACT_ADDRESS' => 'Our Coordinates',
'CONTACT_FORM_TITLE' => 'Contact us',
'CONTACT_FORM_LAST_NAME_FIRST_NAME' => 'Last_name & First_name :',
'CONTACT_FORM_EMAIL' => 'Email',
'CONTACT_FORM_OBJECT' => 'Object',
'CONTACT_FORM_MESSAGE' => 'Message',
'CONTACT_FORM_ERROR_YOUR_NAME_RQ' =>'Your name is required.',
'CONTACT_FORM_ERROR_YOUR_NAME_MAX' =>'Your name can not exceed 60 characters.',
'CONTACT_FORM_ERROR_YOUR_EMAIL_RQ' =>'You must enter an email address.',
'CONTACT_FORM_ERROR_YOUR_EMAIL_INV' =>'Your email address is invalid.',
'CONTACT_FORM_ERROR_SUBJECT_RQ' =>'You must enter a subject.',
'CONTACT_FORM_ERROR_SUBJECT_MAX' =>'Your subject can not exceed 60 characters.',
'CONTACT_FORM_ERROR_MESSAGE_RQ' =>'You must enter a message.',

//--------------------------
// Connexion.php
//--------------------------

'CONNEXION_LOGIN' => 'Login',
'CONNEXION_PASSWORD' => 'Password',
'CONNEXION_CONNEXION' => 'Connexion',
'CONNEXION_ERROR' => 'Login or password disabled',
'CONNEXION_ERROR_IDENTIFIANT_RQ' => 'An ID is required.',
'CONNEXION_ERROR_IDENTIFIANT_INV' => 'The ID is not valid.',
'CONNEXION_ERROR_PASSWORD_RQ' => 'A password is required.',

//--------------------------
// MentionsLegales.php
//--------------------------

'LEGAL_NOTICES_OWNER' => 'Owner of the Web site',
'LEGAL_NOTICES_HOST_PROVIDER' => 'Host provider',
'LEGAL_NOTICES_DESIGN' => 'Web site design',

//--------------------------
// Missions.php
//--------------------------

'MISSIONS_SHOW_NONE' => 'There is no current mission',
'MISSIONS_SHOW_DOWNLOAD' => 'Download the index form of connection',
'MISSIONS_FORM_TITLE' => 'Add a training',
'MISSIONS_FORM_SUCCESS' => 'The mission typed has been registered successfully.',
'MISSIONS_FORM_ERROR' => 'An error arose during the saving of the mission.',
'MISSIONS_FORM_ERROR_PROFESSEUR_NAME_RQ' => 'The professor\'s name is required.',
'MISSIONS_FORM_ERROR_PROFESSEUR_NAME_MAX' => 'The professor\'s name cannot exceed 60 characters.',
'MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_RQ' => 'The professor\'s email address is required.',
'MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_INV' => 'The professor\'s email address is not valid.',
'MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_MAX' => 'The professor\'s email address cannot exceed 60 characters.',
'MISSIONS_FORM_ERROR_MANAGER_NAME_RQ' => 'The TD manager\'s name is required.',
'MISSIONS_FORM_ERROR_MANAGER_NAME_MAX' => 'The TD manager\'s name cannot exceed 60 characters.',
'MISSIONS_FORM_ERROR_MANAGER_EMAIL_RQ' => 'The TD manager\'s email address is required.',
'MISSIONS_FORM_ERROR_MANAGER_EMAIL_INV' => 'The TD manager\'s email address is not valid.',
'MISSIONS_FORM_ERROR_MANAGER_EMAIL_MAX' => 'The TD manager\'s email address cannot exceed 60 characters.',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_RQ' => 'The beginning date is required',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_RQ' => 'The ending date is required.',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_INV' => 'The beginning date is not valid, the date must be equal or later than the today\'s date and it must be for the format "dd/mm/yy".',
'MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_INV' => 'The beginning date is not valid, the date must be later than the beginning date and it must be for the format "dd/mm/yy".',
'MISSIONS_FORM_ERROR_MESSAGE' => 'A message explaining the mission is necessary.',
'MISSIONS_FORM_ERROR_LIAISON_FILE' => 'The index form of connection has to be for the format "jpg", "jpeg", "png", "doc", "docx" ou "pdf".',
'MISSIONS_FORM_EXPLAINATION' => 'You can add easily a mission on the schedule of the missions, by means of the form below.', 
'MISSIONS_FORM_PROFESSOR_LAST_NAME_FIRST_NAME' => 'Teacher - Last_name & First_name :',
'MISSIONS_FORM_PROFESSOR_EMAIL' => 'Teacher - Email :',
'MISSIONS_FORM_MANAGER_LAST_NAME_FIRST_NAME' => 'Manager - Last_name & First_name :',
'MISSIONS_FORM_MANAGER_EMAIL' => 'Manager - Email :',
'MISSIONS_FORM_FROM' => 'Period of :',
'MISSIONS_FORM_TO' => 'To :',
'MISSIONS_FORM_MESSAGE' => 'Message :',
'MISSIONS_FORM_LIAISON_FILE' => 'Liaison file :',

//--------------------------
// 500.php
//--------------------------

'ERROR_500_SITUATION_TITLE' => 'Error 500',
'ERROR_500_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_500_CONTENT_TITLE' => '500',
'ERROR_500_CONTENT_TEXTE' => 'Our service is for a moment unavailable.',

//--------------------------
// 404.php
//--------------------------

'ERROR_404_SITUATION_TITLE' => 'Error 404',
'ERROR_404_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_404_CONTENT_TITLE' => '404',
'ERROR_404_CONTENT_TEXTE' => 'The wanted page is not or more accessible',

//--------------------------
// 403.php
//--------------------------

'ERROR_403_SITUATION_TITLE' => 'Error 403',
'ERROR_403_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_403_CONTENT_TITLE' => '403',
'ERROR_403_CONTENT_TEXTE' => 'Your access rights do not allow to reach this resource.',

//--------------------------
// 401.php
//--------------------------

'ERROR_401_SITUATION_TITLE' => 'Error 401',
'ERROR_401_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_401_CONTENT_TITLE' => '401',
'ERROR_401_CONTENT_TEXTE' => 'UAn authentification is necessary to reach the resource.'
);