<?php
class NewsController extends BaseController{
    /* Affichage de la news  */
	public function post($language, $id){
		// Définition de la langue :
		if(!$this->setupLanguage($language)){ App::abort(404); };
		
		// Récupération de l'id de la news à afficher :
		$id = explode("-", $id);
		if(!isset($id[0]) or empty($id[0])){ App::abort(404); }
		
		// Variables :
			// News (dans un try catch pour gérer les erreurs) :
			try{
				$languages = Language::where('_url', '=', $language)->firstOrFail();
	            $page =  Pages::where('_language', $languages->_id)->where("_name", "=", "news")->get()->first();
				$new = News::whereRaw('_langue = '.$languages->_id.' and _id='.$id[0])->firstOrFail();
			}catch(Exception $e){ App::abort(404); }
			$data = array(
				"situation" => array(
	                "title" => $page->_title,
	                "description" => $page->_description
	            ),
	            "position" => array(
	                $page->_title => URL::to(Request::segment(1).'/news/'),
	                $new->_title => ""
				)
			);
		
		// Renvoi de la vue avec la news :
		$page = $this->layout->content = View::make('pages.news.post', array("data"=>$data, "page"=>$page, "new"=>$new));
		return Response::make($page);
	}
	
	/* Affichage des news  */
	public function news($language){
		// Définition de la langue :
		if(!$this->setupLanguage($language)){ App::abort(404); };
		
		// Variables :
			// News (dans un try catch pour gérer les erreurs) :
			try{
				$languages = Language::where('_url', '=', $language)->firstOrFail();
	            $page =  Pages::where('_language', $languages->_id)->where("_name", "=", "news")->get()->first();
				$news = News::where('_langue', '=', $languages->_id)->get();
				$news = $news->sortBy('_date')->take(15);
			}catch(Exception $e){ App::abort(500); }
			// Details :
			$data = array(
				"situation" => array(
	                "title" => $page->_title,
	                "description" => $page->_description
	            ),
	            "position" => array(
	                $page->_title => ""
				)
			);

		// Renvoi de la vue avec les news :
		$page = $this->layout->content = View::make('pages.news.news', array("data"=>$data, "page"=>$page, "news"=>$news));
		return Response::make($page);
	}
    
	// Création du fichier XML : 
    public function rss($language){
		// Définition de la langue :
        if(!$this->setupLanguage($language)){ App::abort(404); };

		// Variables :
			// Details :
			$header = '<?xml version="1.0" encoding="UTF-8"?>';
			// News (dans un try catch pour gérer les erreurs) :
			try{
				$languages = Language::where('_url', '=', $language)->firstOrFail();
				$news = News::where('_langue', '=', $languages->_id)->get();
				$news = $news->sortBy('_date')->take(30);
			}catch(Exception $e){ App::abort(404); }
		
		// Renvoi de la vue avec les news et l'en-tête :
		$page = $this->layout->content = View::make('pages.news.fluxXML', array("news"=>$news, "header" => $header));
		return Response::make($page, '200')->header('Content-Type', 'text/xml');
    }
} 