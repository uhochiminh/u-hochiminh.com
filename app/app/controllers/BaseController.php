<?php

class BaseController extends Controller {
    protected $layout = 'layout.master';
    protected static $languages = array('en', 'fr');
    protected static $language = 'en';

	protected function setupLayout()	{
		if (!is_null($this->layout)){
			// récupération des paramétres en base :
			$settings = array();
			foreach (Settings::all() as $set) { $settings[$set['_key']] = $set['_value']; }
			View::share('settings', $settings);
			
			// affichage du layout :
			$this->layout = View::make($this->layout);
		}
	}
    public static function setupLanguage($lang){
        $retour = false;
        
        if(!empty($lang) && in_array($lang, self::$languages)){
            self::$language = $lang;
            $retour = true;
        }
    	App::setLocale($lang);
    	
    	return $retour;
    }
    
    // GETTERS :
    public static function getLanguage(){
    	return self::$language;
    }
}
