<?php
class UsersController extends BaseController{
	// Gestion de l'utilisateur : 
	public function user($language){
		// Définition de la langue :
		if(!$this->setupLanguage($language)){ App::abort(404); };
		
		// Redirection en fonction de l'état de l'utilisateur (conectée ou pas) :
		if (Auth::check()){ return Redirect::to(URL::to($language.'/home')); }
		return Redirect::to(URL::to($language.'/user/login'));
	}
	
	// Connexion de l'utilisateur : 
	public function login($language){
		// Définition de la langue :
		if(!$this->setupLanguage($language)){ App::abort(404); };
		
        try{
            $languages = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $languages->_id)->where("_name", "=", "user/login")->get()->first();
        }catch(Exception $e){ App::abort(500); }
		
		// Redirection de l'utilisateur s'il est déjà connecté :
		if (Auth::check()){ return Redirect::to(URL::to($language.'/home')); }
		
		// Variables :
			// Details :
	        $data = array(
	            "situation" => array(
	                "title" => $page->_title,
	                "description" => $page->_description
	            ),
	            "position" => array(
	                $page->_title => ""
	            )
	        );
			// Message : 
			$message = "";
			// Inputs :
			$login ="";
			$password ="";
		
		// Envoi du formulaire de connexion :
		if(count(Input::all()) > 0){
			// récupération des inputs :
			$login = htmlentities(Input::get('your-login'));
			$password = htmlentities(Input::get('your-password'));

			// validation des formulaire :
			$validator = Validator::make(
			    array( 'email' => $login, 'password' => $password ),
			    array( 'email' => 'required|email', 'password' => 'required' ),
			    array(
					'email.required' => Lang::get('libelle.CONNEXION_ERROR_IDENTIFIANT_RQ'),
					'email.email' => Lang::get('libelle.CONNEXION_ERROR_IDENTIFIANT_INV'),
					'password.required' => Lang::get('libelle.CONNEXION_ERROR_PASSWORD_RQ')
				)
			);
			
			// retour en cas d'erreur :
			if($validator->fails()) { $message = $validator->messages(); } 
			else {
				// connexion en cas de succès :
				$auth = Users::where('_email', '=', $login)->where('_password', '=', md5($password))->first();
                
				// si utilisateur trouvé, redirection :
				if($auth){
            		Auth::login($auth);
                    Auth::user()->_ipLastConnection = Request::getClientIp(true);
                    Auth::user()->_dateLastConnection = date("Y-m-d H:i:s");
                    Auth::user()->save();
					return Redirect::to(URL::to($language.'/home'));
				}
				// sinon affichage d'erreur :
				else{ $message = Lang::get("libelle.CONNEXION_ERROR"); }
				
			}
		}
		
		// Renvoi de la vue avec les message(s) d'erreurs :
		$page = $this->layout->content = View::make('pages.users.login', array(	"data"=>$data,
																				"page"=>$page,
																				"message"=>$message));
		return Response::make($page, '200');
	}
	
	// Déconnexion de l'utilisateur : 
	public function logout($language){
		// Définition de la langue :
		if(!$this->setupLanguage($language)){ App::abort(404); };
		
		// Deconnexion
		if (Auth::check()){
		    Auth::logout();
        }
	
		// Redirection sur la page d'acceuil :
		return Redirect::to(URL::to($language.'/home'));
	}
} 