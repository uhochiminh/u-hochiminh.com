<?php

class SearchController extends BaseController{
    
    // Affiche le résultat de la recherche de l'utilisateur :
    public function search($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };
		
		// VARIABLES :
			// Pages :
	        try{
	            $languages = Language::where('_url', '=', $language)->firstOrFail();
	            $page =  Pages::where('_language', $languages->_id)->where("_name", "=", "search")->get()->first();
	        }catch(Exception $e){ App::abort(500); }
			
			// Request :
			$accentARemplacer = array(	'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
			                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
			                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
			                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
			                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
			$request = trim(Input::get("q"));
			$requestWithoutAccent = strtr( $request, $accentARemplacer);
			$requestWithAccent = $request;
			$request_explode = array_unique(array_merge(explode(" ", $requestWithoutAccent), explode(" ", $requestWithAccent)));

			// Details :
	        $data = array(
	            "situation" => array(
	                "title" => $page->_title,
	                "description" => $page->_description
	            ),
	            "position" => array(
	                $page->_title => " ",
					$request => ""
	            )
	        );
			
		//
		$results = array();
		
		if(count($request_explode)>0){
			foreach ($request_explode as $key => $value) {
				$pages = Pages::where("_keywords", "LIKE", '%'.$value.'%')
							->orWhere('_title', "LIKE", '%'.$value.'%')
							->orWhere('_name', "LIKE", '%'.$value.'%')
							->orWhere('_description', "LIKE", '%'.$value.'%')
							->get();
				$news = News::where('_langue', '=', $languages->_id)
							->where("_extract", "LIKE", '%'.$value.'%')
							->orWhere('_content', "LIKE", '%'.$value.'%')
							->orWhere('_title', "LIKE", '%'.$value.'%')
							->get();
				if(count($pages)>0){
					$results["pages"] = $pages;
				}
				if(count($news)>0){ $results["news"] = $news; }
			}
		}
		else{ App::abort(404); }
		
        $this->layout->content =  View::make('pages.search.search', array('data'=>$data, "page"=>$page, "language"=>$languages, 'request' => $request, 'results' => $results));
    }
} 