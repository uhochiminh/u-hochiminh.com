<?php

class MessageController extends BaseController{
    /* Méthode génération vue page langue */
    public function langue(){
    	$language = "fr";
    	try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "language")->get()->first();
        }catch(Exception $e){ App::abort(500); }
		
		$this->layout->content = View::make('pages.langue.langue', array("page"=>$page));
    }

    /* Méthode génération vue page accueil */
    public function accessibility($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "accessibility")->get()->first();
        }catch(Exception $e){ App::abort(404); }

        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );

        $this->layout->content = View::make('pages.accessibility.accessibility', array("data"=>$data, "page"=>$page));
    }

    /* Méthode génération vue page contact */
    public function contact($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "contact")->get()->first();
        }catch(Exception $e){ App::abort(500); }
        
        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );
        // Form :
        $inputs = Input::all();
        // Resultat :
        $result = array();
    
        // Envoi du formulaire d'ajout de mission :	
        if(count(Input::all()) > 0){
            // validation des formulaire :
            $validator = Validator::make(
                $inputs,
                array(  'your-name' => 'required|max:60', 
                        'your-email' => 'required|email', 
                        'your-subject' => 'required|max:120',
                        'your-message' => 'required'
                     ),
                        
                array(  'your-name.required' => Lang::get('libelle.CONTACT_FORM_ERROR_YOUR_NAME_RQ'), 
                        'your-name.max' => Lang::get('libelle.CONTACT_FORM_ERROR_YOUR_NAME_MAX'), 
                        'your-email.required' => Lang::get('libelle.CONTACT_FORM_ERROR_YOUR_EMAIL_RQ'),
                        'your-email.email' => Lang::get('libelle.CONTACT_FORM_ERROR_YOUR_EMAIL_INV'),
                        'your-subject.required' => Lang::get('libelle.CONTACT_FORM_ERROR_SUBJECT_RQ'),
                        'your-subject.max' => Lang::get('libelle.CONTACT_FORM_ERROR_SUBJECT_MAX'),
                        'your-message.required' => Lang::get('libelle.CONTACT_FORM_ERROR_MESSAGE_RQ')
                      )
            );
            
            // retour en cas d'erreur :
            if($validator->fails()) { $result["errors"] = $validator->messages(); } 
            else {
            	// Récupération des paramètres :
				foreach (Settings::all() as $set) { $settings[$set['_key']] = $set['_value']; }
			
                // traitement du texte en paragraphe
                $texte = explode('<br />', nl2br(trim(htmlentities($inputs['your-message']))));
                $inputs['your-message'] = "";
                foreach ($texte as $key => $value) { $inputs['your-message'] .= "<p>".$value."</p>"; }
                
                // envoie du mail :
                $your_name = trim(htmlentities($inputs['your-name']));
                $your_email = trim(htmlentities($inputs['your-email']));
                $your_subject = trim($inputs['your-subject']);
                $your_message = $inputs['your-message'];
                
				$mail=array_merge($inputs, array(
					"admin_mail" => $settings['mail'],
					"admin_name" => $settings['domain'],
					"domain" => $settings['domain']
				));
				
				
                Mail::send('emails.contact_admin', array("data"=>$mail), function($message) use ($mail) {
                	$message->to($mail['admin_mail'], $mail['admin_name'])->subject("Contact :".$mail['your-subject']);
                });
                
                Mail::send('emails.contact_user', array("data"=>$mail), function($message) use ($mail){
                	$message->to($mail['your-email'], $mail['your-name'])->subject("Contact :".$mail['your-subject']);
                });
                
				
				foreach ($inputs as $key => $value) {$inputs[$key]="";}
				$result["success"] = Lang::get("libelle.CONTACT_FORM_SUCCESS");
            }
        }
        else{ $inputs = null; }

        $this->layout->content = View::make('pages.contact.contact', array("data"=>$data, "page"=>$page, "inputs" => $inputs, "result" => $result));
    }

    /* Méthode génération vue page formations */
    public function education($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "educations")->get()->first();
        }catch(Exception $e){ App::abort(500); }
        
        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );
		
        $this->layout->content = View::make('pages.educations.educations', array("data"=>$data, "page"=>$page));
    }

    /* Méthode de génération d'une formation */
    public function education_level ($language, $id){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page_sublevel =  Pages::where('_language', $language->_id)->where("_name", "=", "educations")->get()->first();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "education/".$id)->get()->first();
        }catch(Exception $e){ App::abort(500); }

        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page_sublevel->_title => URL::to(Request::segment(1).'/educations/'),
                $page->_title => ""
            )
        );
        
        $this->layout->content = View::make('pages.educations.'.$id, array("data"=>$data, "page"=>$page));
    }

    /* Méthode génération vue page entreprise */
    public function entreprise($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "entreprises")->get()->first();
        }catch(Exception $e){ App::abort(500); }
		
		// Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );		
		
        $this->layout->content = View::make('pages.entreprises.entreprises', array("data"=>$data, "page"=>$page));
    }

    /* Méthode génération vue page accueil */
    public function home($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "home")->get()->first();
        }catch(Exception $e){ App::abort(500); }
        
        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );
		
        $this->layout->content = View::make('pages.home.home', array("data"=>$data, "page"=>$page));
    }

    /* Méthode génération vue page mentions légales */
    public function legalnotice($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "legal-notice")->get()->first();
        }catch(Exception $e){ App::abort(500); }
	
        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );

        $this->layout->content = View::make('pages.legalnotice.legalnotice', array("data"=>$data, "page"=>$page));
    }

    /* Méthode génération vue page plan du site */
    public function siteMap($language){
        if(!$this->setupLanguage($language)){ App::abort(404); };

        try{
            $language = Language::where('_url', '=', $language)->firstOrFail();
            $page =  Pages::where('_language', $language->_id)->where("_name", "=", "sitemap")->get()->first();
			$pages = Pages::where('_language', $language->_id)->where('_dependsOn', "=", NULL)->where("_public", "=", "1")->orderBy('_priority', 'DESC')->get();
        	$news = News::where('_langue', $language->_id)->orderBy('_date', 'DESC')->get();
		}catch(Exception $e){ App::abort(500); }
        
        // Details :
        $data = array(
            "situation" => array(
                "title" => $page->_title,
                "description" => $page->_description
            ),
            "position" => array(
                $page->_title => ""
            )
        );

        $this->layout->content = View::make('pages.sitemap.sitemap', array("data"=>$data, "page"=>$page, "pages"=>$pages, "news" => $news));
    }

    /* Méthode génération vue page xlm plan du site */
    public function siteMap_xml(){
        try{
            $page =  Pages::where("_name", "=", "sitemap")->get()->first();
            $pages = Pages::where('_dependsOn', "=", NULL)->where("_public", "=", "1")->orderBy('_priority', 'DESC')->get();
            $news = News::orderBy('_date', 'DESC')->get();
        }catch(Exception $e){ App::abort(500); }
        
        $header = '<?xml version="1.0" encoding="UTF-8"?>';

        // Renvoi de la vue avec les news et l'en-tête :
        $page = $this->layout->content = View::make('pages.sitemap.sitemap_xml', array("news"=>$news, "pages"=>$pages, "header" => $header));
        return Response::make($page, '200')->header('Content-Type', 'text/xml');

    }
} 
