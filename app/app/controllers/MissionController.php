<?php

class MissionController extends BaseController{
    /* Affichage des missions : */
    public function missions($language){
        // Définition de la langue :
		if(!$this->setupLanguage($language)){ App::abort(404); };
        // Redirection de l'utilisateur s'il est pas connecté et s'il n'est pas Admin:
        if (!Auth::check()){ return Redirect::to(URL::to($language.'/home')); }
		
		// Variables :
			// Missions :
			$languages = Language::where('_url', '=', $language)->firstOrFail();
	        $page =  Pages::where('_language', $languages->_id)->where("_name", "=", "missions")->get()->first();
			$missions = Missions::where("_dateEnd", ">=", date("Y-m-d 00:00:00"))->orderBy('_dateStart')->orderBy('_dateEnd')->get();
			// Details :
			$data = array(
				"situation" => array(
	                "title" => $page->_title,
	                "description" => $page->_description
	            ),
	            "position" => array(
	                $page->_title => ""
				)
			);
			
		// Affichage de la vue :
        $this->layout->content = View::make('pages.missions.missions', array("data"=>$data, "page"=>$page, "missions" =>$missions));
    }
    
	/* Ajout d'une mission */
    public function addMission($language){
        // Définition de la langue :
        if(!$this->setupLanguage($language)){ App::abort(404); };
		
        // Redirection de l'utilisateur s'il est pas connecté et s'il n'est pas Admin:
        if (!Auth::check()){ return Redirect::to(URL::to($language.'/home')); }
        elseif (Auth::User()->_level != "admin"){ return Redirect::to(URL::to($language.'/missions')); }
				
		// Variables :
			// Missions :
			$languages = Language::where('_url', '=', $language)->firstOrFail();
	        $page =  Pages::where('_language', $languages->_id)->where("_name", "=", "missions")->get()->first();
			$missions = Missions::where("_dateEnd", ">=", date("Y-m-d 00:00:00"))->orderBy('_dateStart')->orderBy('_dateEnd')->get();
			// Form :
			$inputs = Input::all();
			$result = array();
			// Details :
			$data = array(
				"situation" => array(
	                "title" => $page->_title,
	                "description" => $page->_description
	            ),
	            "position" => array(
	                $page->_title => ""
				)
			);
			
			
		// Envoi du formulaire d'ajout de mission :
		if(count(Input::all()) > 0){
			// validation des formulaire :
			$validator = Validator::make(
			    $inputs,
			    array(	'professor-name' => 'required|max:60', 
			    		'professor-email' => 'required|email|max:60', 
			    		'manager-name' => 'required|max:60',
			    		'manager-email' => 'required|email|max:60', 
			    		'professor-mission-start' => 'required|date_format:"Y-m-d"|after:"now -1 day"',
			    		'professor-mission-end' => 'required|date_format:"Y-m-d"|after:"'.strtotime("professor-mission-start").'"',
			    		'your-message' => 'required',
			    		'liaison-file' => 'mimes:jpg,jpeg,png,doc,docx,pdf'	),
			    		
			    array(	'professor-name.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_NAME_RQ'), 
			    		'professor-name.max' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_NAME_MAX'), 
			    		'professor-email.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_RQ'),
			    		'professor-email.email' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_INV'),
			    		'professor-email.max' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_EMAIL_MAX'), 
			    		'manager-name.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_MANAGER_NAME_RQ'),
			    		'manager-name.max' => Lang::get('libelle.MISSIONS_FORM_ERROR_MANAGER_NAME_MAX'),
			    		'manager-email.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_MANAGER_EMAIL_RQ'), 
			    		'manager-email.email' => Lang::get('libelle.MISSIONS_FORM_ERROR_MANAGER_EMAIL_INV'), 
			    		'manager-email.max' => Lang::get('libelle.MISSIONS_FORM_ERROR_MANAGER_EMAIL_MAX'), 
			    		'professor-mission-start.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_RQ'),
			    		'professor-mission-start.date_format' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_INV'),
			    		'professor-mission-start.after' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_START_INV'),
			    		'professor-mission-end.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_RQ'),
			    		'professor-mission-end.date_format' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_INV'),
			    		'professor-mission-end.after' => Lang::get('libelle.MISSIONS_FORM_ERROR_PROFESSEUR_MISSION_END_INV'),
			    		'your-message.required' => Lang::get('libelle.MISSIONS_FORM_ERROR_MESSAGE'),
			    		'liaison-file.mimes' => Lang::get('libelle.MISSIONS_FORM_ERROR_LIAISON_FILE'),	)
			);
			
			// retour en cas d'erreur :
			if($validator->fails()) { $result["errors"] = $validator->messages(); } 
			else {
				// traitement du texte en paragraphe
				$texte = explode('<br />', nl2br(trim(htmlentities($inputs['your-message']))));
				$inputs['your-message'] = "";
				foreach ($texte as $key => $value) { $inputs['your-message'] .= "<p>".$value."</p>"; }
				
				// ajout en cas de succès :
				$mission = new Missions;
				$mission->_author = 1;
				$mission->_dateStart = trim(htmlentities($inputs['professor-mission-start']));
				$mission->_dateEnd = trim(htmlentities($inputs['professor-mission-end']));
				$mission->_profName = trim(htmlentities($inputs['professor-name']));
				$mission->_profMail = trim(htmlentities($inputs['professor-email']));
				$mission->_ctdName = trim(htmlentities($inputs['manager-name']));
				$mission->_ctdMail = trim(htmlentities($inputs['manager-email']));
				$mission->_description = $inputs['your-message'];
				if(Input::hasFile('liaison-file')){
		            $filename = date("YmdHis")."-".Input::file('liaison-file')->getClientOriginalName();
					Input::file('liaison-file')->move(dirname("app/storage/uploads/missions/")."/missions/", $filename);
					$mission->_file = "storage/uploads/missions/".$filename;
		        }
				
				// enrgistrement en base de données.
				if($mission->save()){
					// actualisation des missions :
					$missions = Missions::where("_dateEnd", ">=", date("Y-m-d 00:00:00"))->orderBy('_dateStart')->orderBy('_dateEnd')->get();
					
					// vidage du formulaire  :
					foreach ($inputs as $key => $value) {$inputs[$key]="";}
					
					// affichage du message de reussite :
					$result["success"] = Lang::get("libelle.MISSIONS_FORM_SUCCESS");
				}
				// sinon affichage d'erreur :
				else{ $result["errors"] = Lang::get("libelle.MISSIONS_FORM_ERROR"); }
				
			}
		}	
			
		
		// Affichage de la vue avec retour du résultat de l'ajout :
        $this->layout->content = View::make('pages.missions.missions', array(	"data"=>$data, 
        																		"page"=>$page, 
        																		"missions" =>$missions, 
        																		"inputs" => $inputs,
        																		"result" => $result		));
    }
	
	/* Supression d'une mission */
	public function delMission($id){
		
	}
    
	/* Modification d'une mission */
	public function modMission($id){
		
	}
	
    /* Récupere le ficheir de liasion de la mission choisi */
	public function getFileMission($language, $id){
        if(!$this->setupLanguage($language)){ App::abort(404); };
		
        // Redirection de l'utilisateur s'il est pas connecté :
		if (!Auth::check()){ return Redirect::to(URL::to($language.'/home')); }
		
		try {
			$mission = Missions::where('_id', "=", $id)->firstOrFail();
			return Response::download(app_path()."/".$mission->_file);
		}
		catch(Exception $e){ App::abort(404); }
    }
} 