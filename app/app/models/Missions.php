<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Missions extends Eloquent implements UserInterface, RemindableInterface {
		
	use UserTrait, RemindableTrait;

	protected $table = 'missions';
	protected $primaryKey = '_id';
	public $timestamps = false;

	// Relations :
	public function user(){
		return $this->belongsTo('Users', '_author');
	}
}
