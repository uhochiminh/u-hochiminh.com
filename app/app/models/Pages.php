<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Pages extends Eloquent implements UserInterface, RemindableInterface {
		
	use UserTrait, RemindableTrait;	
	
	protected $table = 'page';
	protected $primaryKey = '_id';
	public $timestamps = false;

    public function language(){
        return $this->belongsTo('Language', '_language');
    }
    public function modules(){
        return $this->hasMany('Modules', "_page");
    }
    public function parentPage(){
        return $this->hasMany('Pages', "_dependsOn");
    }
}
