<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Modules extends Eloquent implements UserInterface, RemindableInterface {
		
	use UserTrait, RemindableTrait;
	
	protected $table = 'module';
	protected $primaryKey = '_id';
	public $timestamps = false;

	// Relations :
	public function language(){
		return $this->belongsTo('Language', '_language');
	}
	public function page(){
		return $this->belongsTo('Pages', '_page');
	}
}
