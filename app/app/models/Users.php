<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Users extends Eloquent implements UserInterface, RemindableInterface {
		
	use UserTrait, RemindableTrait;

	protected $table = 'user';
	protected $primaryKey = '_id';
	protected $hidden = array('_password');
	public $timestamps = false;
	
	 public function getAuthIdentifier() {
        return $this->_id;
    }

    public function getAuthPassword() {
        return $this->_password;
    }
	

}
