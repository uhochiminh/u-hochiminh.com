<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Settings extends Eloquent implements UserInterface, RemindableInterface {
		
	use UserTrait, RemindableTrait;

	protected $table = 'settings';
	protected $primaryKey = '_id';
	public $timestamps = false;



}
