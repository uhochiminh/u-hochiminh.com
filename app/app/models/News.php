<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class News extends Eloquent implements UserInterface, RemindableInterface {
		
	use UserTrait, RemindableTrait;
	
	protected $table = 'news';
	protected $primaryKey = '_id';
	public $timestamps = false;

	// Relations :
	public function language(){
		return $this->belongsTo('Language', '_langue');
	}
	public function user(){
		return $this->belongsTo('Users', '_author');
	}
	
}
