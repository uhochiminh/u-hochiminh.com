<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

App::error(function(Exception $exception, $code)
{
	BaseController::setupLanguage(Request::segment(1));
    switch ($code)
    {
        case 401:
			$data = array(
				"situation" => array(
					"title" => Lang::get('libelle.ERROR_401_SITUATION_TITLE'),
					"description" => Lang::get('libelle.ERROR_401_SITUATION_TXT')
				),
				"position" => array(
					"ERREUR 401"=>""
				),
				"content" => array(
					"title" => Lang::get('libelle.ERROR_401_CONTENT_TITLE'),
					"texte" => Lang::get('libelle.ERROR_401_CONTENT_TEXTE'),
				)
			);
			
			return Response::view('pages.errors.error', array("data" => $data), 401);
            
        case 403:
            $data = array(
				"situation" => array(
					"title" => Lang::get('libelle.ERROR_403_SITUATION_TITLE'),
					"description" => Lang::get('libelle.ERROR_403_SITUATION_TXT')
				),
				"position" => array(
					"ERREUR 403"=>""
				),
				"content" => array(
					"title" => Lang::get('libelle.ERROR_403_CONTENT_TITLE'),
					"texte" => Lang::get('libelle.ERROR_403_CONTENT_TEXTE'),
				)
			);
			return Response::view('pages.errors.error', array("data" => $data), 403);
            
        case 404:
            $data = array(
				"situation" => array(
					"title" => Lang::get('libelle.ERROR_404_SITUATION_TITLE'),
					"description" => Lang::get('libelle.ERROR_404_SITUATION_TXT')
				),
				"position" => array(
					"ERREUR 404"=>""
				),
				"content" => array(
					"title" => Lang::get('libelle.ERROR_404_CONTENT_TITLE'),
					"texte" => Lang::get('libelle.ERROR_404_CONTENT_TEXTE'),
				)
			);
			return Response::view('pages.errors.error', array("data" => $data), 404);

        case 500:
            $data = array(
				"situation" => array(
					"title" => Lang::get('libelle.ERROR_500_SITUATION_TITLE'),
					"description" => Lang::get('libelle.ERROR_500_SITUATION_TXT')
				),
				"position" => array(
					"ERREUR 500"=>""
				),
				"content" => array(
					"title" => Lang::get('libelle.ERROR_500_CONTENT_TITLE'),
					"texte" => Lang::get('libelle.ERROR_500_CONTENT_TEXTE'),
				)
			);
			//return Response::view('pages.errors.error', array("data" => $data), 500);
        
        default:
            $data = array(
				"situation" => array(
					"title" => Lang::get('libelle.ERROR_500_SITUATION_TITLE'),
					"description" => Lang::get('libelle.ERROR_500_SITUATION_TXT')
				),
				"position" => array(
					"ERREUR 500"=>""
				),
				"content" => array(
					"title" => Lang::get('libelle.ERROR_500_CONTENT_TITLE'),
					"texte" => Lang::get('libelle.ERROR_500_CONTENT_TEXTE'),
				)
			);
			//return Response::view('pages.errors.error', array("data" => $data), 500);
    }
});
/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';
