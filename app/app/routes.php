<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

// sitemap.xml
Route::get('/sitemap.xml', 'MessageController@siteMap_xml');

// Language :
Route::get('/', 'MessageController@langue');

// Home :
Route::get('/{language}/', function($language){ return Redirect::to(URL::to($language.'/home')); });
Route::get('/{language}/home', 'MessageController@home');

// Entreprises :
Route::get('/{language}/entreprises', 'MessageController@entreprise');

// Education :
Route::get('/{language}/educations', 'MessageController@education');
Route::get('/{language}/education/{id}', 'MessageController@education_level');

// News :
Route::get('/{language}/news',  'NewsController@news');
Route::get('/{language}/news/rss', 'NewsController@rss');
Route::get('/{language}/news/{id}', 'NewsController@post');

// Contact :
Route::get('/{language}/contact', 'MessageController@contact');
Route::post('/{language}/contact', 'MessageController@contact');

// User :
Route::get('/{language}/user', 'UsersController@user');
Route::get('/{language}/user/login', 'UsersController@login');
Route::post('/{language}/user/login', 'UsersController@login');
Route::get('/{language}/user/logout', 'UsersController@logout');

// Missions :
Route::get('/{language}/missions', 'MissionController@missions');
Route::post('/{language}/missions', 'MissionController@addMission');
Route::get('/{language}/mission/{id}', 'MissionController@getFileMission');

// Accessibility :
Route::get('/{language}/accessibility', 'MessageController@accessibility');

// Legal-Notice :
Route::get('/{language}/legal-notice', 'MessageController@legalnotice');

// Sitemap :
Route::get('/{language}/sitemap', 'MessageController@siteMap');

// Search :
Route::get('/{language}/search', 'SearchController@search');

// Admin :
Route::get('/admin/...', function(){});

// Others :
Route::controller('','BaseController');
