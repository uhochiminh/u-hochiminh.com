<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

<div id="formation" class="content">
    <div class="situation">
        <h2>2nd year of university</h2>
        <h3>Discover our university level license 2</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Home</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Training</a>
		<i class="fa fa-angle-right"></i>
		L2
	</div>
        
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-pratic-lesson.png" />
                <p>
                    The first two years of undifferentiated courts(yards) allow <br>
                    To the students to obtain a first professional university degree: <br>
                    The DUT(TWO-YEAR TECHNICAL DEGREE) (Two-year technical degree) of computing. <br><br>
                    <strong>Program: </strong><br>
                    Algorithmics and Programming - Theory and Practice <br>
                    Architecture, Systems and Networks<br>
                    Tools and Methods of the Software engineering<br>
                    Mathematics<br>
                    Economy and Management of organizations<br>
                    Expression and Communication<br>
                    Projects and Professional Internship<br><br>

                    The Diploma (DUT)(TWO-YEAR TECHNICAL DEGREE) is delivered by the university of Bordeaux.
                </p>
            </div>
        </section>
    </div>
    </div>
    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
