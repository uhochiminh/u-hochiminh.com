<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 

ob_start();
pDoctype("start", array("Accessibilité"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    
	<div id="accessibilite" class="content">
    	<div class="situation">
            <h2>Accessibility</h2>
            <h3>We attempt to respect the criteria of accessibility on our web site, according to the legislation in force.</h3>
        </div>
        <div class="position">
        	<i class="fa fa-sitemap blue"></i>
        	<a href="Accueil.html">Home</a>
            <i class="fa fa-angle-right"></i> 
            Accessibility
		</div>
		
		<div class="content-text">
			<section class="content-text-presentation">
				<h4 class="content-text-title"><span>NAVIGATION BY <strong>Tab</strong>ULATION</span></h4>
				<div class="content-text-text text justify">
                    <p>It is possible to use the button <strong>Tab</strong> (Tabulation) Of your keyboard to walk of link in link throughout the page. For it:</p>
                  	<ul>
                    	<li>Press on the button <strong>Tab</strong> And repeat until select the wished link.</li>
                    	<li>Validate by <strong>Enter</strong>.</li>
                    	<li>To go back among the links, Use the combination of button <strong>Maj</strong> (Shift) + <strong>Tab</strong> (Tabulation).</li>
                    </ul>
                    <p> Attention under Safari, by default, the navigation by tabulation is not activated. 
                        To activate it: in the menu of the regulations go to 'Preferences', then, to 'Advances', check the box allowing to navigate by tabulation.</p>

                    <br/>
                    
                    <p>Here is the list of hot keys usable on this site: </p>
                    <ul>
                        <li>Button <strong>0</strong> - Policy of accessibility of the site</li>
                        <li>Button <strong>1</strong> - Homepage of the site</li>
                        <li>Button <strong>2</strong> - Page of presentation of the proposed Trainings</li>
                        <li>Button <strong>3</strong> - Page of space to Companies</li>
                        <li>Button <strong>4</strong> - Practical information</li>
                        <li>Button <strong>5</strong> - Contact</li>
                        <li>Button <strong>6</strong> - Form of Connection</li>
                        <li>Button <strong>7</strong> - Credits and Legal notices</li>
                        <li>Button <strong>8</strong> - Site map</li>
                        <li>Button <strong>f</strong> - Facebook page</li>
                        <li>Button <strong>t</strong> - Twitter account</li>
                        <li>Button <strong>r</strong> - Search bar</li>
                    </ul>

                    <br/>
                    
                    <p>The combinations of buttons to activate these short-cuts are different according to the browser and the operating system used. The procedures to be followed to activate hot keys in the main configurations are the following ones:</p>
                    <ul>
                        <li>Mozilla Firefox, Google Chrome, Safari On Windows : buttons <strong>Maj</strong> + Alt + [hot keys] (Attention: use the digital buttons of the keyboard and not those of the keypad).</li>
                        <li>Internet Explorer sur Windows : buttons Alt + Maj + [Raccourci clavier], puis touche <strong>Entrée</strong> (Attention: use the digital buttons of the keyboard and not those of the keypad). With Windows Vista, use the combination of buttons Alt+ <strong>Maj</strong> + [hot key].</li>
                        <li>Opera 7 sur Windows, Macintosh et Linux : Echap + <strong>Maj</strong> + [hot key]</li>
                        <li>Safari on Macintosh : <strong>Ctrl</strong> + <strong>Maj</strong> + [hot key]</li>
                        <li>Mozilla and Netscape on Macintosh : <strong>Ctrl</strong> + [hot key]</li>
                        <li>Galeon, Mozilla FireFox on Linux : Alt + [hot key]</li>
                	</ul>

                    <br/>
                </div>
			</section>
            
			<section class="content-text-presentation">
				<h4 class="content-text-title"><span>Adjustment of the size of the text</span></h4>
				<div class="content-text-text text justify">
                	<p>The texts of the site have a relative font size: they can be enlarged by the user. To enlarge or decrease the size of display of texts, it is possible to use one of the following methods:</p>
                    <ul>
                        <li>Feature " Size of the text " presents in the menu "Display" of the browser.</li>
                        <li>combinations of buttons <strong><strong>Ctrl</strong></strong> + Thumb wheel of the mouse.</li>
                        <li>combinations of buttons <strong>Ctrl</strong> + buttons "+" and "-" of key pad.</li>
                    </ul>
                </div>
			</section>
            
			<section class="content-text-presentation">
				<h4 class="content-text-title"><span>Respect for the standards web</span></h4>
				<div class="content-text-text text justify">
                	<p>This site uses the last technologies of <a href="#"><strong>W3C</strong></a>. It offers a better depiction with the browsers of last generation, but nevertheless available for consultation rest with the older browsers.</p>
                </div>
			</section>
		</div>
	</div>	

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
pDoctype("end");

$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
