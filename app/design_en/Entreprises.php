<?php  	
/* @Author: Charly Parpet - cparpet, IACHI Dimitri - diachi */
require("_all.php"); 

ob_start();
pDoctype("start", array("Entreprises"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

	<div id="entreprise" class="content">
		<div class="situation">
			<h2>Companies</h2>
			<h3>We train professionals and as such, all the actors of the economy have authority to be our partners.</h3>
		</div>
		<div class="position">
			<i class="fa fa-sitemap blue"></i>
			<a href="Accueil.html">Home</a>
			<i class="fa fa-angle-right"></i>
			Companies
		</div>
    	
    	<div class="content-text">
			<section class="content-text-entreprises">
				<h4 class="content-text-title"><span>You look for skills in computing?</span></h4>
				<div class="content-text-text content-text-points text justify">
					<table><tbody><tr>
						<td><div class="point-circle"><i class="fa fa-university"></i></div></td>
						<td>
							<p>We train professionals and as such, all the actors of the economy have authority to be<strong> our partners</strong>. 
								This partnership, with the professional world, is for us a priority. Furthermore, our students have the training adapted to participate in the development of your company.</p>
						</td>
					</tr></tbody></table>
				</div>
			</section>

			<section class="content-text-entreprises">
				<h4 class="content-text-title"><span>The internships </span></h4>
				<div class="content-text-text content-text-points text justify">
					<table><tbody><tr>
						<td>
							<p>The professional internships establish one of the key points of our relation with companies. For our students, the internship is the opportunity of <strong>put into practice the acquired knowledge</strong> And to complete significantly their training(formation). In return, the experience(experiment) shows that the professionals can matter(count) on our trainees in whom they find most of the time <strong>competent collaborators</strong> and quickly <strong>operational.</strong></p>
	
							<p>They can join you in internship:</p>

							<br>

							<ul>
								<li><strong>The internships of License(Bachelor's degree):</strong></li>
								<ul>
									<li>After 2 years of training, 10 in 12 weeks of internship between August 1st and October 30th</li>
									<li>After 3 years of training,...</li>
								</ul>
								<li><strong>The internship of Master's degree: 6 months of internship from ???</strong></li>
							</ul>

							<p><i>If you are interested, contact us through the page <a href="contact.html">contact.</a></i></p>
						</td>
						<td><div class="point-circle"><i class="fa fa-briefcase"></i></div></td>
					</tr></tbody></table>
				</div>
			</section>
			
			<section class="content-text-entreprises">
				<h4 class="content-text-title"><span>Participate in our teachings</span></h4>
				<div class="content-text-text text justify">
					<p>You can also participate in our You can also participate in our teachings. Professionals' interventions allow to <strong>strengthen the professional anchoring</strong> Contents of training and contribute to the link UNIVERSITY INSTITUTES OF TECHNOLOGY - COMPANIES.</p>
 
<p>These interventions by temporary replacements stemming from the professional world (business managers, technicians, executives), <strong>very appreciated by the students</strong>, represent an important part of the hours of teaching in our trainings.</p>
 
<p>The UIT is <strong>always in research for professional temporary replacements</strong>, If you wish to get involved in one of our trainings, get in touch with us through the page <a href="contact.html">contact</a>.</p>
				</div>
			</section>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
            ob_end_clean();
htmlCleaner::make($result);
?>
