<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 


ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

    <div class="script-direct">
        <?php pHeader(); ?>
        <div id="accueil" class="content">
            <div class="situation no-display">
                <h2>Home</h2>
                <h3>The IT department of the French University center ( PUF ) of Ho Chi Minh City ( HCMV ) proposes trainings of the 1st year of university in the 5th year of university.</h3>
            </div>
			
			<div class="slider-fixed no-display">
				<img src="images/slider-01.png" alt="Pictures of students" />
			</div>
			
			<div class="slider">
				<div id="slide1" class="slide">
					<img src="images/slider-01.png" alt="Pictures of students" />
				</div>
                <div id="slide2" class="slide">
                    <img src="images/slider-02.png" alt="Pictures of students" />
                </div>
                <div id="slide3" class="slide">
                    <img src="images/slider-03.png" alt="Pictures of students" />
                </div>
				<div id="prec">
					<img src="images/slider-fleche-gauche.png" class="fleche-gauche" alt="Previous" />
				</div>
				<div id="suiv">
					<img src="images/slider-fleche-droite.png" class="fleche-droite" alt="Next" />
				</div>
				<div class="clear"></div>
			</div>

            <div class="content-text">
                <section class="content-text-presentation">
                    <h4 class="content-text-title"><span>Presentation</span></h4>
                    <div class="content-text-text text alinea justify">
                        <p>The IT department of the French University center of Ho Chi Minh presents you here the trainings(formations) which he proposes.
							We hope that you will find the information for which you look, about is your profile: company, student, teacher, future student...</p>
                        <p>The proposed trainings(formations) are on diverse domains of the computing and can bring towards several levels (of the 1st year of university in the 5th year of university).
                        <br>Our programs are supplied by big international universities and our key points are researchers of world class and experimented professional trainers.
                        <br>Our diplomas are accredited according to the highest academic standards and offer credibility and world recognition.</p>
                        <p>Take advantage of our narrow links with the biggest local and international companies for additional opportunities of learning.
                        <br>Our accent on the technical aspects, the skill-based learning gives you the possibility of pursuing a career in the research, the universities or the industry.</p>
                    </div>

                    <div class="content-text-list-points text">
                        <h5 class="content-text-list-points-title click"><i>+</i> Why to study in the PUF</h5>
                        <div class="content-text-list-points-content no-display">
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>1</i></div>
                                <p><strong>Leading European universities:</strong><br>
                                   Our trainings are delivered by teachers and researchers stemming from Universities of international level.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>2</i></div>
                                <p><strong>Diplomas internationally recognized:</strong><br>
                                Our diplomas are approved and will allow you to integrate the European and North American universities.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>3</i></div>
                                <p><strong>Major industrial partners:</strong><br>
                                Our narrow links with the local and international industrial world offer you opportunities of internship and learning.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>4</i></div>
                                <p><strong>Career prospects: </strong><br>
                                Our teaching allows you to acquire the technical and theoretical skills which authorize you indifferently a career directed to the research, the teaching or the company.</p>
                            </div>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> For which diplomas</h5>
                        <div class="content-text-list-points-content alinea no-display">
                            <p><strong>License(Bachelor's degree) in computing:</strong></p>
                            <p>Our license(Bachelor's degree) brings you in 3 years the skills and the necessary knowledge to participate in the conception, in the development and in the maintenance of computer systems:<br>
                                - Conception, development, validation, follow-up and maintenance of software,<br>
                                - Installation, administration and maintenance of systems and data networks.</p>

                            <p><strong>Network Master's degree:</strong></p>
                            <p>Our Master's degree in Internet and mobile services trains you in 2 years by concentrating its teachings on the conception and the development of mobile and international-wide computer systems, on the conception of a network infrastructure which can support these computer systems, as well as on the design of evolutionary systems which run distributed architectures. </p>

                            <p><strong>Master's degree Software engineering:</strong></p>
                            <p>Our Master's degree in Software engineering trains you in 2 years by concentrating its teachings on the formal methods, the methodologies hardened by project management, as well as practical skills, to train experts in conception and development of effective and safe software systems. </p>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i>Trainings of international level</h5>
                        <div class="content-text-list-points-content alinea no-display">
                            <p>Created in 2006, the PUF delivers university education in computing in partnership with the best French and European universities.
                            We License(Bachelor's degree) and Master's degree offer you a high-quality technical training coupled with a real practical experience.</p>
                            <p>These diplomas allow you to acquire technical and managerial skills preparing you to occupy decision-makers' posts in the sector of information technologies and communication.<p>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> Career opportunities</h5>
                        <div class="content-text-list-points-content alinea no-display">
                            <p>The sector of ICTS suffers from a world shortage of qualified personnels today. Our diplomas are thus carriers on the labor market and guarantee to our graduates of the progress of fast careers and the high salaries. Our students acquire essential skills of management to the computing leaders of the world of the current competitive work. </p>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> Our key points</h5>
                        <div class="content-text-list-points-content no-display">
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-share-alt"></i></div>
                                <p>The PUF maintains narrow links with Europe through industrial partnerships, through collaborations of search(research) and exchanges of students and teachers.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-graduation-cap"></i></div>
                                <p>The trainings of the PUF are assured by a team experimented by teachers and by professionals of the sector of ICTS.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-book"></i></div>
                                <p>Our educational practices ally lectures, tutorial classes, practical class and works in " group project " to allow you to develop a
                                real practical experience and to prepare you at best for an optimal occupational integration.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-flag"></i></div>
                                <p>With a training delivered in French or in English you can acquire language skills essential to your success in professional
                                environment..</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-briefcase"></i></div>
                                <p>You will benefit finally from our narrow links with the state-owned and international companies of the sector of ICTS to prepare your future career.</p>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section class="content-text-parteners">
                    <h4 class="content-text-title"><span>Partners</span></h4>
                    <ul class="content-text-text">
                        <li><a href="http://www.ambafrance-vn.org/Poles-Universitaires-Francais-et" target="_blank"><img src="images/partenaire-puf.png" alt="Logo of the PUF"></a></li>
                        <li><a href="http://www.u-bordeaux.fr/" target="_blank"><img src="images/partenaire-ubordeaux.png" alt="Logo of the university of Bordeaux"></a></li>
                        <li><a href="http://www.upmc.fr/" target="_blank"><img src="images/partenaire-upmc.png" alt="Logo of the UPMC"></a></li>
                        <li><a href="http://www.vnuhcm.edu.vn/" target="_blank"><img src="images/partenaire-daihocquaocgia.png" alt="Logo of the national university of Ho Chi Minh"></a></li>
                    </ul>
                </section>
            </div>
        </div>

        <?php pFooter(); ?>
    </div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
