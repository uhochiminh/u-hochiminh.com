<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

    <div id="formations" class="content">
        <div class="situation">
            <h2>Trainings</h2>
            <h3>Discover all our university education to the international standards regarding teaching.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Trainings
        </div>

        <div class="content-text">
            <section class="content-text-formations-slider">
                <img src="images/formations-amphi.png" alt="Amphithéâtres">
                <ul>
                    <li><a href="L1.html">1st year of university</a></li>
                    <li><a href="L2.html">2nd year of university</a></li>
                    <li><a href="L3.html">3rd year of university</a></li>
                    <li class="expand"><a href="M1.html">4th year of university</a></li>
                    <li class="expand"><a href="M2.html">5th year of university</a></li>
                </ul>
            </section>

            <section class="content-text-formations-presentation">
                <h4 class="content-text-title"><span>Trainings</span></h4>
                <div class="content-text-text text alinea justify">
                    <p>Vietnam knows at present a considerable economic development in all the professional domains.
						In this context, information technologies constitute an essential lever in the modernization of the Vietnamese economy.
						Companies settled in Vietnam, that they are public or deprived, Vietnamese or international, feel a crucial need for high-level IT specialists today. </p>
                    <p>The PUF Ho Chi Minh has for main activity to liven up French university education relocated in Vietnam. All the trainings follow the following principles:</p>
                    <ul>
                        <li>they are the <strong>same</strong> as trainings distributed in France, and bind the theory and the practice.</li>
                        <li>They are assured at least 50 % by teachers of the partner French universities, or by professionals which their are associated.</li>
                        <li>They lead to a French diploma of the DML(LICENCE-MASTER-DOCTORAT) plan, <strong>recognized everywhere by Europe</strong> and by equivalence in numerous countries.</li>
                    </ul>
                </div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
