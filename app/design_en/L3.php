<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

 <div id="formation" class="content">
    <div class="situation">
        <h2>3rd year of university</h2>
        <h3>Discover our university level license 3</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Home</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Training</a>
		<i class="fa fa-angle-right"></i>
		L3
	</div>
    
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-projets.png" />
                <p> The third year presents two reference courses:</p>
                <p><strong>The Professional License</strong>  brings a robust capital of knowledge suited to any job(business) of 
							The computing and allowing the students to take immediately computing responsibilities 
							In companies.</p>
                <p><strong>The License of Sciences and Technologies IT mention</strong> Aim at making acquire theoretical bases and 
							Practices allowing to study in a speciality of Master's degree of Computing of Ho Chi Minh 
							City (Network Speciality or Speciality Software engineering) or of any French university or 
							European.</p>
            </div>
            
            <div class="content-text-list-points text">
                <h5 class="content-text-list-points-title click"><i>+</i> Program </h5>
                <div class="content-text-list-points-content justify no-display">
                    <p><strong>Managerial skills:</strong><br>
						Opérationel management<br>
                        Human resources, Communication and Management
                    </p>
                    <p><strong>Technical skills:</strong><br>
                        Management of Information systems<br>
                        Advanced methods of software engineering<br>
                        Advanced methods of development
                    </p>
                    <p><strong>Scientific and technical skills:</strong><br>
                        Systems, Networks and Database<br>
                        Algorithmics and Mathematics for the computing<br>
                        Programming and Development
                    </p>
                    <p><strong>Project</strong></p>
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i> Professional license</h5> 
                <div class="content-text-list-points-content justify no-display">
                    <p>    
                        <strong>Company internship</strong><br>
                        Diploma delivered by the University of Bordeaux
                    </p> 
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i> It license and technologies</h5> 
                <div class="content-text-list-points-content justify no-display">
                    <p>    
                        <strong>Knowledge of the company</strong><br>
                        Diploma delivered by the University of Paris 6
                    </p> 
                </div>
            </div>
        </section>
    </div>
</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
