<?php  	
/* @Author: ETCHEVERRY Laetitia letchv4 */
require("_all.php"); 


ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

    <div id="mention_legales" class="content">
        <div class="situation">
            <h2>LEGAL NOTICES</h2>
            <h3>You will find here all the information concerning the creation of this site as well as its context.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Home</a>
            <i class="fa fa-angle-right"></i>
            Legal Notices
        </div>
        
        <div class="content-text">
            <section class="content-text-text text alinea justify">
                <p>The present site is published by Mrs Chritine UNY professor to the IUT(UNIVERSITY INSTITUTE OF TECHNOLOGY) of the university of Bordeaux. Nevertheless this site is the property of the IT department of the PUF ( French University center) of Ho Chi Minh.</p> 

                <p>This site in summer to develop, within the framework of a project of training(formation), by <strong>IACHI Dimitri Fabien</strong>, <strong>ETCHEVERRY Laetitia</strong>, <strong>GARCIA Johan</strong>, <strong>PARPET Charles-Eddy</strong> et <strong>ROBIN Benoit</strong>, Undergraduates Professional DAWIN to the IUT(UNIVERSITY INSTITUTE OF TECHNOLOGY) of the university of Bordeaux.</p>

                <p>All the contents of the site are covered by it <a href="http://creativecommons.fr/licences/" title="Lien menant au site sur la licence creative commons">Creative license commons</a> which allow:</p>
                <ul>
                    <li> To share and facilitate the use of their creation by others </li>
                    <li> To authorize free of charge the reproduction and the broadcasting (under certain conditions) </li>
                    <li> To grant more rights for the users by completing the copyright which applies by default </li>
                    <li> To develop a work and enrich the common holdings (joint property or Commons) </li>
                    <li> To save transaction costs </li>
                    <li> To legalize the peer to peer of their works </li>
                </ul>

                <p></p>

                <p><strong>U-HoChiMinh</strong>, as well as its graphical representation, and <a href="http://www.u-hochiminh.com/">www.u-hochiminh.com</a> are the property of the PUF of Ho Chi Minh.</p>

                <p>Source codes used for the creation of this web site and the architecture of the program are covered by the French and international legislation on the copyright and the intellectual property. All the reproduction rights are reserved. Nevertheless any reproduction of the site, even partial, is forbidden except express authorization of the persons in charge of this site and these autheurs. In case of violation of these measures the person, the morality or the physics, submits himself to the statutory penal and civil penalties French.</p>

                <p>According to the regulations in force since 10/07/2006, the statement(declaration) with the CNIL(NATIONAL COMMISSION FOR INFORMATION TECHNOLOGY AND CIVIL LIBERTIES), with the not trade and/or institutional web site and/or not collecting data at risk, is not compulsory. Nevertheless, the PUF of Ho Chi Minh makes a commitment to respect the private life of Internet users shape to the French legislation on the protection of the private life and personal freedoms (law N 78-17 of January 6th, 1978).</p>

                <p>This site thus collects no information staff. Any time in application of the article 34 of data protection acts of August 1st, 2000, every person having been the object of a collection of information refreshed of a right of access, a rectification and an opposition to the personal data concerning it or by mail (15, rue Naudet CS 10207 33175 Gradignan Cedex, France) or by e-mail to the address<a href="mailto:contact@u-HoChiMinh.com">contact@u-hochiminh.com</a>.</p>
            </section>

            <section class="content-text-text">
                <div class="element">
                    <div class="content-title"><span>Owner of the site</span></div>
                    <div class="content-info">IT department of the French University center<br/>District 6, Linh Trung Ward,<br/>Thu Duc District,<br/>Ho Chi Min-Ville, Vietnam<br/><br/>Phone: <a href="tel:+0837242169">(08) 37 242 169</a><br/>Fax: <a href="tel:+0837242169">(08) 37 242 166</a></div>
                </div>
                <div class="element">
                    <div class="content-title"><span>Host</span></div>
                    <div class="content-info">U.I.T. Bordeaux 1<br/>15 Rue de Naudet<br/>33175 Gradignan</div>
                </div>
                <div class="element">
                    <div class="content-title"><span>Design of the site </span></div>
                    <div class="content-info">IACHI Dimitri<br/>ETCHEVERRY Laetitia<br/>GARCIA Johan<br/>PARPET Charles-Eddy<br/>ROBIN Benoit</div>
                </div>
            </section>

            <div class="clear"></div>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
