<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 


ob_start();
pDoctype("start", array("Connexion"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

	<div id="connexion" class="content">
		<div class="situation">
			<h2>Connexion</h2>
			<h3>This page is to reserve for the staff supervising of the PUF of Ho Chi Minh.</h3>
		</div>
		<div class="position">
			<i class="fa fa-sitemap blue"></i>
			<a href="Accueil.html">Home</a>
			<i class="fa fa-angle-right"></i>
            Connexion 
		</div>

		<div class="content-text">
            <div class="box">
                <div class="icon"><i class="fa fa-lock"></i></div>
                
                <div class="notify red no-display">Identifier or password is not valid .</div>

                <form action="Missions.html" method="POST">
                    <div class="element">
                        <label>Id :</label>
                        <input type="text" name="your-login" size="60" />
                    </div>

                    <div class="element">
                        <label>Password :</label>
                        <input type="password" name="your-password" size="180" />
                    </div>

                    <div class="element button">
                        <input type="submit" value="Connexion" class="click" />
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
            
			<div class="clear"></div>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
