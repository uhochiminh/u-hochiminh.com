<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 

ob_start();
pDoctype("start", array("Missions"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

    <div id="missions" class="content">
        <div class="situation">
            <h2>Missions</h2>
            <h3>Discover the missions organize within the IT department of the PUF by our French professors.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Home</a>
            <i class="fa fa-angle-right"></i>
            Missions
        </div>

        <div class="content-text">
            <section class="content-text-view"> 
                <div class="content-text-text text">
                    <div class="action right no-display">
                        <i class="missions-calendar fa fa-calendar"></i>
                        <i class="missions-menu fa fa-bars"></i>
                    </div>
                    
                    <ul class="missions-liste">
                        <li class="click"><span class="code">27/11/2014 - 28/11/2014</span>
                            <span class="professor">Olivier Frigo</span>
                            <i class="fa fa-plus"></i>
                            <div class="contente">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque velit ligula, porttitor ac convallis sed, tincidunt vel sapien. Donec ultrices eros ligula, in tincidunt eros fermentum lobortis. Donec vel lacus elit. Vivamus euismod ut ante ac posuere. Proin faucibus urna et elit feugiat, mattis consectetur erat aliquam. In vulputate efficitur magna a ullamcorper. Suspendisse aliquet lobortis lobortis. Quisque at massa mauris. Integer scelerisque lacinia nisl, a mollis urna semper non.</p>
                                     <p>Fusce a dolor ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis pulvinar mollis. Suspendisse id ante feugiat, hendrerit odio vitae, blandit leo. Ut posuere magna et mi venenatis consequat. Nam neque mi, posuere vitae orci eget, facilisis sagittis lorem. Mauris at semper dui. Proin et leo placerat, suscipit metus a, laoreet eros. Ut tempus tellus a sapien ultricies ullamcorper. Suspendisse nec mollis tellus. In ac ligula tristique, scelerisque nulla id, luctus mi. ...</p>
                                </div>
                                <button>Download the link master record</button>
                                <div class="clear"></div>
                            </div>
                        </li>
                        
                        <li class="click"><span class="code">27/11/2014 - 28/11/2014</span>
                            <span class="professor">Olivier Frigo</span>
                            <i class="fa fa-plus"></i>
                            <div class="contente">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque velit ligula, porttitor ac convallis sed, tincidunt vel sapien. Donec ultrices eros ligula, in tincidunt eros fermentum lobortis. Donec vel lacus elit. Vivamus euismod ut ante ac posuere. Proin faucibus urna et elit feugiat, mattis consectetur erat aliquam. In vulputate efficitur magna a ullamcorper. Suspendisse aliquet lobortis lobortis. Quisque at massa mauris. Integer scelerisque lacinia nisl, a mollis urna semper non.</p>
                                     <p>Fusce a dolor ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis pulvinar mollis. Suspendisse id ante feugiat, hendrerit odio vitae, blandit leo. Ut posuere magna et mi venenatis consequat. Nam neque mi, posuere vitae orci eget, facilisis sagittis lorem. Mauris at semper dui. Proin et leo placerat, suscipit metus a, laoreet eros. Ut tempus tellus a sapien ultricies ullamcorper. Suspendisse nec mollis tellus. In ac ligula tristique, scelerisque nulla id, luctus mi. ...</p>
                                </div>
                                <button>Download the link master record</button>
                                <div class="clear"></div>
                            </div>
                        </li>
                        
                        <li class="click"><span class="code">27/11/2014 - 28/11/2014</span>
                            <span class="professor">Olivier Frigo</span>
                            <i class="fa fa-plus"></i>
                            <div class="contente">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque velit ligula, porttitor ac convallis sed, tincidunt vel sapien. Donec ultrices eros ligula, in tincidunt eros fermentum lobortis. Donec vel lacus elit. Vivamus euismod ut ante ac posuere. Proin faucibus urna et elit feugiat, mattis consectetur erat aliquam. In vulputate efficitur magna a ullamcorper. Suspendisse aliquet lobortis lobortis. Quisque at massa mauris. Integer scelerisque lacinia nisl, a mollis urna semper non.</p>
                                     <p>Fusce a dolor ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis pulvinar mollis. Suspendisse id ante feugiat, hendrerit odio vitae, blandit leo. Ut posuere magna et mi venenatis consequat. Nam neque mi, posuere vitae orci eget, facilisis sagittis lorem. Mauris at semper dui. Proin et leo placerat, suscipit metus a, laoreet eros. Ut tempus tellus a sapien ultricies ullamcorper. Suspendisse nec mollis tellus. In ac ligula tristique, scelerisque nulla id, luctus mi. ...</p>
                                </div>
                                <button>Download the link master record</button>
                                <div class="clear"></div>
                            </div>
                        </li>
                        
                        <li class="click"><span class="code">27/11/2014 - 28/11/2014</span>
                            <span class="professor">Olivier Frigo</span>
                            <i class="fa fa-plus"></i>
                            <div class="contente">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque velit ligula, porttitor ac convallis sed, tincidunt vel sapien. Donec ultrices eros ligula, in tincidunt eros fermentum lobortis. Donec vel lacus elit. Vivamus euismod ut ante ac posuere. Proin faucibus urna et elit feugiat, mattis consectetur erat aliquam. In vulputate efficitur magna a ullamcorper. Suspendisse aliquet lobortis lobortis. Quisque at massa mauris. Integer scelerisque lacinia nisl, a mollis urna semper non.</p>
                                     <p>Fusce a dolor ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis pulvinar mollis. Suspendisse id ante feugiat, hendrerit odio vitae, blandit leo. Ut posuere magna et mi venenatis consequat. Nam neque mi, posuere vitae orci eget, facilisis sagittis lorem. Mauris at semper dui. Proin et leo placerat, suscipit metus a, laoreet eros. Ut tempus tellus a sapien ultricies ullamcorper. Suspendisse nec mollis tellus. In ac ligula tristique, scelerisque nulla id, luctus mi. ...</p>
                                </div>
                                <button>Download the link master record</button>
                                <div class="clear"></div>
                            </div>
                        </li>
                        
                        <li class="click"><span class="code">27/11/2014 - 28/11/2014</span>
                            <span class="professor">Olivier Frigo</span>
                            <i class="fa fa-plus"></i>
                            <div class="contente">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque velit ligula, porttitor ac convallis sed, tincidunt vel sapien. Donec ultrices eros ligula, in tincidunt eros fermentum lobortis. Donec vel lacus elit. Vivamus euismod ut ante ac posuere. Proin faucibus urna et elit feugiat, mattis consectetur erat aliquam. In vulputate efficitur magna a ullamcorper. Suspendisse aliquet lobortis lobortis. Quisque at massa mauris. Integer scelerisque lacinia nisl, a mollis urna semper non.</p>
                                     <p>Fusce a dolor ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis pulvinar mollis. Suspendisse id ante feugiat, hendrerit odio vitae, blandit leo. Ut posuere magna et mi venenatis consequat. Nam neque mi, posuere vitae orci eget, facilisis sagittis lorem. Mauris at semper dui. Proin et leo placerat, suscipit metus a, laoreet eros. Ut tempus tellus a sapien ultricies ullamcorper. Suspendisse nec mollis tellus. In ac ligula tristique, scelerisque nulla id, luctus mi. ...</p>
                                </div>
                                <button>Download the link master record</button>
                                <div class="clear"></div>
                            </div>
                        </li>
                    </ul>
                    
                </div>
            </section>

            <section class="content-text-form">
                <h4 class="content-text-title"><span>Add a training</span></h4>
                <div class="content-text-text text justify">
                    <p>You can add easily a mission on the schedule of the missions, by means of the form below: </p>
                    
                    <div class="notify blue no-display">Your message in well transmitted has your addressee.</div>
                    <div class="notify red">An error arose during sending of your message.</div>

                    <form action="#" method="POST">
                        <div class="left">
                            <div class="element">
                                <label>Teacher - Lastname & Firstname :</label>
                                <input type="text" name="professor-name" size="60" />
                            </div>

                            <div class="element">
                                <label>teacher - Email :</label>
                                <input type="email" name="professor-email"size="60" />
                            </div>
                        </div>
                        
                        <div class="right">
                            <div class="element">
                                <label>TD Manager - Lastname & Firstname :</label>
                                <input type="text" name="manager-name" size="60" />
                            </div>

                            <div class="element">
                                <label>Td Manager - Email :</label>
                                <input type="email" name="manager-email"size="60" />
                            </div>
                        </div>

                        <div class="element left">
                            <label>From :</label>
                            <input type="date" name="professor-mission-start" />
                        </div>
                        <div class="element right">
                            <label>To :</label>
                            <input type="date" name="professor-mission-end" />
                        </div>
                        
                        <div class="element">
                            <label>Message :</label>
                            <textarea name="your-message" cols="40" rows="10"></textarea>
                        </div>

                        <div class="element">
                            <label> Link master record :</label>
                            <input type="file" name="liaison-file" />
                        </div>

                        <div class="element button">
                            <input type="submit" value="Envoyer" class="click" />
                            <input type="reset" value="Effacer" class="click" />
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>