<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 

ob_start();
pDoctype("start", array("Semaine du livre - Info Pratique"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    
	<div id="info_pratique" class="content">
		<div class="situation">
			<h2>Practical information</h2>
			<h3>You will find here all the practical information in the life as students (university calendar, access to the university restoring, useful links)</h3>
		</div>
		<div class="position">
			<i class="fa fa-sitemap blue"></i>
			<a href="Accueil.html">Home</a>
			<i class="fa fa-angle-right"></i>
			<a href="InfosPratiques.html">Practical information</a>
			<i class="fa fa-angle-right"></i>
            SEMAINE DU LIVRE 
		</div>

		<div class="content-text">
			<article class="content-text-post">
				<img src="images/infospratiques-post-img.png" class="post-img" />
				<div class="post-content">
					<h4 class="content-text-title-left"><span>Semaine du livre</span></h4>
					<div class="info">Posted on 24/10/2014 by Christine UNY</div>
					<div class="content-text-text alinea text justify">
						<p>Praesent commodo cursus magna, vel <strong>scelerisque nisl consectetur et</strong> mecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum.</p>
						<p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. <strong>Integer posuere erat</strong> a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur <strong>est at lobortis</strong>. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
                        
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec volutpat ligula, consequat egestas orci. Maecenas vehicula nec dui sed ullamcorper. Proin vulputate lorem vitae mauris congue, nec egestas lectus imperdiet. Curabitur lobortis erat augue, quis egestas diam fermentum sed. Suspendisse sed iaculis risus. <strong>Suspendisse ornare tincidunt</strong> efficitur. In hac habitasse platea dictumst. Duis facilisis mauris ac velit rhoncus, nec sagittis sem condimentum.</p>
                        
                        <p>Sed semper mi quis eros pellentesque egestas. Donec a nisl commodo, mollis mi id, mattis enim. Nulla posuere ex in sem luctus ultricies. In at odio dolor. Donec vel ex a nibh eleifend efficitur. Sed pulvinar dictum dui. Phasellus et <strong>viverra elit</strong>. Nulla congue libero vel congue blandit. Morbi diam lacus, scelerisque at urna et, hendrerit aliquam sapien. In diam odio, dapibus ac ante ut, consectetur lobortis elit. Donec ac sem suscipit, interdum massa non, commodo magna. Integer sed ipsum in lectus fermentum ultrices. Fusce quis dolor sed lectus cursus vestibulum sit amet vitae leo. Quisque scelerisque velit non lorem eleifend porta. Vestibulum eu luctus lectus. Cras euismod accumsan vulputate.</p>

                        <p>Aenean ut urna a massa scelerisque tempor. Nullam eleifend tristique elit, sed varius ex efficitur ac. Vestibulum ac velit ac nisl porttitor porttitor. Aenean in dictum justo, at viverra ligula. Vivamus <strong>maximus faucibus eros</strong> sit amet volutpat. Nullam ornare auctor gravida. Donec urna felis, rutrum nec suscipit sed, dignissim quis orci. Morbi malesuada tempor rutrum. Aenean et turpis libero.</p>

                        <p>Nulla mauris magna, pellentesque sed diam id, dictum accumsan nisi. Praesent vitae malesuada ante. Quisque bibendum lobortis erat, in tincidunt odio molestie quis. Sed porta euismod tortor, non condimentum sem. Integer eu fermentum eros, eu vestibulum nibh. Donec vehicula, massa ut lacinia pharetra, leo tellus blandit risus, eget volutpat lacus dui sit amet metus. Etiam sit amet tellus risus. Suspendisse mauris dui, molestie vitae metus non, dapibus placerat nunc.</p>

					</div>
				</div>
                
                <div class="clear"></div>
			</article>
            
            <nav class="content-text-nav">
                <div class="left"><a href="InfoPratique.html"><i class="fa fa-angle-left"></i><span>Show of video games</span></a></div>
                <div class="right"><a href="InfoPratique.html"><span>Showed them against the reform of the PUF</span><i class="fa fa-angle-right"></i></a></div>
                <div class="clear"></div>
            </nav>
            
			<div class="clear"></div>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
