<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

<div id="formation" class="content">
    <div class="situation">
        <h2>5th year of university</h2>
        <h3>Discover our university level Master's degree 2</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Home</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Training</a>
		<i class="fa fa-angle-right"></i>
		M2
	</div>
    
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-diploma.png" />
                <p> The 5th year of university is one year of specialization. It is possible to specialize in Networks or in Software Engineer.</p>
                <p><strong> The Network option </strong>can result in a career of network administrator, for example.</p>
                <p><strong>The option Software Engineer, </strong>As its name indicates it, leads to engineer's status.<br>
                It is also a possible specialization to become a project manager, a consultant...</p>
            </div>
            <div class="content-text-list-points text">
                <h5 class="content-text-list-points-title click"><i>+</i>Specialization networks</h5> 
                <div class="content-text-list-points-content justify no-display">
                   <p>Occupational integration<br>
                    Multimedia and Quality of service<br>
                    Management of Information system<br>
                    Voice over IP<br>
                    Routers and routing<br>
                    Control and traffic of networks<br>
                    Multimedia transfers on networks IP<br>
                    Big graphic networks<br>
                    Categories of network carriers<br>
                    Digital communication<br>
                    Security of the advanced networks<br>
                    Regulation of internet<br>
                    Distribution and resistance in attacks<br>
                    <strong>Internship :</strong> 4 to 6 months in January or February<br>
                    Internship (memory of thesis) of research and industrial internship)</p>
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i>Specialization software engineer</h5> 
                <div class="content-text-list-points-content justify no-display">
                   <p>Software architecture<br>
                    Project management<br>
                    Formal conception<br>
                    Advanced database<br>
                    Structure software adaptive and advanced<br>
                    Project of research and study<br>
                    Communication<br>
                    Regulation of internet<br>
                    <strong>Internship :</strong> 4 to 6 months in January or February<br>
                    Internship (memory of thesis) of research and industrial internship)</p>
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i>Perspectives after the diploma</h5>
                <div class="content-text-list-points-content justify no-display"> 
                    <p>A professional career in the software networks or the engineer 
                    (As network administrator, consultant computing, project manager or software engineer)<br>
                    Researcher's career) after a doctorate</p> 
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i>The graduates</h5> 
                <div class="content-text-list-points-content justify no-display">
                    <p>59 students qualified software engineers from 2008 till 2013<br>
                    Project managers, team leaders, directors(managers): 30 %<br>
                    Professors, teachers-researchers, researchers: 40 %<br>
                    Studies in doctorate: 5 %<br>
                    Other : 5%</p>
                </div>
            </div>
        </section>
    </div>
</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
