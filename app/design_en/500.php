<?php  	
/* @Author: ROBIN Benoit -  */
require("_all.php"); 

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); 
        header('Status : 500 Internal Server Error');
        header('HTTP/1.0 500 Internal Server Error');
	?>	
	
    <div id="erreur" class="content">
        <div class="situation">
                <h2>Error 500</h2>
                <h3>An error occurred during the load of the wanted page.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Home</a>
            <i class="fa fa-angle-right"></i>
            Error 500
        </div>

        <div class="content-text">
            <section class="content-text-erreur">
                <div class="title">500</div>
                <div class="texte">Our service is for a moment unavailable.</div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
