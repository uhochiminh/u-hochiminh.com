<?php
/**
 * htmlCleaner
 * Nettoie le code HTML et le réIndent en sortie.
 *
 * @Author: http://www.dzone.com/snippets/php-function-cleaning-html-and
 */

class htmlCleaner
{
    // CONSTRUCTEUR :
    function __construct($code){}

    // METHODES :
        // Function to seperate multiple tags one line :
        private static function newlines($code){
            $code_array = explode("\n", $code);
            foreach ($code_array as $unfixedtextkey => $unfixedtextvalue)
            {
                //Makes sure empty lines are ignores
                if (!preg_match("/^(\s)*$/", $unfixedtextvalue))
                {
                    $fixedtextvalue = preg_replace("/>(\s|\t)*</U", ">\n<", $unfixedtextvalue);
                    $fixedtext_array[$unfixedtextkey] = $fixedtextvalue;
                }
            }
            return implode("\n", $fixedtext_array);
        }

        // Clean HTML in arguments :
        private static function clean($code){
            //Set wanted indentation
            $indent = "    ";

            //Uses previous function to seperate tags
            $fixed_uncleanhtml = self::newlines($code);
            $code_array = explode("\n", $fixed_uncleanhtml);
            //Sets no indentation
            $indentlevel = 0;
            foreach ($code_array as $code_key => $currentuncleanhtml)
            {
                //Removes all indentation
                $currentuncleanhtml = preg_replace("/\t+/", "", $currentuncleanhtml);
                $currentuncleanhtml = preg_replace("/^\s+/", "", $currentuncleanhtml);
                
                $replaceindent = "";
                
                //Sets the indentation from current indentlevel
                for ($o = 0; $o < $indentlevel; $o++)
                {
                    $replaceindent .= $indent;
                }
                
                //If self-closing tag, simply apply indent
                if (preg_match("/<(.+)\/>/", $currentuncleanhtml))
                { 
                    $cleanhtml_array[$code_key] = $replaceindent.$currentuncleanhtml;
                }
                //If doctype declaration, simply apply indent
                else if (preg_match("/<!(.*)>/", $currentuncleanhtml))
                { 
                    $cleanhtml_array[$code_key] = $replaceindent.$currentuncleanhtml;
                }
                //If opening AND closing tag on same line, simply apply indent
                else if (preg_match("/<[^\/](.*)>/", $currentuncleanhtml) && preg_match("/<\/(.*)>/", $currentuncleanhtml))
                { 
                    $cleanhtml_array[$code_key] = $replaceindent.$currentuncleanhtml;
                }
                //If closing HTML tag or closing JavaScript clams, decrease indentation and then apply the new level
                else if (preg_match("/<\/(.*)>/", $currentuncleanhtml) || preg_match("/^(\s|\t)*\}{1}(\s|\t)*$/", $currentuncleanhtml))
                {
                    $indentlevel--;
                    $replaceindent = "";
                    for ($o = 0; $o < $indentlevel; $o++)
                    {
                        $replaceindent .= $indent;
                    }
                    
                    $cleanhtml_array[$code_key] = $replaceindent.$currentuncleanhtml;
                }
                //If opening HTML tag AND not a stand-alone tag, or opening JavaScript clams, increase indentation and then apply new level
                else if ((preg_match("/<[^\/](.*)>/", $currentuncleanhtml) && !preg_match("/<(link|meta|base|br|img|hr)(.*)>/", $currentuncleanhtml)) || preg_match("/^(\s|\t)*\{{1}(\s|\t)*$/", $currentuncleanhtml))
                {
                    $cleanhtml_array[$code_key] = $replaceindent.$currentuncleanhtml;
                    
                    $indentlevel++;
                    $replaceindent = "";
                    for ($o = 0; $o < $indentlevel; $o++)
                    {
                        $replaceindent .= $indent;
                    }
                }
                else
                //Else, only apply indentation
                {$cleanhtml_array[$code_key] = $replaceindent.$currentuncleanhtml;}
            }
            //Return single string seperated by newline
            return implode("\n", $cleanhtml_array); 
        }

    // GETS :
        // Make clean :
        public static function make($code){
            echo self::clean($code);
        }
}
?>