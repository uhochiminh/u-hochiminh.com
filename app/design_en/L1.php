<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    <div id="formation" class="content">
        <div class="situation">
            <h2>1st year of university</h2>
            <h3>Discover our university level license 1</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Home</a>
            <i class="fa fa-angle-right"></i>
			<a href="Formations.html">Training</a>
            <i class="fa fa-angle-right"></i>
			L1
        </div>
        
        <div class="content-text">
            <section class="content-text-formations-presentation">
                <div class="content-text-text text justify">
                    <img src="images/formation-help.png" />
                    <p>
                        The license of computing addresses the students interested in the computing under all its aspects. 
                        It aims at giving bases so theoretical as practical. </p>
                    <p>
                        The graduates of this license will be immediately operational in numerous and varied jobs either can continue studies in Master's degree of computing.</p>
                    <p>
                        <strong>Three diplomas</strong> can be obtained within the cycle License of computing:
                        An IT DUT(two-year technical degree), a Professional License of Computer and software Systems, a License of Sciences and Technologies IT mention.
                    </p>
                </div>
                
                <div class="clear"></div>
                
                <div class="content-text-list-points text">
                    <h5 class="content-text-list-points-title click"><i>+</i> Why an it license</h5>
                    <div class="content-text-list-points-content justify no-display">
                                <ul>
                                    <li>The computing floods our everyday life: e-mails, social networks, video games, office automation, NFC, embarked systems, etc....</li>
                                    <li>The license gives the bases of a general IT skill: conception, development, innovation...</li>
                                </ul>
                    </div>
                    <h5 class="content-text-list-points-title click"><i>+</i> Why at the puf</h5>
                    <div class="content-text-list-points-content justify no-display">
                        <ul>
                            <li>You obtain simultaneously two diplomas in computing: license of the Université Pierre et Marie Curie (Paris VI Sorbonne) and DUT(TWO-YEAR TECHNICAL DEGREE) of the university of Bordeaux.</li>
                            <li>Internationally recognized, theoretical and practical, these diplomas offer you an immediate occupational integration. </li>
                            <li>If you prefer, you can continue in Master's degree in France, in Europe, in the USA or in Vietnam.</li>
                        </ul>
                    </div>
                    <h5 class="content-text-list-points-title click"><i>+</i> Organization of the trainings</h5>
                    <div class="content-text-list-points-content justify no-display">
                        <p><strong>Condition of access:</strong> Be a holder of the high school diploma. </p>
                        <p><strong>Duration and rhythm of the training: </strong>3 years distributed in 6 half-years.</p>
                        <p><strong>The taught subjects:</strong>Algorithmics, programming and programming objects, conception objects, databases, operating systems, networks, mathematics, management, expression and communication in French and in English...
                        <p><strong>Projects and internships: </strong>6 half-years of the training allow to realize numerous projects and 2 internships (half-years 4 and 6).</p>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
