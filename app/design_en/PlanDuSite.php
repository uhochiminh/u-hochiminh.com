<?php  	
/* @Author: Charly Parpet - cparpet */
require("_all.php"); 


ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    
	<div id="plan_du_site" class="content">
		<div class="situation">
        	<h2>Site map</h2>
        	<h3>The site map below will allow you to find its architecture and to navigate there in a intuitive way.</h3>
   		</div>

	    <div class="position">
	        <i class="fa fa-sitemap blue"></i>
	        <a href="Accueil.php" title="Aller à la page d'accueil">Home</a>
	        <i class="fa fa-angle-right"></i>
	        Site map
	    </div>

		<div class="content-text content-text-text text">
			<ul>
				<li><i class="fa fa-angle-right"></i><a href="Accueil.php" title="Aller à la page d'accueil">Accueil</a></li>
				<li><i class="fa fa-angle-right"></i><a href="Formations.php" title="Aller à la page des formations">Formations</a></li>
				<ul>
					<li><i class="fa fa-angle-right"></i><a href="L1.php" title="Aller à la page de la formation Licence 1">Licence 1</a></li>
					<li><i class="fa fa-angle-right"></i><a href="L2.php" title="Aller à la page de la formation Licence 2">Licence 2</a></li>
					<li><i class="fa fa-angle-right"></i><a href="L3.php" title="Aller à la page de la formation Licence 3">Licence 3</a></li>
					<li><i class="fa fa-angle-right"></i><a href="M1.php" title="Aller à la page de la formation Master 1">Master 1</a></li>
					<li><i class="fa fa-angle-right"></i><a href="M2.php" title="Aller à la page de la formation Master 2">Master 2</a></li>
				</ul>
				<li><i class="fa fa-angle-right"></i><a href="Entreprises.php" title="Aller à la page pour les entreprises">Companies</a></li> 
				<li><i class="fa fa-angle-right"></i><a href="InfosPratiques.php" title="Aller à la page des infos pratiques">Practical information</a></li>
				<li><i class="fa fa-angle-right"></i><a href="Accessibilite.php" title="Aller à la page d'accessibilité">Accessibility </a></li>
				<li><i class="fa fa-angle-right"></i><a href="Connexion.php" title="Aller à la page de connexion">Connexion </a></li>
				<li><i class="fa fa-angle-right"></i><a href="MentionsLegales.php" title="Aller à la page des mentions légales">Legal notices</a></li>
				<li><i class="fa fa-angle-right"></i><a href="Contact.php" title="Aller à la page contact">Contact </a></li>
				
			</ul>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
