<?php
/**
 * Header
 * Intégrer facilement le Header à vos pages.
 *
 * @Author: IACHI Dimitri - diachi, ETCHEVERRY Laetitia - letchev4
 */

function pHeader()	{
	?>
	<header class="content">
		<div class="content">
			<a href="Accueil.html" tabindex="0" accesskey="1" class="logo"><h1><img src="images/logo_menu.png" alt="<?php echo $GLOBALS["site"]["nom"]; ?>" title="<?php echo $GLOBALS["site"]["nom"]; ?>" /></h1></a>
			<nav>
				<ul>
					<li><a href="Formations.html" tabindex="1" accesskey="2">Training</a></li>
					<li>|</li>
					<li><a href="Entreprises.html" tabindex="2" accesskey="3">Companies</a></li>
					<li>|</li>
					<li><a href="InfosPratiques.html" tabindex="3" accesskey="4">Practical information</a></li>
					<li>|</li>
					<li><a href="Contact.html" tabindex="4" accesskey="5">Contact</a></li>
					<li class="space"></li>
					<li class="icon"><a href="Connexion.html" tabindex="5" accesskey="6"><i class="fa fa-user"></i></a></li>
					<li class="icon icon-search"><a href="#" tabindex="6" accesskey="r"><i class="fa fa-search"></i></a></li>
				</ul>
			</nav>
            
            <div class="mobile no-display click">
                <i class="fa fa-bars"></i>
                <ul>
                    <li><a href="Formations.html">Training</a></li>
                    <li><a href="Entreprises.html">Companies/a></li>
                    <li><a href="InfosPratiques.html">Practical information</a></li>
                    <li><a href="Contact.html">Contact</a></li>
                    <li><a href="Connexion.html">Connexion</a></li>
                </ul>
            </div>
			<div class="clear"></div>	
		</div>
        <div class="search content no-display">
        	<div class="content">
                <form action="#" method="GET">
                    <i class="fa fa-search"></i>
                    <input type="text" placeholder="Saisir votre recherche ...">
                    <button class="no-display" type="submit">search</button>
                    <div class="clear"></div>
				</form>
            </div>
        </div>
	</header>
	<?php
}
?>