<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    
<div id="formation" class="content">
    <div class="situation">
        <h2>4th year of university</h2>
        <h3>Discover our university level Master's degree 1</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Home</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Training</a>
		<i class="fa fa-angle-right"></i>
		M1
	</div>
    
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-working.png" />
                <p> Why in Ho Chi Minh rather than in France? <br><br>
                    <strong>It suits better in your training:</strong><br>
                    To study in France it is necessary to be fluent in French</p>
                <p> <strong>It suits better in your future</strong><br>
                    You plan a professional career immediately after the diploma</p>
                <p> <strong>It is cheaper</strong><br>
                    The spending of a student in France all inclusive: 1000 $ a month</p> 
            </div>
            
            <div class="content-text-list-points text">
                <h5 class="content-text-list-points-title click"><i>+</i> Program</h5>
                <div class="content-text-list-points-content justify no-display">
                    <p>
                        <strong>Introduction</strong> September -> December :<br>
                        English, French, Programming Java<br><br>
                        <strong>Fundamental</strong> December -> August : <br>
                        Projects<br>
                        Object approach Directed<br>
                        Network architecture<br>
                        Theoretical language<br>
                        Compilation<br>
                        Advanced algorithmics<br>
                        Network and Telecommunication<br>
                        Internet new generation<br>
                        Mobile and Conception of adaptive Web sites
                    </p>
                </div>
            </div>
        </section>
    </div>
</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
