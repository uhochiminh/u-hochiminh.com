<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 


ob_start();
pDoctype("start", array("Contact"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

	<div id="contact" class="content">
		<div class="situation">
			<h2>Contact</h2>
			<h3>You wish to contact us to have further information on our trainings or quite different thing? Do not hesitate.</h3>
		</div>
		<div class="position">
			<i class="fa fa-sitemap blue"></i>
			<a href="Accueil.html">Home</a>
			<i class="fa fa-angle-right"></i>
			Contact
		</div>
    	
    	<div class="content-text">
			<section class="content-text-maps">
				<div class="content-text-text">
					<iframe class="map" width="100%" height="450" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q=Vietnam%20National%20University%20Ho%20Chi%20Minh%20City%20%C4%90%C3%B4ng%20H%C3%B2a%20Th%E1%BB%A7%20%C4%90%E1%BB%A9c%20H%E1%BB%93%20Ch%C3%AD%20Minh%2C%20Vietnam&key=AIzaSyBGcfGQ42xvhWWixv7SiVymvilfg_AbHLE"></iframe>
				</div>
			</section>

			<div class="clear"></div>

			<section class="content-text-form">
				<h4 class="content-text-title-left"><span>Contact us</span></h4>
				<div class="content-text-text">
					<div class="notify blue no-display">Your message in well transmitted has your addressee.</div>
					<div class="notify red">An error arose during sending of your message.</div>
                    
					<form action="#" method="POST">
						<div class="element">
							<label>Last name and first name :</label>
							<input type="text" name="your-name" size="40" />
						</div>

						<div class="element">
							<label>Email :</label>
							<input type="email" name="your-email" size="40" />
						</div>

						<div class="element">
							<label>Object :</label>
							<input type="text" name="your-subject" size="70" />
						</div>

						<div class="element">
							<label>Message :</label>
							<textarea name="your-message" cols="40" rows="10"></textarea>
						</div>

						<div class="element button">
							<input type="submit" value="Envoyer" class="click" />
							<input type="reset" value="Effacer" class="click" />
						</div>
                        <div class="clear"></div>
					</form>
				</div>
			</section>

			<section class="content-text-sidebar">
				<h4 class="content-text-title-right"><span>Our address, email and phone number</span></h4>
				<div class="content-text-text">
					<div class="element address">
						<i class="fa fa-home"></i>
						<address class="value">French university center of the National University</address>
						<address class="value">District 6, Linh Trung Ward, Thu Duc District, 
						Ho Chi Minh-city, Vietnam</address>
                        <div class="clear"></div>
					</div>

					<div class="element email">
						<i class="fa fa-envelope"></i>
						<a class="value" href="mailto:contact@u-hochiminh.com">contact@u-hochiminh.com</a>
                        <div class="clear"></div>
					</div>

					<div class="element telephone">
						<i class="fa fa-phone"></i>
                        <a class="value" href="tel:+84837242169">+84 (0)837 242 169</a>
                        <div class="clear"></div>
					</div>

					<div class="element fax">
						<i class="fa fa-fax"></i>
                        <a class="value" href="tel:+84837242166">+84 (0)837 242 166</a>
                        <div class="clear"></div>
					</div>
				</div>
			</section>
            
            <div class="clear"></div>
		</div>
	</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
