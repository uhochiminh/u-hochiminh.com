<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

<div id="formation" class="content">
    <div class="situation">
        <h2>Master 2</h2>
        <h3>Découvrez notre master universitaire de niveau 2</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Accueil</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Formations</a>
		<i class="fa fa-angle-right"></i>
		M2
	</div>
    
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-diploma.png" />
                <p> Le Master 2 est une année de spécialisation. Il est possible de se spécialiser en Réseaux ou en Ingénieur Logiciel.</p>
                <p><strong> L'option Réseaux </strong>peut déboucher sur une carrière d'administrateur réseaux, par exemple.</p>
                <p><strong>L'option Ingénieur Logiciel, </strong>comme son nom l'indique, mène à un statut d'ingénieur.<br>
                C'est aussi une spécialisation possible pour devinir chef de projet, consultant ...</p>
            </div>
            <div class="content-text-list-points text">
                <h5 class="content-text-list-points-title click"><i>+</i>SPECIALISATION RESEAUX</h5> 
                <div class="content-text-list-points-content justify no-display">
                   <p>Insertion professionnelle<br>
                    Multimédia et Qualité de service<br>
                    Management de Système d'Information<br>
                    Voix sur IP<br>
                    Routeurs et routage<br>
                    Contrôle et trafic de réseaux<br>
                    Transferts multimédia sur réseaux IP<br>
                    Grands réseaux graphiques<br>
                    Catégories de transporteurs réseaux<br>
                    Communication digitale<br>
                    Sécurité des réseaux avancés<br>
                    Régulation d'internet<br>
                    Distribution et résistance aux attaques<br>
                    <strong>Stage :</strong> 4 à 6 mois en Janvier ou Février<br>
                    Stage (mémoire de thèse) de recherche et stage industriel</p>
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i>SPECIALISATION INGENIEUR LOGICIEL</h5> 
                <div class="content-text-list-points-content justify no-display">
                   <p>Architecture logicielle<br>
                    Management de projet<br>
                    Conception formelle<br>
                    Base de données avancée<br>
                    Architecture logiciel adaptative et avancée<br>
                    Projet de recherche et d'étude<br>
                    Communication<br>
                    Régulation d'internet<br>
                    <strong>Stage :</strong> 4 à 6 mois en Janvier ou Février<br>
                    Stage (mémoire de thèse) de recherche et stage industriel</p>
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i>PERSPECTIVES APRES LE DIPLÔME</h5>
                <div class="content-text-list-points-content justify no-display"> 
                    <p>Une carrière professionnelle dans les réseaux ou ingénieur logiciels 
                    (comme administrateur réseaux, consultant informatique, chef de projet ou ingénieur logiciel...)<br>
                    Carrière de chercheur après un doctorat</p> 
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i>LES DIPLÔMéS</h5> 
                <div class="content-text-list-points-content justify no-display">
                    <p>59 étudiants ingénieurs logiciels diplomés de 2008 à 2013<br>
                    Chefs de projet, chefs d'équipe, directeurs : 30%<br>
                    Professeurs, enseignants-chercheurs, chercheurs: 40%<br>
                    Etudes en doctorat : 5%<br>
                    Autre : 5%</p>
                </div>
            </div>
        </section>
    </div>
</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
