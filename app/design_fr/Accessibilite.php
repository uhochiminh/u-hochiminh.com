<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 

ob_start();
pDoctype("start", array("Accessibilité"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    
	<div id="accessibilite" class="content">
    	<div class="situation">
            <h2>Accessibilité</h2>
            <h3>Nous nous attachons à respecter les critères d'accessibilité sur notre site internet, conformément à la législation en vigueur.</h3>
        </div>
        <div class="position">
        	<i class="fa fa-sitemap blue"></i>
        	<a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i> 
            Accessibilité
		</div>
		
		<div class="content-text">
			<section class="content-text-presentation">
				<h4 class="content-text-title"><span>NAVIGATION PAR <strong>Tab</strong>ULATION</span></h4>
				<div class="content-text-text text justify">
                    <p>Il est possible d'utiliser la touche <strong>Tab</strong> (Tabulation) de votre clavier pour cheminer de lien en lien tout au long de la page. Pour cela :</p>
                  	<ul>
                    	<li>Appuyer sur la touche <strong>Tab</strong> et répéter jusqu'à sélectionner le lien désiré.</li>
                    	<li>Valider par <strong>Entrée</strong>.</li>
                    	<li>Pour revenir en arrière parmi les liens, utiliser la combinaison de touches <strong>Maj</strong> (Majuscules) + <strong>Tab</strong> (Tabulation).</li>
                    </ul>
                    <p> Attiention sous Safari, par défaut, la navigation par tabulation n'est pas activée. 
                        Pour l'activer : dans le menu des réglages aller dans 'Préférences', puis, dans 'Avancées', cocher la case permettant de naviguer par tabulation.</p>

                    <br/>
                    
                    <p>Voici la liste des raccourcis clavier utilisables sur ce site : </p>
                    <ul>
                        <li>Touche <strong>0</strong> - Politique d'accessibilité du site</li>
                        <li>Touche <strong>1</strong> - Page d'Accueil du site</li>
                        <li>Touche <strong>2</strong> - Page de présentation des Formations proposées</li>
                        <li>Touche <strong>3</strong> - Page d'espace aux Entreprises</li>
                        <li>Touche <strong>4</strong> - Infos Pratiques</li>
                        <li>Touche <strong>5</strong> - Contact</li>
                        <li>Touche <strong>6</strong> - Formulaire de Connexion</li>
                        <li>Touche <strong>7</strong> - Crédits et Mentions légales</li>
                        <li>Touche <strong>8</strong> - Plan du site</li>
                        <li>Touche <strong>f</strong> - Page Facebook</li>
                        <li>Touche <strong>t</strong> - Compte Twitter</li>
                        <li>Touche <strong>r</strong> - Barre de Recherches</li>
                    </ul>

                    <br/>
                    
                    <p>Les combinaisons de touches pour activer ces raccourcis sont différentes selon le navigateur et le système d'exploitation utilisés. Les procédures à suivre pour activer les raccourcis claviers dans les principales configurations sont les suivantes :</p>
                    <ul>
                        <li>Mozilla Firefox, Google Chrome, Safari sur Windows : touches <strong>Maj</strong> + Alt + [Raccourci clavier] (attention : utiliser les touches numériques du clavier et non celles du pavé numérique).</li>
                        <li>Internet Explorer sur Windows : touches Alt + Maj + [Raccourci clavier], puis touche <strong>Entrée</strong> (attention : utiliser les touches numériques du clavier et non celles du pavé numérique). AvecWindows Vista, utiliser la combinaison de touches Alt+ <strong>Maj</strong> + [Raccourci clavier].</li>
                        <li>Opera 7 sur Windows, Macintosh et Linux : Echap + <strong>Maj</strong> + [Raccourci clavier]</li>
                        <li>Safari sur Macintosh : <strong>Ctrl</strong> + <strong>Maj</strong> + [Raccourci clavier]</li>
                        <li>Mozilla et Netscape sur Macintosh : <strong>Ctrl</strong> + [Raccourci clavier]</li>
                        <li>Galeon, Mozilla FireFox sur Linux : Alt + [Raccourci clavier]</li>
                	</ul>

                    <br/>
                </div>
			</section>
            
			<section class="content-text-presentation">
				<h4 class="content-text-title"><span>AJUSTEMENT DE LA TAILLE DU TEXTE</span></h4>
				<div class="content-text-text text justify">
                	<p>Les textes du site ont une taille de police relative : ils peuvent être agrandis par l'utilisateur. Pour agrandir ou diminuer la taille d'affichage des textes, il est possible d'utiliser l'une des méthodes suivantes :</p>
                    <ul>
                        <li>Fonctionnalité "Taille du texte" présente dans le menu "Affichage" du navigateur.</li>
                        <li>Combinaison touche <strong><strong>Ctrl</strong></strong> + molette de la souris.</li>
                        <li>Combinaison touche <strong>Ctrl</strong> + touches "+" et "-" du pavé numérique.</li>
                    </ul>
                </div>
			</section>
            
			<section class="content-text-presentation">
				<h4 class="content-text-title"><span>RESPECT DES STANDARDS WEB</span></h4>
				<div class="content-text-text text justify">
                	<p>Ce site utilise les dernières technologies du <a href="#"><strong>W3C</strong></a>. Il offre un meilleur rendu avec les navigateurs de dernière génération, mais reste néanmoins consultable avec les navigateurs plus anciens.</p>
                </div>
			</section>
		</div>
	</div>	

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
pDoctype("end");

$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
