<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

<div id="formation" class="content">
    <div class="situation">
        <h2>LICENCE 2</h2>
        <h3>Découvrez notre licence universitaire de niveau 2</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Accueil</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Formations</a>
		<i class="fa fa-angle-right"></i>
		L2
	</div>
        
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-pratic-lesson.png" />
                <p>
                    Les deux premières années de cours indifférenciées permettent <br>
                    aux étudiants d’obtenir un premier diplôme universitaire professionnel : <br>
                    le DUT (Diplôme Universitaire de Technologie) d’informatique.    <br><br>
                    <strong>Programme : </strong><br>
                    Algorithmique et Programmation - Théorie et Pratique <br>
                    Architecture, Systèmes et Réseaux<br>
                    Outils et Méthodes du Génie Logiciel<br>
                    Mathématiques<br>
                    Economie et Gestion des organisations<br>
                    Expression et Communication<br>
                    Projets et Stage Professionnel<br><br>

                    Le Diplôme (DUT) est délivré par l'université de Bordeaux.
                </p>
            </div>
        </section>
    </div>
    </div>
    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
