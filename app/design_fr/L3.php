<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

 <div id="formation" class="content">
    <div class="situation">
        <h2>LICENCE 3</h2>
        <h3>Découvrez notre licence universitaire de niveau 3</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Accueil</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Formations</a>
		<i class="fa fa-angle-right"></i>
		L3
	</div>
    
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-projets.png" />
                <p> La troisième année présente deux parcours de référence :</p>
                <p><strong>La Licence Professionnelle</strong> apporte un solide capital de connaissances approprié à tout métier de 
                    l'informatique et permettant aux étudiants de prendre immédiatement des responsabilités informatiques 
                    dans les entreprises.</p>
                <p><strong>La Licence de Sciences et Technologies mention Informatique</strong> vise à faire acquérir des bases théoriques et 
                    pratiques permettant de poursuivre des études dans une spécialité de Master d'Informatique de Ho Chi Minh 
                    Ville (Spécialité Réseaux ou Spécialité Génie Logiciel) ou de n’importe quelle université française ou 
                    européenne.</p>
            </div>
            
            <div class="content-text-list-points text">
                <h5 class="content-text-list-points-title click"><i>+</i> PROGRAMME TRONC COMMUN</h5>
                <div class="content-text-list-points-content justify no-display">
                    <p><strong>Compétences managériales :</strong><br>
                        Management opérationel<br>
                        Ressources Humaines, Communication et Management
                    </p>
                    <p><strong>Compétences techniques :</strong><br>
                        Gestion des Systèmes d'Information<br>
                        Méthodes avancées d'ingénierie logicielle<br>
                        Méthodes avancées de développement
                    </p>
                    <p><strong>Compétences scientifiques et techniques :</strong><br>
                        Systèmes, Réseaux et Base de données<br>
                        Algorithmique et Mathématiques pour l'informatique<br>
                        Programmation et Développement
                    </p>
                    <p><strong>Projet</strong></p>
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i> LICENCE PROFESSIONNELLE</h5> 
                <div class="content-text-list-points-content justify no-display">
                    <p>    
                        <strong>Stage en entreprise</strong><br>
                        Diplôme délivré par l'Université de Bordeaux
                    </p> 
                </div>
                <h5 class="content-text-list-points-title click"><i>+</i> LICENCE INFORMATIQUE ET TECHNOLOGIES</h5> 
                <div class="content-text-list-points-content justify no-display">
                    <p>    
                        <strong>Connaissance de l'entreprise</strong><br>
                        Diplôme délivré par l'Université de Paris 6
                    </p> 
                </div>
            </div>
        </section>
    </div>
</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
