<?php  	
/* @Author: ROBIN Benoit -  */
require("_all.php"); 

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); 
        header('Status : 404 Not Found');
        header('HTTP/1.0 404 Not Found');
	?>	
	
    <div id="erreur" class="content">
        <div class="situation">
                <h2>Erreur 404</h2>
                <h3>Une erreur s'est produite durant le chargement de la page demandée.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Erreur 404
        </div>

        <div class="content-text">
            <section class="content-text-erreur">
                <div class="title">404</div>
                <div class="texte">La page demandée n'est pas ou plus accessible.</div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
