<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>
    
<div id="formation" class="content">
    <div class="situation">
        <h2>Master 1</h2>
        <h3>Découvrez notre master universitaire de niveau 1</h3>
    </div>
	<div class="position">
		<i class="fa fa-sitemap blue"></i>
		<a href="Accueil.html">Accueil</a>
		<i class="fa fa-angle-right"></i>
		<a href="Formations.html">Formations</a>
		<i class="fa fa-angle-right"></i>
		M1
	</div>
    
    <div class="content-text">
        <section class="content-text-formations-presentation">
            <div class="content-text-text text justify">
                <img src="images/formation-working.png" />
                <p> Pourquoi à Ho Chi Minh plutôt qu'en France ? <br><br>
                    <strong>Cela convient mieux à votre formation:</strong><br>
                    pour étudier en France il faut parler le français couramment</p>
                <p> <strong>Cela convient mieux à votre avenir</strong><br>
                    Vous planifiez une carrière professionnelle immédiatement après le diplôme</p>
                <p> <strong>C'est moins cher</strong><br>
                    Les dépenses d'un étudiant en France tout compris : 1000 $ par mois</p> 
            </div>
            
            <div class="content-text-list-points text">
                <h5 class="content-text-list-points-title click"><i>+</i> PROGRAMME</h5>
                <div class="content-text-list-points-content justify no-display">
                    <p>
                        <strong>Introduction</strong> Septembre -> Décembre :<br>
                        Anglais, Français, Programmation Java<br><br>
                        <strong>Fondamentaux</strong> Décembre -> Août : <br>
                        Projets<br>
                        Approche Orientée Objet<br>
                        Architecture Réseau<br>
                        Langage théorique<br>
                        Compilation<br>
                        Algorithmique avancée<br>
                        Réseau et Télécommunication<br>
                        Internet nouvelle génération<br>
                        Mobile et Conception de sites web adaptatifs
                    </p>
                </div>
            </div>
        </section>
    </div>
</div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
