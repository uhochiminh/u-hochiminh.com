<?php
/* @Author: ROBIN Benoit -  */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); ?>

    <div id="formations" class="content">
        <div class="situation">
            <h2>FORMATIONS</h2>
            <h3>Découvrez toutes nos formations universitaires aux standards internationaux en matière d'enseignement.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Formations
        </div>

        <div class="content-text">
            <section class="content-text-formations-slider">
                <img src="images/formations-amphi.png" alt="Amphithéâtres">
                <ul>
                    <li><a href="L1.html">Licence 1</a></li>
                    <li><a href="L2.html">Licence 2</a></li>
                    <li><a href="L3.html">Licence 3</a></li>
                    <li class="expand"><a href="M1.html">Master 1</a></li>
                    <li class="expand"><a href="M2.html">Master 2</a></li>
                </ul>
            </section>

            <section class="content-text-formations-presentation">
                <h4 class="content-text-title"><span>Les formations</span></h4>
                <div class="content-text-text text alinea justify">
                    <p>Le Vietnam connaît actuellement un développement économique considérable dans tous les domaines professionnels.
                        Dans ce contexte, les technologies de l’information constituent un levier essentiel dans la modernisation de l’économie vietnamienne.
                        Les entreprises installées au Vietnam, qu’elles soient publiques ou privées, vietnamiennes ou internationales, ressentent aujourd’hui un besoin crucial d’informaticiens de haut niveau. </p>
                    <p>Le PUF Ho Chi Minh a pour principale activité d'animer des formations universitaires françaises délocalisées au Vietnam. Toutes les formations suivent les principes suivants :</p>
                    <ul>
                        <li>Elles sont <strong>identiques</strong> à des formations dispensées en France, et lient la théorie et la pratique.</li>
                        <li>Elles sont assurées à au moins 50% par des enseignants des universités françaises partenaires, ou par des professionnels qui leurs sont associées.</li>
                        <li>Elles mènent à un diplôme français du schéma LMD, <strong>reconnu partout en Europe</strong> et par équivalence dans de nombreux pays.</li>
                    </ul>
                </div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end");
$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
