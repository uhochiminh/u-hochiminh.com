<?php
return array(
//--------------------------
// Position
//--------------------------

'POSITION_HOMEPAGE' => 'Homepage',
'POSITION_PRACTICAL_INFORMATION' => 'Practical Information',
'POSITION_TRAINING' => 'Training',
'POSITION_COMPANIES' => 'Companies',
'POSITION_CONTACT' => 'Contact',
'POSITION_CONNEXION' => 'Connexion',
'POSITION_ACCESSIBILITY' => 'Accessibility',
'POSITION_LEGAL_NOTICES' => 'Legal Notices',
'POSITION_SITE_MAP' => 'Site map',
'POSITION_MISSIONS' => 'Missions',
'POSITION_500' => 'Error 500',
'POSITION_404' => 'Error 404',
'POSITION_401' => 'Error 401',
'POSITION_403' => 'Error 403',

//--------------------------
// Formulaire
//--------------------------

'FORM_SEND' => 'Send',
'FORM_DELETE' => 'Delete',

//--------------------------
// header.php
//--------------------------

'MENU_TRAINING' => 'Traning',
'MENU_COMPANIES' => 'Companies',
'MENU_PRACTICAL_INFORMATION' => 'Practical Information',
'MENU_CONTACT' => 'Contact',
'SEARCH' => 'Research...',

//--------------------------
// footer.php
//--------------------------

'LOCATION_TITLE' => 'OUR ADDRESS',
'LOCATION_NAME' => 'Pôle Universitaire Français de l\'Université Nationale',
'LOCATION_ADDRESS' => 'Quartier 6, Linh Trung Ward, Thu Duc District, Ho Chi Min-Ville, Vietnam',
'LOCATION_PHONE' => 'Phone',
'LOCATION_FAX' => 'Fax',

'LINKS_TITLE' => 'LINKS',
'LINKS_ACCESSIBILITY' => 'Accessibility',
'LINKS_CONNEXION' => 'Connexion',
'LINKS_LEGAL_NOTICES' => 'Legal Notices',
'LINKS_SITE_MAP' => 'Site map',
'LINKS_CONTACT' => 'Contact',

'SOCIAL_NETWORKS_TITLE' => 'SOCIAL NETWORKS',
'SOCIAL_NETWORKS_RSS' => 'RSS Feed',
'SOCIAL_NETWORKS_FACEBOOK' => 'Facebook',
'SOCIAL_NETWORKS_TWITTER' => 'Twitter',

'DESIGNED_AND_DEVELOPED_BY' => 'Designed and Developed by',
'COPYRIGHT' => 'Copyright',
'ALL_RIGHT_RESERVED' => 'All Right Reserved',

//--------------------------
// Acceuil.php
//--------------------------

'HOMEPAGE_CONTENT_PRESENTATION_TITLE' => 'Presentation',
'HOMEPAGE_CONTENT_PARTNERS_TITLE' => 'Partners',
'HOMEPAGE_SITUATION_TITLE' => 'HOMEPAGE',
'HOMEPAGE_SITUATION_TXT' => 'The IT department of the French University center ( PUF) of Ho Chi Minh City ( HCMV) proposes trainings of the 1st year of university in the 5th year of university.',


//--------------------------
// Formations.php
//--------------------------

'TRAINING_SITUATION_TITLE' => 'TRAINING',
'TRAINING_SITUATION_TXT' => 'Discover all our university education to the international standards regarding education.',
'TRAINING_CONTENT_LICENCE' => 'Licence',
'TRAINING_CONTENT_MASTER' => 'Master',
'TRAINING_CONTENT_PRESENTATION_TITLE' => 'Training',

//--------------------------
// Entreprises.php
//--------------------------

'COMPANIES_SITUATION_TITLE' => 'COMPANIES',
'COMPANIES_SITUATION_TXT' => 'We train professionals and as such, all the actors of the economy have authority to be our partners.',
'COMPANIES_CONTENT_SEARCH_SKILL_TITLE' => 'You look for skills in computing ?',
'COMPANIES_CONTENT_INTERNSHIP_TITLE' => 'Interships',
'COMPANIES_CONTENT_PARTICIPATE_TITLE' => 'Participate in our teachings',

//--------------------------
// InfosPratiques.php
//--------------------------

'PRACTICAL_INFORMATION_SITUATION_TITLE' => 'PRACTICAL INFORMATION',
'PRACTICAL_INFORMATION_SITUATION_TXT' => 'You will find here all the practical information in the life as students (university calendar, access in university canteen, useful links)',
'PRACTICAL_INFORMATION_READ_NEXT_BUTTON' => 'Read next',
'PRACTICAL_INFORMATION_POSTED_ON' => 'Posted on',
'PRACTICAL_INFORMATION_BY' => 'by',

//--------------------------
// Contact.php
//--------------------------

'CONTACT_SITUATION_TITLE' => 'CONTACT',
'CONTACT_SITUATION_TXT' => 'You wish to contact us to have further information on our trainings or quite different thing? Do not hesitate.',
'CONTACT_ADDRESS' => 'Our Coordinates',
'CONTACT_FORM_TITLE' => 'Contact us',
'CONTACT_FORM_LAST_NAME_FIRST_NAME' => 'Last_name & First_name :',
'CONTACT_FORM_EMAIL' => 'Email',
'CONTACT_FORM_OBJECT' => 'Object',
'CONTACT_FORM_MESSAGE' => 'Message',

//--------------------------
// Connexion.php
//--------------------------

'CONNEXION_SITUATION_TITLE' => 'CONNEXION',
'CONNEXION_SITUATION_TXT' => 'This page is to reserve for the staff supervising of the PUF of Ho Chi Minh.',
'CONNEXION_LOGIN' => 'Login',
'CONNEXION_PASSWORD' => 'Password',
'CONNEXION_CONNEXION' => 'Connexion',
'CONNEXION_ERROR' => 'Login or password disabled',


//--------------------------
// Recherche.php
//--------------------------



//--------------------------
// Accessibilite.php
//--------------------------

'ACCESSIBILITY_SITUATION_TITLE' => 'ACCESSIBILITY',
'ACCESSIBILITY_SITUATION_TXT' => 'We attempt to respect the criteria of accessibility on our web site, according to the legislation in force.',
'ACCESSIBILITY_NAVIGATION_TITLE' => 'NAVIGATION BY TABULATION',
'ACCESSIBILITY_TEXTE_ADJUSTMENT_TITLE' => 'ADJUSTMENT OF THE SIZE OF THE TEXT',
'ACCESSIBILITY_STANDARD_RESPECT' => 'RESPECT FOR THE STANDARDS WEB',

//--------------------------
// MentionsLegales.php
//--------------------------

'LEGAL_NOTICES_SITUATION_TITLE' => 'LEGAL NOTICES',
'LEGAL_NOTICES_SITUATION_TXT' => 'You will find here all the information concerning the creation of this site as well as its context.',
'LEGAL_NOTICES_OWNER' => 'Owner of the Web site',
'LEGAL_NOTICES_HOST_PROVIDER' => 'Host provider',
'LEGAL_NOTICES_DESIGN' => 'Web site design',

//--------------------------
// PlanDuSite.php
//--------------------------

'SITE_MAP_SITUATION_TITLE' => 'Site map',
'SITE_MAP_SITUATION_TXT' => 'The site map below will allow you to find its architecture and to navigate there in a intuitive way.',
'SITE_MAP_HOMEPAGE' => 'Homepage',
'SITE_MAP_TRAINING' => 'Training',
'SITE_MAP_LICENCE' => 'Licence',
'SITE_MAP_MASTER' => 'Master',
'SITE_MAP_COMPANIES' => 'Companies',
'SITE_MAP_PRACTICAL_INFORMATION' => 'Practical Information',
'SITE_MAP_ACCESSIBILITY' => 'Accessibility',
'SITE_MAP_CONNEXION' => 'Connexion',
'SITE_MAP_LEGAL_NOTICES' => 'Legal Notices',
'SITE_MAP_CONTACT' => 'Contact',


//--------------------------
// Missions.php
//--------------------------

'MISSIONS_SITUATION_TITLE' => 'Missions',
'MISSIONS_SITUATION_TXT' => 'Discover the missions organize within the IT department of the PUF by our french professors.',
'MISSIONS_FORM_TITLE' => 'Add a training',
'MISSIONS_FORM_PROFESSOR_LAST_NAME_FIRST_NAME' => 'Teacher - Last_name & First_name :',
'MISSIONS_FORM_PROFESSOR_EMAIL' => 'Teacher - Email :',
'MISSIONS_FORM_MANAGER_LAST_NAME_FIRST_NAME' => 'Manager - Last_name & First_name :',
'MISSIONS_FORM_MANAGER_EMAIL' => 'Manager - Email :',
'MISSIONS_FORM_FROM' => 'Period of :',
'MISSIONS_FORM_TO' => 'To :',
'MISSIONS_FORM_MESSAGE' => 'Message :',
'MISSIONS_FORM_LIAISON_FILE' => 'Liaison file :',

//--------------------------
// 500.php
//--------------------------

'ERROR_500_SITUATION_TITLE' => 'Error 500',
'ERROR_500_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_500_CONTENT_TITLE' => '500',
'ERROR_500_CONTENT_TEXTE' => 'Our service is for a moment unavailable.',

//--------------------------
// 404.php
//--------------------------

'ERROR_404_SITUATION_TITLE' => 'Error 404',
'ERROR_404_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_404_CONTENT_TITLE' => '404',
'ERROR_404_CONTENT_TEXTE' => 'The wanted page is not or more accessible',

//--------------------------
// 403.php
//--------------------------

'ERROR_403_SITUATION_TITLE' => 'Error 403',
'ERROR_403_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_403_CONTENT_TITLE' => '403',
'ERROR_403_CONTENT_TEXTE' => 'Your access rights do not allow to reach this resource.',

//--------------------------
// 401.php
//--------------------------

'ERROR_401_SITUATION_TITLE' => 'Error 401',
'ERROR_401_SITUATION_TXT' => 'An error occurred during the load of the wanted page.',
'ERROR_401_CONTENT_TITLE' => '401',
'ERROR_401_CONTENT_TEXTE' => 'UAn authentification is necessary to reach the resource.'
);