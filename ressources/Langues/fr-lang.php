<?php
return array(
//--------------------------
// Position
//--------------------------

'POSITION_HOMEPAGE' => 'Acceuil',
'POSITION_PRACTICAL_INFORMATION' => 'Infos Pratiques',
'POSITION_TRAINING' => 'Formations',
'POSITION_COMPANIES' => 'Entreprises',
'POSITION_CONTACT' => 'Contact',
'POSITION_CONNEXION' => 'Connexion',
'POSITION_ACCESSIBILITY' => 'Accessibilité',
'POSITION_LEGAL_NOTICES' => 'Mentions légales',
'POSITION_SITE_MAP' => 'Plan du site',
'POSITION_MISSIONS' => 'Missions',
'POSITION_500' => 'Erreur 500',
'POSITION_404' => 'Erreur 404',
'POSITION_401' => 'Erreur 401',
'POSITION_403' => 'Erreur 403',

//--------------------------
// Formulaire
//--------------------------

'FORM_SEND' => 'Envoyer',
'FORM_DELETE' => 'Effacer',

//--------------------------
// header.php
//--------------------------

'MENU_TRAINING' => 'Formations',
'MENU_COMPANIES' => 'Entreprises',
'MENU_PRACTICAL_INFORMATION' => 'Infos Pratiques',
'MENU_CONTACT' => 'Contact',
'SEARCH' => 'Saisir votre recherche...',

//--------------------------
// footer.php
//--------------------------

'LOCATION_TITLE' => 'NOTRE ADRESSE',
'LOCATION_NAME' => 'Pôle Universitaire Français de l\'Université Nationale',
'LOCATION_ADDRESS' => 'Quartier 6, Linh Trung Ward, Thu Duc District, Ho Chi Min-Ville, Vietnam',
'LOCATION_PHONE' => 'Téléphone',
'LOCATION_FAX' => 'Fax',

'LINKS_TITLE' => 'LIENS',
'LINKS_ACCESSIBILITY' => 'Accessibilité',
'LINKS_CONNEXION' => 'Connexion',
'LINKS_LEGAL_NOTICES' => 'Mentions Légales',
'LINKS_SITE_MAP' => 'Plan du site',
'LINKS_CONTACT' => 'Contact',

'SOCIAL_NETWORKS_TITLE' => 'RESEAUX SOCIAUX',
'SOCIAL_NETWORKS_RSS' => 'Flux RSS',
'SOCIAL_NETWORKS_FACEBOOK' => 'Facebook',
'SOCIAL_NETWORKS_TWITTER' => 'Twitter',

'DESIGNED_AND_DEVELOPED_BY' => 'Déssiné et Développé par',
'COPYRIGHT' => 'Droit d\'auteur',
'ALL_RIGHT_RESERVED' => 'Tous droits réservés',

//--------------------------
// Acceuil.php
//--------------------------

'HOMEPAGE_CONTENT_PRESENTATION_TITLE' => 'Présentation',
'HOMEPAGE_CONTENT_PARTNERS_TITLE' => 'Partenaires',
'HOMEPAGE_SITUATION_TITLE' => 'ACCUEIL',
'HOMEPAGE_SITUATION_TXT' => 'Le département Informatique du Pôle Universitaire Français (PUF) de Ho Chi Minh Ville (HCMV) propose des formations de la Licence 1 au Master 2.',


//--------------------------
// Formations.php
//--------------------------

'TRAINING_SITUATION_TITLE' => 'FORMATIONS',
'TRAINING_SITUATION_TXT' => 'Découvrez toutes nos formations universitaires aux standards internationaux en matière d\'enseignement.',
'TRAINING_CONTENT_LICENCE' => 'Licence',
'TRAINING_CONTENT_MASTER' => 'Master',
'TRAINING_CONTENT_PRESENTATION_TITLE' => 'Les formations',

//--------------------------
// Entreprises.php
//--------------------------

'COMPANIES_SITUATION_TITLE' => 'ENTREPRISES',
'COMPANIES_SITUATION_TXT' => 'Nous formons des professionnels et à ce titre, tous les acteurs de l\'économie ont vocation à être nos partenaires.',
'COMPANIES_CONTENT_SEARCH_SKILL_TITLE' => 'Vous recherchez des compétences en informatique ?',
'COMPANIES_CONTENT_INTERNSHIP_TITLE' => 'Les stages',
'COMPANIES_CONTENT_PARTICIPATE_TITLE' => 'Participer à nos enseignements',

//--------------------------
// InfosPratiques.php
//--------------------------

'PRACTICAL_INFORMATION_SITUATION_TITLE' => 'INFOS PRATIQUES',
'PRACTICAL_INFORMATION_SITUATION_TXT' => 'Vous trouverez ici toutes les informations pratiques à la vie en tant qu\'étudiants (calendrier universitaire, accès aux restaurant U, liens utiles ...)',
'PRACTICAL_INFORMATION_READ_NEXT_BUTTON' => 'Lire la suite',
'PRACTICAL_INFORMATION_POSTED_ON' => 'Posté le',
'PRACTICAL_INFORMATION_BY' => 'par',

//--------------------------
// Contact.php
//--------------------------

'CONTACT_SITUATION_TITLE' => 'CONTACT',
'CONTACT_SITUATION_TXT' => 'Vous souhaitez nous contacter pour avoir des informations complémentaires sur nos formations ou toute autre chose ? N\'hésitez pas.',
'CONTACT_ADDRESS' => 'Nos coordonnées',
'CONTACT_FORM_TITLE' => 'Nous contacter',
'CONTACT_FORM_LAST_NAME_FIRST_NAME' => 'Nom & Prénom :',
'CONTACT_FORM_EMAIL' => 'Mail',
'CONTACT_FORM_OBJECT' => 'Objet',
'CONTACT_FORM_MESSAGE' => 'Message',

//--------------------------
// Connexion.php
//--------------------------

'CONNEXION_SITUATION_TITLE' => 'CONNEXION',
'CONNEXION_SITUATION_TXT' => 'Cette page est réserver au personnel encadrant du PUF de Ho Chi Minh.',
'CONNEXION_LOGIN' => 'Identifiant',
'CONNEXION_PASSWORD' => 'Mot de passe',
'CONNEXION_CONNEXION' => 'Connexion',
'CONNEXION_ERROR' => 'Identifiant ou mots de passe invalide.',


//--------------------------
// Recherche.php
//--------------------------



//--------------------------
// Accessibilite.php
//--------------------------

'ACCESSIBILITY_SITUATION_TITLE' => 'ACCESSIBILITE',
'ACCESSIBILITY_SITUATION_TXT' => 'Nous nous attachons à respecter les critères d\'accessibilité sur notre site internet, conformément à la législation en vigueur.',
'ACCESSIBILITY_NAVIGATION_TITLE' => 'NAVIGATION PAR TABULATION',
'ACCESSIBILITY_TEXTE_ADJUSTMENT_TITLE' => 'AJUSTEMENT DE LA TAILLE DU TEXTE',
'ACCESSIBILITY_STANDARD_RESPECT' => 'RESPECT DES STANDARDS WEB',

//--------------------------
// MentionsLegales.php
//--------------------------

'LEGAL_NOTICES_SITUATION_TITLE' => 'MENTIONS LEGALES',
'LEGAL_NOTICES_SITUATION_TXT' => 'Vous trouverez ici toutes les informations concernant la création de ce site ainsi que son contexte.',
'LEGAL_NOTICES_OWNER' => 'Propriétaire du site',
'LEGAL_NOTICES_HOST_PROVIDER' => 'Hébergeur',
'LEGAL_NOTICES_DESIGN' => 'Conception du site',

//--------------------------
// PlanDuSite.php
//--------------------------

'SITE_MAP_SITUATION_TITLE' => 'Plan du site',
'SITE_MAP_SITUATION_TXT' => 'Le plan du site ci-dessous vous permettra de retrouver son architecture et d\'y naviguer de façon intuitive.',
'SITE_MAP_HOMEPAGE' => 'Accueil',
'SITE_MAP_TRAINING' => 'Formations',
'SITE_MAP_LICENCE' => 'Licence',
'SITE_MAP_MASTER' => 'Master',
'SITE_MAP_COMPANIES' => 'Entreprises',
'SITE_MAP_PRACTICAL_INFORMATION' => 'Infos Pratiques',
'SITE_MAP_ACCESSIBILITY' => 'Accessibilité',
'SITE_MAP_CONNEXION' => 'Connexion',
'SITE_MAP_LEGAL_NOTICES' => 'Mentions Légales',
'SITE_MAP_CONTACT' => 'Contact',


//--------------------------
// Missions.php
//--------------------------

'MISSIONS_SITUATION_TITLE' => 'Missions',
'MISSIONS_SITUATION_TXT' => 'Découvrez les missions organiser au sein du département informatique du PUF par nos professeurs Français.',
'MISSIONS_FORM_TITLE' => 'Ajouter une formation',
'MISSIONS_FORM_PROFESSOR_LAST_NAME_FIRST_NAME' => 'Enseignant - Nom & Prénom :',
'MISSIONS_FORM_PROFESSOR_EMAIL' => 'Enseignant - Mail :',
'MISSIONS_FORM_MANAGER_LAST_NAME_FIRST_NAME' => 'Chargé de TD - Nom & Prénom :',
'MISSIONS_FORM_MANAGER_EMAIL' => 'Chargé de TD - Mail :',
'MISSIONS_FORM_FROM' => 'Période du :',
'MISSIONS_FORM_TO' => 'Au :',
'MISSIONS_FORM_MESSAGE' => 'Message :',
'MISSIONS_FORM_LIAISON_FILE' => 'Fiche de liaison :',

//--------------------------
// 500.php
//--------------------------

'ERROR_500_SITUATION_TITLE' => 'Erreur 500',
'ERROR_500_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_500_CONTENT_TITLE' => '500',
'ERROR_500_CONTENT_TEXTE' => 'Notre service est momentanément indisponible.',

//--------------------------
// 404.php
//--------------------------

'ERROR_404_SITUATION_TITLE' => 'Erreur 404',
'ERROR_404_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_404_CONTENT_TITLE' => '404',
'ERROR_404_CONTENT_TEXTE' => 'La page demandée n\'est pas ou plus accessible.',

//--------------------------
// 403.php
//--------------------------

'ERROR_403_SITUATION_TITLE' => 'Erreur 403',
'ERROR_403_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_403_CONTENT_TITLE' => '403',
'ERROR_403_CONTENT_TEXTE' => 'Vos droits d\'accès ne permettent pas d\'accéder à cette ressource.',

//--------------------------
// 401.php
//--------------------------

'ERROR_401_SITUATION_TITLE' => 'Erreur 401',
'ERROR_401_SITUATION_TXT' => 'Une erreur s\'est produite durant le chargement de la page demandée.',
'ERROR_401_CONTENT_TITLE' => '401',
'ERROR_401_CONTENT_TEXTE' => 'Une authentification est nécessaire pour accéder à la ressource.'
);