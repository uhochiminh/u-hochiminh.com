-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 09 Janvier 2015 à 00:27
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `pt_c`
--
CREATE DATABASE IF NOT EXISTS `pt_c` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pt_c`;

-- --------------------------------------------------------

--
-- Structure de la table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `_id` int(2) NOT NULL AUTO_INCREMENT,
  `_language` varchar(45) NOT NULL,
  `_image` text NOT NULL,
  `_url` varchar(2) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `language`
--

INSERT INTO `language` (`_id`, `_language`, `_image`, `_url`) VALUES
(1, 'Français', 'http://', 'fr'),
(2, 'English', 'http://', 'en');

-- --------------------------------------------------------

--
-- Structure de la table `missions`
--

CREATE TABLE IF NOT EXISTS `missions` (
  `_id` int(8) NOT NULL AUTO_INCREMENT,
  `_author` int(4) NOT NULL,
  `_dateStart` datetime NOT NULL,
  `_dateEnd` datetime NOT NULL,
  `_profName` varchar(70) NOT NULL,
  `_profMail` varchar(70) NOT NULL,
  `_ctdName` varchar(70) DEFAULT NULL,
  `_ctdMail` varchar(70) DEFAULT NULL,
  `_description` longtext,
  `_file` text,
  PRIMARY KEY (`_id`,`_author`),
  KEY `fk_missions_user1` (`_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `missions`
--

INSERT INTO `missions` (`_id`, `_author`, `_dateStart`, `_dateEnd`, `_profName`, `_profMail`, `_ctdName`, `_ctdMail`, `_description`, `_file`) VALUES
(1, 1, '2014-11-17 00:00:00', '2014-11-25 00:00:00', 'Olivier Frigo', 'olivier.frigo@u-hochiminh.com', 'Thomas Edison', 'thomas.edison@u-hochiminh.com', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque velit ligula, porttitor ac convallis sed, tincidunt vel sapien. Donec ultrices eros ligula, in tincidunt eros fermentum lobortis. Donec vel lacus elit. Vivamus euismod ut ante ac posuere. Proin faucibus urna et elit feugiat, mattis consectetur erat aliquam. In vulputate efficitur magna a ullamcorper. Suspendisse aliquet lobortis lobortis. Quisque at massa mauris. Integer scelerisque lacinia nisl, a mollis urna semper non.</p>\r\n                                                <p>Fusce a dolor ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis pulvinar mollis. Suspendisse id ante feugiat, hendrerit odio vitae, blandit leo. Ut posuere magna et mi venenatis consequat. Nam neque mi, posuere vitae orci eget, facilisis sagittis lorem. Mauris at semper dui. Proin et leo placerat, suscipit metus a, laoreet eros. Ut tempus tellus a sapien ultricies ullamcorper. Suspendisse nec mollis tellus. In ac ligula tristique, scelerisque nulla id, luctus mi. ...</p>', NULL),
(2, 1, '2015-01-30 00:00:00', '2015-01-31 00:00:00', 'Dimitri Fabien', 'dimer47@gmail.com', 'Dimitri Fabien', 'dimer47@gmail.com', '<p>sdgsdf</p>', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `_id` int(4) NOT NULL AUTO_INCREMENT,
  `_language` int(2) NOT NULL,
  `_page` int(2) NOT NULL,
  `_type` varchar(15) NOT NULL,
  `_title` varchar(50) DEFAULT NULL,
  `_content` text NOT NULL,
  PRIMARY KEY (`_id`,`_language`,`_page`),
  KEY `fk_modules_page1_idx` (`_page`,`_language`),
  KEY `_language` (`_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `module`
--

INSERT INTO `module` (`_id`, `_language`, `_page`, `_type`, `_title`, `_content`) VALUES
(2, 1, 2, 'text-block', 'Présentation', '<p>Le département informatique du Pôle Universitaire Français de Ho Chi Minh vous présente ici les formations qu''il propose.\n                                Nous espérons que vous trouverez les informations que vous cherchez, quelque soit votre profil : entreprise, étudiant, enseignant, futur étudiant, ...</p>\n                                <p>Les formations proposées sont sur divers domaines de l''informatique et peuvent amener vers plusieurs niveaux (de la Licence 1 au Master 2).\n                                    <br>Nos programmes sont fournis par de grandes universités internationales et nos points forts sont des chercheurs de classe mondiale et des formateurs professionnels expérimentés.\n                                    <br>Nos diplômes sont accrédités selon les plus hauts standards académiques et offrent crédibilité et reconnaissance mondiale.</p>\n                                    <p>Profitez de nos liens étroits avec les plus grandes sociétés locales et internationales pour des occasions d''apprentissage supplémentaires.\n                                        <br>Notre accent sur les aspects techniques, l''apprentissage basé sur les compétences vous donne la possibilité de poursuivre une carrière dans la recherche, les universités ou l''industrie.</p>'),
(3, 1, 2, 'list', NULL, 'POURQUOI ÉTUDIER AU PUF\n<--separator-->\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i>1</i></div>\n	<p><strong>Des universités européennes de premier plan :</strong><br>\n		Nos formations sont délivrées par des enseignants et chercheurs issus d’Universités de niveau international.</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i>2</i></div>\n	<p><strong>Des diplômes internationalement reconnus :</strong><br>\n		Nos diplômes sont homologués et vous permettront d’intégrer les universités européennes et nord américaines.</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i>3</i></div>\n	<p><strong>Des partenaires industriels majeurs :</strong><br>\n		Nos liens étroits avec le monde industriel local et international vous offrent des opportunités de stage et d’apprentissage.</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i>4</i></div>\n	<p><strong>Des perspectives de carrières : </strong><br>\n		Notre enseignement vous permet d’acquérir les compétences techniques et théoriques qui vous autorisent indifféremment une carrière orientée vers la recherche, l’enseignement ou l’entreprise.</p>\n</div>\n<----separator---->\n\n\nPOUR QUELS DIPLÔMES\n<--separator-->\n<p><strong>Licence en informatique :</strong></p>\n<p>Notre licence vous apporte en 3 ans les compétences et connaissances nécessaires pour participer à la conception, au développement et à la maintenance de systèmes informatiques :\n	- Conception, développement, validation, suivi et maintenance de logiciels,\n	- Installation, administration et maintenance de systèmes et réseaux informatiques.</p>\n\n<p><strong>Master Réseaux :</strong></p>\n<p>Notre Master  en services Internet et Mobiles vous forme en 2 ans en concentrant ses enseignements sur la conception et le développement de systèmes informatiques mobiles et de dimension internationale, sur la conception d''une infrastructure réseau pouvant soutenir ces systèmes informatiques, ainsi que sur la conception de systèmes évolutifs qui exploitent des architectures distribuées. </p>\n\n<p><strong>Master Génie Logiciel :</strong></p>\n<p>Notre Master en Genie Logiciel vous forme en 2 ans en concentrant ses enseignements sur les méthodes formelles, des méthodologies aguerries de management de projet, ainsi que des compétences pratiques, afin de former des experts en conception et développement de systèmes logiciels efficaces et sûrs. </p>\n<----separator---->\n\n\nDES FORMATIONS DE NIVEAU INTERNATIONAL \n<--separator-->\n<p>Créé en 2006, le PUF délivre des formations universitaires en informatique en partenariat avec les meilleures universités françaises et européennes.\nNos Licence et Master vous offrent une formation technique de grande qualité couplée à une réelle expérience pratique.</p>\n<p>Ces diplômes vous permettent d’acquérir des compétences techniques et managériales vous préparant à occuper des postes de décideurs dans le secteur des technologies de l’information et de la communication.<p>\n<----separator---->\n\n\nDES OPPORTUNITÉS DE CARRIÈRES\n<--separator-->\n<p>Le secteur des TIC souffre aujourd’hui d’une pénurie mondiale de personnels qualifiés. Nos diplômes sont donc porteurs sur le marché du travail et garantissent à nos diplômés des progressions de carrières rapides et des salaires élevés. Nos étudiants acquièrent des compétences de management essentielles aux leaders informatiques du monde du travail compétitif actuel. </p>\n<----separator---->\n\n\nNOS POINTS FORTS\n<--separator-->\n\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i class="fa fa-share-alt"></i></div>\n	<p>Le PUF entretient des liens étroits avec l’Europe au travers de partenariats industriels, de collaborations de recherche et d’échanges d’étudiants et d''enseignants.</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i class="fa fa-graduation-cap"></i></div>\n	<p>Les formations du PUF sont assurées par une équipe expérimentée d’enseignants et de professionnels du secteur des TIC.</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i class="fa fa-book"></i></div>\n	<p>Nos pratiques pédagogiques allient cours magistraux, travaux dirigés, travaux pratiques et travaux en « groupe projet » pour vous permettre de développer une\n		véritable expérience pratique et vous préparer au mieux à une insertion professionnelle optimale.</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i class="fa fa-flag"></i></div>\n	<p>Avec une formation délivrée en français ou en anglais vous pourrez acquérir les compétences linguistiques indispensables à votre réussite en milieu\n		professionnel..</p>\n</div>\n<div class="content-text-list-points-content-point">\n	<div class="point-circle"><i class="fa fa-briefcase"></i></div>\n	<p>Vous bénéficierez enfin de nos liens étroits avec les entreprises nationales et internationales du secteur des TIC afin de préparer votre future carrière.</p>\n</div>'),
(6, 1, 2, 'slider', NULL, 'http://;\nhttp://;\nhttp://;'),
(7, 1, 2, 'parteners', 'Partenaires', 'http://;;;http://;;;Partenaire 1;;;;\r\nhttp://;;;http://;;;Partenaire 2;;;;\r\nhttp://;;;http://;;;Partenaire 3;;;;\r\nhttp://;;;http://;;;Partenaire 4;;;;'),
(8, 1, 15, 'maps', NULL, '<iframe class="map" width="100%" height="450" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q=Vietnam%20National%20University%20Ho%20Chi%20Minh%20City%20%C4%90%C3%B4ng%20H%C3%B2a%20Th%E1%BB%A7%20%C4%90%E1%BB%A9c%20H%E1%BB%93%20Ch%C3%AD%20Minh%2C%20Vietnam&amp;key=AIzaSyBGcfGQ42xvhWWixv7SiVymvilfg_AbHLE" kwframeid="1"></iframe>'),
(9, 1, 13, 'text-block', 'Vous recherchez des compétences en informatiq', '<p>Nous formons des professionnels et à ce titre, tous les acteurs de l''économie ont vocation à <strong>être nos partenaires</strong>. \n								Ce partenariat, avec le monde professionnel, est pour nous une priorité. De plus, nos étudiants disposent de la formation adaptée pour participer au développement de votre entreprise.</p>\n					'),
(10, 1, 13, 'text-block', 'Les stages', '<p>Les stages professionnels constituent un des points forts de notre relation avec les entreprises. Pour nos étudiants, le stage est l''occasion de <strong>mettre en pratique les connaissances acquises</strong> et de compléter significativement leur formation. En retour, l''expérience montre que les professionnels peuvent compter sur nos stagiaires en qui ils trouvent le plus souvent des <strong>collaborateurs compétents</strong> et rapidement <strong>opérationnels.</strong></p>\n	\n							<p>Ils peuvent vous rejoindre en stage :</p>\n\n							<br>\n\n							<ul>\n								<li><strong>Les stages de Licence :</strong></li>\n								<ul>\n									<li>après 2 ans de formation, 10 à 12 semaines de stage entre le 1er août et le 30 octobre</li>\n									<li>après 3 ans de formation,...</li>\n								</ul>\n								<li><strong>Le stage de Master : 6 mois de stage à compter du ???</strong></li>\n							</ul>\n\n							<p><i>Si vous êtes intéressés, contactez-nous au travers de la page <a href="contact.html">contact.</a></i></p>\n					'),
(11, 1, 13, 'text-block', 'Participer à nos enseignements', '<p>Vous pouvez également participer à nos enseignements. Les interventions de professionnels permettent de <strong>renforcer l’ancrage professionnel</strong> des contenus de formation et contribuent au rapprochement IUT-Entreprises.</p>\n \n<p>Ces interventions par des vacataires issus du monde professionnel (chefs d’entreprise, techniciens, cadres…), <strong>très appréciées par les étudiants</strong>, représentent une part importante des heures d’enseignement dans nos formations.</p>\n \n<p>L’IUT est <strong>toujours en recherche de vacataires professionnels</strong>, si vous souhaitez vous impliquer dans une de nos formations, prenez contact avec nous au travers de la page <a href="contact.html">contact</a>.</p>\n				'),
(13, 1, 3, 'text-block', 'Les formations', '<p>Le Vietnam connaît actuellement un développement économique considérable dans tous les domaines professionnels.\n                        Dans ce contexte, les technologies de l’information constituent un levier essentiel dans la modernisation de l’économie vietnamienne.\n                        Les entreprises installées au Vietnam, qu’elles soient publiques ou privées, vietnamiennes ou internationales, ressentent aujourd’hui un besoin crucial d’informaticiens de haut niveau. </p>\n                    <p>Le PUF Ho Chi Minh a pour principale activité d''animer des formations universitaires françaises délocalisées au Vietnam. Toutes les formations suivent les principes suivants :</p>\n                    <ul>\n                        <li>Elles sont <strong>identiques</strong> à des formations dispensées en France, et lient la théorie et la pratique.</li>\n                        <li>Elles sont assurées à au moins 50% par des enseignants des universités françaises partenaires, ou par des professionnels qui leurs sont associées.</li>\n                        <li>Elles mènent à un diplôme français du schéma LMD, <strong>reconnu partout en Europe</strong> et par équivalence dans de nombreux pays.</li>\n                    </ul>'),
(14, 1, 6, 'text-block', NULL, ' <p>\n                        La licence d''informatique s''adresse aux étudiants intéressés par l''informatique sous tous ses aspects. \n                        Elle vise à en donner les bases tant théoriques que pratiques. </p>\n                    <p>\n                        Les diplômés de cette licence seront immédiatement opérationnels dans des métiers nombreux et variés ou bien pourront continuer des études en master d''informatique.</p>\n                    <p>\n                        <strong>Trois diplômes</strong> peuvent être obtenus au sein du cycle Licence d''informatique :\n                        un DUT Informatique, une Licence Professionnelle Systèmes Informatiques et Logiciels, une Licence de Sciences et Technologies mention Informatique.\n                    </p>'),
(15, 1, 9, 'list', 'Pourquoi a Ho-Chi-Minh plutôt qu''en france ?', 'PROGRAMME\n<--separator-->\n<p>\n                        <strong>Introduction</strong> Septembre -> Décembre :<br>\n                        Anglais, Français, Programmation Java<br><br>\n                        <strong>Fondamentaux</strong> Décembre -> Août : <br>\n                        Projets<br>\n                        Approche Orientée Objet<br>\n                        Architecture Réseau<br>\n                        Langage théorique<br>\n                        Compilation<br>\n                        Algorithmique avancée<br>\n                        Réseau et Télécommunication<br>\n                        Internet nouvelle génération<br>\n                        Mobile et Conception de sites web adaptatifs\n                    </p>'),
(16, 1, 9, 'text-block', NULL, '<p> Pourquoi à Ho Chi Minh plutôt qu''en France ? <br><br>\n                    <strong>Cela convient mieux à votre formation:</strong><br>\n                    pour étudier en France il faut parler le français couramment</p>\n                <p> <strong>Cela convient mieux à votre avenir</strong><br>\n                    Vous planifiez une carrière professionnelle immédiatement après le diplôme</p>\n                <p> <strong>C''est moins cher</strong><br>\n                    Les dépenses d''un étudiant en France tout compris : 1000 $ par mois</p> '),
(17, 1, 12, 'text-block', NULL, '<p> Le Master 2 est une année de spécialisation. Il est possible de se spécialiser en Réseaux ou en Ingénieur Logiciel.</p>\n                <p><strong> L''option Réseaux </strong>peut déboucher sur une carrière d''administrateur réseaux, par exemple.</p>\n                <p><strong>L''option Ingénieur Logiciel, </strong>comme son nom l''indique, mène à un statut d''ingénieur.<br>\n                C''est aussi une spécialisation possible pour devinir chef de projet, consultant ...</p>\n      '),
(18, 1, 12, 'list', NULL, 'SPÉCIALISATION RÉSEAUX\n<--separator-->\n <p>Insertion professionnelle<br>\n                    Multimédia et Qualité de service<br>\n                    Management de Système d''Information<br>\n                    Voix sur IP<br>\n                    Routeurs et routage<br>\n                    Contrôle et trafic de réseaux<br>\n                    Transferts multimédia sur réseaux IP<br>\n                    Grands réseaux graphiques<br>\n                    Catégories de transporteurs réseaux<br>\n                    Communication digitale<br>\n                    Sécurité des réseaux avancés<br>\n                    Régulation d''internet<br>\n                    Distribution et résistance aux attaques<br>\n                    <strong>Stage :</strong> 4 à 6 mois en Janvier ou Février<br>\n                    Stage (mémoire de thèse) de recherche et stage industriel</p>\n\n<----separator---->\nSPÉCIALISATION INGÉNIEUR LOGICIEL\n<--separator-->\n <p>Architecture logicielle<br>\n                    Management de projet<br>\n                    Conception formelle<br>\n                    Base de données avancée<br>\n                    Architecture logiciel adaptative et avancée<br>\n                    Projet de recherche et d''étude<br>\n                    Communication<br>\n                    Régulation d''internet<br>\n                    <strong>Stage :</strong> 4 à 6 mois en Janvier ou Février<br>\n                    Stage (mémoire de thèse) de recherche et stage industriel</p>\n\n<----separator---->\nPERSPECTIVES APRES LE DIPLÔME\n<--separator-->\n\n<p>Une carrière professionnelle dans les réseaux ou ingénieur logiciels \n                    (comme administrateur réseaux, consultant informatique, chef de projet ou ingénieur logiciel...)<br>\n                    Carrière de chercheur après un doctorat</p> \n\n<----separator---->\nLES DIPLÔMéS\n<--separator-->\n\n<p>59 étudiants ingénieurs logiciels diplomés de 2008 à 2013<br>\n                    Chefs de projet, chefs d''équipe, directeurs : 30%<br>\n                    Professeurs, enseignants-chercheurs, chercheurs: 40%<br>\n                    Etudes en doctorat : 5%<br>\n                    Autre : 5%</p>'),
(19, 1, 6, 'list', NULL, 'POURQUOI UNE LICENCE INFORMATIQUE\n<--separator-->\n <ul>\n                                    <li>L’informatique inonde notre quotidien : mails, réseaux sociaux, jeux vidéo, bureautique, NFC, systèmes embarqués, etc...</li>\n                                    <li>La licence donne les bases d’une compétence informatique générale : conception, développement, innovation... </li>\n                                </ul>\n\n<----separator---->\nPOURQUOI AU PUF\n<--separator-->\n<ul>\n                            <li>Vous obtenez simultanément deux diplômes en informatique : licence de l’Université Pierre et Marie Curie (Paris VI Sorbonne) et DUT de l’université de Bordeaux.</li>\n                            <li>Internationalement reconnus, théoriques et pratiques, ces diplômes vous offrent une insertion professionnelle immédiate. </li>\n                            <li>Si vous préférez, vous pouvez poursuivre en Master en France, en Europe, aux USA ou au Vietnam.</li>\n                        </ul>\n<----separator---->\nORGANISATION DES FORMATIONS\n<--separator-->\n <p><strong>Condition d’accès :</strong> être titulaire du baccalauréat. </p>\n                        <p><strong>Durée et rythme de la formation : </strong>3 ans répartis en 6 semestres.</p>\n                        <p><strong>Les matières enseignées : </strong>algorithmique, programmation et programmation objets, conception objets, bases de données, systèmes d’exploitation, réseaux, mathématiques, management, expression et communication en français et en anglais, ...\n                        <p><strong>Projets et stages : </strong>les 6 semestres de la formation permettent de réaliser de nombreux projets et 2 stages (semestres 4 et 6).</p>\n                  \n                   \n    '),
(20, 1, 7, 'text-block', NULL, ' <p>\n                    Les deux premières années de cours indifférenciées permettent <br>\n                    aux étudiants d’obtenir un premier diplôme universitaire professionnel : <br>\n                    le DUT (Diplôme Universitaire de Technologie) d’informatique.    <br><br>\n                    <strong>Programme : </strong><br>\n                    Algorithmique et Programmation - Théorie et Pratique <br>\n                    Architecture, Systèmes et Réseaux<br>\n                    Outils et Méthodes du Génie Logiciel<br>\n                    Mathématiques<br>\n                    Economie et Gestion des organisations<br>\n                    Expression et Communication<br>\n                    Projets et Stage Professionnel<br><br>\n\n                    Le Diplôme (DUT) est délivré par l''université de Bordeaux.\n                </p>'),
(21, 1, 8, 'text-block', NULL, '<p> La troisième année présente deux parcours de référence :</p>\n                <p><strong>La Licence Professionnelle</strong> apporte un solide capital de connaissances approprié à tout métier de \n                    l''informatique et permettant aux étudiants de prendre immédiatement des responsabilités informatiques \n                    dans les entreprises.</p>\n                <p><strong>La Licence de Sciences et Technologies mention Informatique</strong> vise à faire acquérir des bases théoriques et \n                    pratiques permettant de poursuivre des études dans une spécialité de Master d''Informatique de Ho Chi Minh \n                    Ville (Spécialité Réseaux ou Spécialité Génie Logiciel) ou de n’importe quelle université française ou \n                    européenne.</p>'),
(22, 1, 8, 'list', NULL, 'PROGRAMME TRONC COMMUN\n<--separator-->\n<p><strong>Compétences managériales :</strong><br>\n                        Management opérationel<br>\n                        Ressources Humaines, Communication et Management\n                    </p>\n                    <p><strong>Compétences techniques :</strong><br>\n                        Gestion des Systèmes d''Information<br>\n                        Méthodes avancées d''ingénierie logicielle<br>\n                        Méthodes avancées de développement\n                    </p>\n                    <p><strong>Compétences scientifiques et techniques :</strong><br>\n                        Systèmes, Réseaux et Base de données<br>\n                        Algorithmique et Mathématiques pour l''informatique<br>\n                        Programmation et Développement\n                    </p>\n                    <p><strong>Projet</strong></p>\n\n<----separator---->\nLICENCE PROFESSIONNELLE\n<--separator-->\n                            <p>    \n                        <strong>Stage en entreprise</strong><br>\n                        Diplôme délivré par l''Université de Bordeaux\n                    </p> \n\n<----separator---->\nLICENCE INFORMATIQUE ET TECHNOLOGIES \n<--separator-->\n<p>    \n                        <strong>Connaissance de l''entreprise</strong><br>\n                        Diplôme délivré par l''Université de Paris 6\n                    </p> '),
(23, 1, 20, 'text-block', 'Navigation par tabulation', '<p>Il est possible d''utiliser la touche <strong>Tab</strong> (Tabulation) de votre clavier pour cheminer de lien en lien tout au long de la page. Pour cela :</p>\r\n                    <ul>\r\n                        <li>Appuyer sur la touche <strong>Tab</strong> et répéter jusqu''à sélectionner le lien désiré.</li>\r\n                        <li>Valider par <strong>Entrée</strong>.</li>\r\n                        <li>Pour revenir en arrière parmi les liens, utiliser la combinaison de touches <strong>Maj</strong> (Majuscules) + <strong>Tab</strong> (Tabulation).</li>\r\n                    </ul>\r\n                    <p> Attiention sous Safari, par défaut, la navigation par tabulation n''est pas activée. \r\n                        Pour l''activer : dans le menu des réglages aller dans ''Préférences'', puis, dans ''Avancées'', cocher la case permettant de naviguer par tabulation.</p>\r\n\r\n                    <br/>\r\n                    \r\n                    <p>Voici la liste des raccourcis clavier utilisables sur ce site : </p>\r\n                    <ul>\r\n                        <li>Touche <strong>0</strong> - Politique d''accessibilité du site</li>\r\n                        <li>Touche <strong>1</strong> - Page d''Accueil du site</li>\r\n                        <li>Touche <strong>2</strong> - Page de présentation des Formations proposées</li>\r\n                        <li>Touche <strong>3</strong> - Page d''espace aux Entreprises</li>\r\n                        <li>Touche <strong>4</strong> - Infos Pratiques</li>\r\n                        <li>Touche <strong>5</strong> - Contact</li>\r\n                        <li>Touche <strong>6</strong> - Formulaire de Connexion</li>\r\n                        <li>Touche <strong>7</strong> - Crédits et Mentions légales</li>\r\n                        <li>Touche <strong>8</strong> - Plan du site</li>\r\n                        <li>Touche <strong>f</strong> - Page Facebook</li>\r\n                        <li>Touche <strong>t</strong> - Compte Twitter</li>\r\n                        <li>Touche <strong>r</strong> - Barre de Recherches</li>\r\n                    </ul>\r\n\r\n                    <br/>\r\n                    \r\n                    <p>Les combinaisons de touches pour activer ces raccourcis sont différentes selon le navigateur et le système d''exploitation utilisés. Les procédures à suivre pour activer les raccourcis claviers dans les principales configurations sont les suivantes :</p>\r\n                    <ul>\r\n                        <li>Mozilla Firefox, Google Chrome, Safari sur Windows : touches <strong>Maj</strong> + Alt + [Raccourci clavier] (attention : utiliser les touches numériques du clavier et non celles du pavé numérique).</li>\r\n                        <li>Internet Explorer sur Windows : touches Alt + Maj + [Raccourci clavier], puis touche <strong>Entrée</strong> (attention : utiliser les touches numériques du clavier et non celles du pavé numérique). AvecWindows Vista, utiliser la combinaison de touches Alt+ <strong>Maj</strong> + [Raccourci clavier].</li>\r\n                        <li>Opera 7 sur Windows, Macintosh et Linux : Echap + <strong>Maj</strong> + [Raccourci clavier]</li>\r\n                        <li>Safari sur Macintosh : <strong>Ctrl</strong> + <strong>Maj</strong> + [Raccourci clavier]</li>\r\n                        <li>Mozilla et Netscape sur Macintosh : <strong>Ctrl</strong> + [Raccourci clavier]</li>\r\n                        <li>Galeon, Mozilla FireFox sur Linux : Alt + [Raccourci clavier]</li>\r\n                    </ul>\r\n\r\n                    <br/>'),
(24, 1, 20, 'text-block', 'Ajustement de la taille du texte', '<p>Les textes du site ont une taille de police relative : ils peuvent être agrandis par l''utilisateur. Pour agrandir ou diminuer la taille d''affichage des textes, il est possible d''utiliser l''une des méthodes suivantes :</p>\r\n                    <ul>\r\n                        <li>Fonctionnalité "Taille du texte" présente dans le menu "Affichage" du navigateur.</li>\r\n                        <li>Combinaison touche <strong><strong>Ctrl</strong></strong> + molette de la souris.</li>\r\n                        <li>Combinaison touche <strong>Ctrl</strong> + touches "+" et "-" du pavé numérique.</li>\r\n                    </ul>'),
(25, 1, 20, 'text-block', 'Respect des standards web', '<p>Ce site utilise les dernières technologies du <a href="#"><strong>W3C</strong></a>. Il offre un meilleur rendu avec les navigateurs de dernière génération, mais reste néanmoins consultable avec les navigateurs plus anciens.</p>'),
(26, 1, 21, 'text-block', 'Mentions légales', '<p>Le présent site est édité par Mme Chritine UNY professeur à l''IUT de l''université de Bordeaux. Néanmoins ce site est la propriété du département Informatique du PUF (Pôle Universitaire Français) d''Ho Chi Minh.</p> \r\n\r\n                <p>Ce site à été développer, dans le cadre d''un projet de formation, par <strong>IACHI Dimitri Fabien</strong>, <strong>ETCHEVERRY Laetitia</strong>, <strong>GARCIA Johan</strong>, <strong>PARPET Charles-Eddy</strong> et <strong>ROBIN Benoit</strong>, étudiants en Licence Professionnelle DAWIN à l''IUT de l''université de Bordeaux.</p>\r\n\r\n                <p>Tous les contenus du site sont couverts par la <a href="http://creativecommons.fr/licences/" title="Lien menant au site sur la licence creative commons">licence creative commons</a> qui permet :</p>\r\n                <ul>\r\n                    <li> de partager et faciliter l’utilisation de leur création par d’autres </li>\r\n                    <li> d''autoriser gratuitement la reproduction et la diffusion (sous certaines conditions) </li>\r\n                    <li> d''accorder plus de droits aux utilisateurs en complétant le droit d’auteur qui s’applique par défaut </li>\r\n                    <li> de faire évoluer une oeuvre et enrichir le patrimoine commun (les biens communs ou Commons) </li>\r\n                    <li> d''économiser les coûts de transaction </li>\r\n                    <li> de légaliser le peer to peer de leurs œuvres </li>\r\n                </ul>\r\n\r\n                <p></p>\r\n\r\n                <p><strong>U-HoChiMinh</strong>, ainsi que sa représentation graphique, et <a href="http://www.u-hochiminh.com/">www.u-hochiminh.com</a> sont la propriété du PUF d''Ho Chi Minh.</p>\r\n\r\n                <p>Les codes sources utilisés pour la création de ce site internet et l’architecture du programme sont couverts par la législation française et internationale sur le droit d’auteur et la propriété intellectuelle. Tous les droits de reproduction sont réservés. Néanmoins toute reproduction du site, même partielle, est interdite sauf autorisation expresse des responsables de ce site et de ces autheurs. En cas de violation de ces dispositions la personne, morale ou physique, se soumet aux sanctions pénales et civiles prévues par la loi française.</p>\r\n\r\n                <p>Conformément à la réglementation en vigueur depuis le 10/07/2006, la déclaration auprès de la CNIL, d’un site Internet non marchand et/ou institutionnel et/ou ne récoltant pas des données à risque, n’est pas obligatoire. Néanmoins, le PUF d''Ho Chi Minh s’engage à respecter la vie privée de ses internautes conforment à la législation française sur la protection de la vie privée et des libertés individuelles (loi n° 78-17 du 6 janvier 1978) .</p>\r\n\r\n                <p>Ce site ne collecte donc aucune information personnel. Toute fois en application de l’article 34 de la loi informatique et libertés du 1er août 2000, toute personne ayant fait l’objet d’une collecte d’information dispose d’un droit d’accès, de rectification et d’opposition aux données personnelles la concernant soit par courrier (15, rue Naudet CS 10207 33175 Gradignan Cedex, France) ou par mail à l’adresse <a href="mailto:contact@u-HoChiMinh.com">contact@u-hochiminh.com</a>.</p>'),
(27, 1, 21, 'element-block', 'Parties prenantes', '<div class="element">\n                        <div class="content-title"><span>Propriétaire du site</span></div>\n                        <div class="content-info">Département Informatique du Pôle Universitaire Français<br/>Quartier 6, Linh Trung Ward,<br/>Thu Duc District,<br/>Ho Chi Min-Ville, Vietnam<br/><br/>Téléphone: <a href="tel:+0837242169">(08) 37 242 169</a><br/>Fax: <a href="tel:+0837242169">(08) 37 242 166</a></div>\n                    </div>\n                    <div class="element">\n                        <div class="content-title"><span>Hébergeur</span></div>\n                        <div class="content-info">I.U.T. Bordeaux 1<br/>15 Rue de Naudet<br/>33175 Gradignan</div>\n                    </div>\n                    <div class="element">\n                        <div class="content-title"><span>Conception du site </span></div>\n                        <div class="content-info">IACHI Dimitri<br/>ETCHEVERRY Laetitia<br/>GARCIA Johan<br/>PARPET Charles-Eddy<br/>ROBIN Benoit</div>\n                    </div>');

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `_id` int(8) NOT NULL AUTO_INCREMENT,
  `_langue` int(2) NOT NULL,
  `_title` varchar(60) NOT NULL,
  `_extract` text NOT NULL,
  `_content` longtext NOT NULL,
  `_imageThumb` text,
  `_imageOriginal` text,
  `_author` int(4) NOT NULL,
  `_date` datetime NOT NULL,
  PRIMARY KEY (`_id`,`_langue`),
  KEY `fk_news_langue1_idx` (`_langue`),
  KEY `fk_news_user1_idx` (`_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `news`
--

INSERT INTO `news` (`_id`, `_langue`, `_title`, `_extract`, `_content`, `_imageThumb`, `_imageOriginal`, `_author`, `_date`) VALUES
(2, 1, 'Semaine du livre', '<p>Praesent commodo cursus magna, vel <strong>scelerisque nisl consectetur et</strong> mecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum.</p>\n                                    <p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. <strong>Integer posuere erat</strong> a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur <strong>est at lobortis</strong>. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>', '<p>Praesent commodo cursus magna, vel <strong>scelerisque nisl consectetur et</strong> mecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum.</p>\n                                    <p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. <strong>Integer posuere erat</strong> a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur <strong>est at lobortis</strong>. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\n                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec volutpat ligula, consequat egestas orci. Maecenas vehicula nec dui sed ullamcorper. Proin vulputate lorem vitae mauris congue, nec egestas lectus imperdiet. Curabitur lobortis erat augue, quis egestas diam fermentum sed. Suspendisse sed iaculis risus. <strong>Suspendisse ornare tincidunt</strong> efficitur. In hac habitasse platea dictumst. Duis facilisis mauris ac velit rhoncus, nec sagittis sem condimentum.</p>\n                                    <p>Sed semper mi quis eros pellentesque egestas. Donec a nisl commodo, mollis mi id, mattis enim. Nulla posuere ex in sem luctus ultricies. In at odio dolor. Donec vel ex a nibh eleifend efficitur. Sed pulvinar dictum dui. Phasellus et <strong>viverra elit</strong>. Nulla congue libero vel congue blandit. Morbi diam lacus, scelerisque at urna et, hendrerit aliquam sapien. In diam odio, dapibus ac ante ut, consectetur lobortis elit. Donec ac sem suscipit, interdum massa non, commodo magna. Integer sed ipsum in lectus fermentum ultrices. Fusce quis dolor sed lectus cursus vestibulum sit amet vitae leo. Quisque scelerisque velit non lorem eleifend porta. Vestibulum eu luctus lectus. Cras euismod accumsan vulputate.</p>\n                                    <p>Aenean ut urna a massa scelerisque tempor. Nullam eleifend tristique elit, sed varius ex efficitur ac. Vestibulum ac velit ac nisl porttitor porttitor. Aenean in dictum justo, at viverra ligula. Vivamus <strong>maximus faucibus eros</strong> sit amet volutpat. Nullam ornare auctor gravida. Donec urna felis, rutrum nec suscipit sed, dignissim quis orci. Morbi malesuada tempor rutrum. Aenean et turpis libero.</p>\n                                    <p>Nulla mauris magna, pellentesque sed diam id, dictum accumsan nisi. Praesent vitae malesuada ante. Quisque bibendum lobortis erat, in tincidunt odio molestie quis. Sed porta euismod tortor, non condimentum sem. Integer eu fermentum eros, eu vestibulum nibh. Donec vehicula, massa ut lacinia pharetra, leo tellus blandit risus, eget volutpat lacus dui sit amet metus. Etiam sit amet tellus risus. Suspendisse mauris dui, molestie vitae metus non, dapibus placerat nunc.</p>', NULL, NULL, 1, '2014-11-25 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `_id` int(2) NOT NULL AUTO_INCREMENT,
  `_language` int(2) NOT NULL,
  `_name` varchar(50) NOT NULL,
  `_title` varchar(45) NOT NULL,
  `_description` varchar(200) NOT NULL,
  `_keywords` varchar(250) NOT NULL,
  `_date` datetime NOT NULL,
  `_dependsOn` int(2) NOT NULL,
  PRIMARY KEY (`_id`,`_language`),
  KEY `fk_page_langue_idx` (`_language`),
  KEY `fk_page_page` (`_dependsOn`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `page`
--

INSERT INTO `page` (`_id`, `_language`, `_name`, `_title`, `_description`, `_keywords`, `_date`, `_dependsOn`) VALUES
(1, 1, 'langue', '', 'Le pôle universitaire français (PUF) de Ho Chi Minh spécialité INFO qui prépare 5 formations au titre d''expert en informatique et systèmes d''information arrive bientôt sur vos écrans.', '', '2014-11-25 00:00:00', 0),
(2, 1, 'home', 'Accueil', 'Le département Informatique du Pôle Universitaire Français (PUF) de Ho Chi Minh Ville (HCMV) propose des formations de la Licence 1 au Master 2.', 'accueil, site', '2014-11-25 21:41:57', 0),
(3, 1, 'educations', 'Formations', 'Découvrez toutes nos formations universitaires aux standards internationaux en matière d''enseignement.', 'Formations, Licences, Masters, Réseaux, DUT, Sciences, Systèmes, Informatique, Logiciels, LMD, reseaux', '2014-11-25 21:41:57', 0),
(6, 1, 'education/l1', 'L1', 'Découvrez notre licence universitaire de niveau 1.', 'Formation, Licence, DUT, conditions, accès, acces', '2014-11-25 00:00:00', 0),
(7, 1, 'education/l2', 'L2', 'Découvrez notre licence universitaire de niveau 2.', 'Formation, Licence,DUT, programme', '2014-11-25 00:00:00', 0),
(8, 1, 'education/l3', 'L3', 'Découvrez notre licence universitaire de niveau 3.', 'Formation, Licence, Sciences, Systèmes, Informatique, Logiciels, stage, systemes, programme', '2014-11-25 00:00:00', 0),
(9, 1, 'education/m1', 'M1', 'Découvrez notre master universitaire de niveau 1.', 'Formation, Master, Programme', '2014-11-25 00:00:00', 0),
(12, 1, 'education/m2', 'M2', 'Découvrez notre master universitaire de niveau 2.', 'Formation, Master, Programme, stage, réseaux, ingénieur, logiciel, ingenieur', '2014-11-25 00:00:00', 0),
(13, 1, 'entreprises', 'Entreprises', 'Nous formons des professionnels et à ce titre, tous les acteurs de l''économie ont vocation à être nos partenaires.', 'entreprise, stage, entreprises, stages, enseigner', '2014-11-25 00:00:00', 0),
(14, 1, 'news', 'InfosPratiques', 'Vous trouverez ici toutes les informations pratiques à la vie en tant qu''étudiants (calendrier universitaire, accès aux restaurant U, liens utiles ...)', 'calendrier, restaurant', '2014-11-25 00:00:00', 0),
(15, 1, 'contact', 'Contact', 'Vous souhaitez nous contacter pour avoir des informations complémentaires sur nos formations ou toute autre chose ? N''hésitez pas.', 'contact, contacts, téléphone, téléphones, adresse, adresses, mail, e-mail, gmap, accès, google, map, formulaire, telephone, acces', '2014-11-25 00:00:00', 0),
(16, 1, 'user/connexion', 'Connexion', 'Cette page est réserver au personnel encadrant du PUF de Ho Chi Minh.', 'connexion, connexions', '2014-11-25 00:00:00', 0),
(17, 1, 'missions', 'Missions', 'Découvrez les missions organiser au sein du département informatique du PUF par nos professeurs Français.', 'mission, missions', '2014-11-25 00:00:00', 0),
(20, 1, 'accessibility', 'Accessibilité', 'Nous nous attachons à respecter les critères d''accessibilité sur notre site internet, conformément à la législation en vigueur.', 'accessibilité, tabulation, raccourcis, accessibilite', '2014-11-25 00:00:00', 0),
(21, 1, 'legal-notice', 'Mentions Légales', 'Vous trouverez ici toutes les informations concernant la création de ce site ainsi que son contexte.', 'lois, mentions, légales, legales', '2014-11-25 00:00:00', 0),
(23, 1, 'sitemap', 'Plan du site', 'Le plan du site ci-dessous vous permettra de retrouver son architecture et d''y naviguer de façon intuitive.', 'plan, site, acces, accès', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `_id` int(3) NOT NULL AUTO_INCREMENT,
  `_key` varchar(200) NOT NULL,
  `_value` text NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `key` (`_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`_id`, `_key`, `_value`) VALUES
(1, 'link-facebook', 'https://www.facebook.com/uhochiminh'),
(2, 'link-twitter', 'https://twitter.com/u_hochiminh'),
(3, 'authors', 'Designed & Developed by IUT Bordeaux 1 - Groupe 3 '),
(4, 'copyright', 'Copyright © 2014 - All Right Reserved - u-HoChiMinh.com'),
(5, 'domain', 'u-hochiminh.com'),
(6, 'twitter_id', '@u_hochiminh'),
(7, 'analytics', '<script>\r\n        (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){\r\n        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n        })(window,document,''script'',''//www.google-analytics.com/analytics.js'',''ga'');\r\n        ga(''create'', ''UA-55710805-1'', ''auto'');\r\n        ga(''send'', ''pageview'');\r\n    </script>'),
(8, 'adress', 'Pôle Universitaire Français de l''Université Nationale<br/>\r\nQuartier 6, Linh Trung Ward, Thu Duc District, Ho Chi Min-Ville, Vietnam'),
(9, 'phone', '+84 (0)837 242 169 '),
(10, 'fax', '+84 (0)837 242 166'),
(11, 'mail', 'contact@u-hochiminh.com');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `_id` int(4) NOT NULL AUTO_INCREMENT,
  `_name` varchar(50) NOT NULL,
  `_surname` varchar(25) NOT NULL,
  `_email` varchar(110) NOT NULL,
  `_password` varchar(200) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `_level` varchar(20) NOT NULL,
  `_ipLastConnection` varchar(40) DEFAULT NULL,
  `_dateLastConnection` datetime DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `email` (`_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`_id`, `_name`, `_surname`, `_email`, `_password`, `remember_token`, `_level`, `_ipLastConnection`, `_dateLastConnection`) VALUES
(1, 'UNY', 'Christine', 'christine.uny@u-hochiminh.com', '098f6bcd4621d373cade4e832627b4f6', 'P4bu6kFyx4ZdV3Tw2UNmEyhI0w7O4jl8WuhaQT6RFB8j5PTIpOPMWmKDnAIS', 'admin', '::1', '2015-01-08 23:23:38');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `missions`
--
ALTER TABLE `missions`
  ADD CONSTRAINT `fk_missions_user1` FOREIGN KEY (`_author`) REFERENCES `user` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `fk_modules_page1` FOREIGN KEY (`_page`, `_language`) REFERENCES `page` (`_id`, `_language`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `module_ibfk_2` FOREIGN KEY (`_language`) REFERENCES `language` (`_id`),
  ADD CONSTRAINT `module_ibfk_3` FOREIGN KEY (`_page`) REFERENCES `page` (`_id`);

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `fk_news_langue1` FOREIGN KEY (`_langue`) REFERENCES `language` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_news_user1` FOREIGN KEY (`_author`) REFERENCES `user` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `fk_page_langue` FOREIGN KEY (`_language`) REFERENCES `language` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
