<?php
/* @Author: IACHI Dimitri - diachi, ETCHEVERRY Laetitia - letchev4 */
require("_all.php");

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php"); 
?>

	<div id="init" class="script">
		<table valign="middle" align="center" border="0" height="100%" width="100%">
			<tbody>
				<tr height="100%">
					<td align="center" height="100%" valign="middle" width="100%">
						<div class="script center" style="display:none;">
							<h1><img src="images/logo_teasing-init.png" alt="<?php echo $GLOBALS["site"]["nom"]; ?>" title="<?php echo $GLOBALS["site"]["nom"]; ?>" /></h1>

							<div class="status"><div>/ CHOOSE YOUR LANGUAGE /;/ CHOISIR VOTRE LANGUE /</div></a></div>

						<div class="language">
							<a href="Accueil.html"><img src="images/drapeau-english.png" alt="English Language" title="English Language" /></a>
							<a href="Accueil.html"><img src="images/drapeau-french.png" alt="Langue Française" title="Langue Française" /></a>
						</div>

						<div class="copyright">Designed & Developed by <strong>IUT Bordeaux 1 - Groupe 3</strong>
							<br/>Copyright  &copy; 2014 - All Right Reserved - u-HoChiMinh.com</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>


<?php
pDoctype("end");

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
