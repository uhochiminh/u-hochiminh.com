<?php  	
/* @Author: IACHI Dimitri - diachi */
require("_all.php"); 


ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>
<script src="scripts/jquery.paulund_modal_box.js"></script>

    <div class="script-direct">
        <?php pHeader(); ?>
        <div id="accueil" class="content">
            <div class="situation no-display">
                <h2>Accueil</h2>
                <h3>Le département Informatique du Pôle Universitaire Français (PUF) de Ho Chi Minh Ville (HCMV) propose des formations de la Licence 1 au Master 2.</h3>
            </div>
			
			<div class="slider-fixed no-display">
				<img src="images/slider-01.png" alt="Photo d'étudiants" />
			</div>
			
			<div class="slider">
				<div id="slide1" class="slide">
					<img src="images/slider-01.png" alt="Photo d'étudiants" />
				</div>
                <div id="slide2" class="slide">
                    <img src="images/slider-02.png" alt="Photo d'étudiants" />
                </div>
                <div id="slide3" class="slide">
                    <img src="images/slider-03.png" alt="Photo d'étudiants" />
                </div>
				<div id="prec">
					<img src="images/slider-fleche-gauche.png" class="fleche-gauche" alt="Précédent" />
				</div>
				<div id="suiv">
					<img src="images/slider-fleche-droite.png" class="fleche-droite" alt="Suivant" />
				</div>
				<div class="clear"></div>
			</div>
			<div class="editionmodule"> 
				<button id="ajouter" class="paulund_modal" >Ajouter un module</button>
				<button id="supprimer">Supprimer un module</button>
			</div>
            <div class="content-text">
                <section class="content-text-presentation">
                    <h4 class="content-text-title"><span>Présentation</span></h4>
                    <div class="content-text-text text alinea justify">
                        <p>Le département informatique du Pôle Universitaire Français de Ho Chi Minh vous présente ici les formations qu'il propose.
                            Nous espérons que vous trouverez les informations que vous cherchez, quelque soit votre profil : entreprise, étudiant, enseignant, futur étudiant, ...</p>
                        <p>Les formations proposées sont sur divers domaines de l'informatique et peuvent amener vers plusieurs niveaux (de la Licence 1 au Master 2).
                        <br>Nos programmes sont fournis par de grandes universités internationales et nos points forts sont des chercheurs de classe mondiale et des formateurs professionnels expérimentés.
                        <br>Nos diplômes sont accrédités selon les plus hauts standards académiques et offrent crédibilité et reconnaissance mondiale.</p>
                        <p>Profitez de nos liens étroits avec les plus grandes sociétés locales et internationales pour des occasions d'apprentissage supplémentaires.
                        <br>Notre accent sur les aspects techniques, l'apprentissage basé sur les compétences vous donne la possibilité de poursuivre une carrière dans la recherche, les universités ou l'industrie.</p>
                    </div>

                    <div class="content-text-list-points text">
                        <h5 class="content-text-list-points-title click"><i>+</i> Pourquoi étudier au PUF</h5>
                        <div class="content-text-list-points-content no-display">
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>1</i></div>
                                <p><strong>Des universités européennes de premier plan :</strong><br>
                                   Nos formations sont délivrées par des enseignants et chercheurs issus d’Universités de niveau international.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>2</i></div>
                                <p><strong>Des diplômes internationalement reconnus :</strong><br>
                                Nos diplômes sont homologués et vous permettront d’intégrer les universités européennes et nord américaines.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>3</i></div>
                                <p><strong>Des partenaires industriels majeurs :</strong><br>
                                Nos liens étroits avec le monde industriel local et international vous offrent des opportunités de stage et d’apprentissage.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i>4</i></div>
                                <p><strong>Des perspectives de carrières : </strong><br>
                                Notre enseignement vous permet d’acquérir les compétences techniques et théoriques qui vous autorisent indifféremment une carrière orientée vers la recherche, l’enseignement ou l’entreprise.</p>
                            </div>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> Pour quels diplômes</h5>
                        <div class="content-text-list-points-content alinea no-display">
                            <p><strong>Licence en informatique :</strong></p>
                            <p>Notre licence vous apporte en 3 ans les compétences et connaissances nécessaires pour participer à la conception, au développement et à la maintenance de systèmes informatiques :
                                - Conception, développement, validation, suivi et maintenance de logiciels,
                                - Installation, administration et maintenance de systèmes et réseaux informatiques.</p>

                            <p><strong>Master Réseaux :</strong></p>
                            <p>Notre Master  en services Internet et Mobiles vous forme en 2 ans en concentrant ses enseignements sur la conception et le développement de systèmes informatiques mobiles et de dimension internationale, sur la conception d'une infrastructure réseau pouvant soutenir ces systèmes informatiques, ainsi que sur la conception de systèmes évolutifs qui exploitent des architectures distribuées. </p>

                            <p><strong>Master Génie Logiciel :</strong></p>
                            <p>Notre Master en Genie Logiciel vous forme en 2 ans en concentrant ses enseignements sur les méthodes formelles, des méthodologies aguerries de management de projet, ainsi que des compétences pratiques, afin de former des experts en conception et développement de systèmes logiciels efficaces et sûrs. </p>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> Des formations de niveau international</h5>
                        <div class="content-text-list-points-content alinea no-display">
                            <p>Créé en 2006, le PUF délivre des formations universitaires en informatique en partenariat avec les meilleures universités françaises et européennes.
                            Nos Licence et Master vous offrent une formation technique de grande qualité couplée à une réelle expérience pratique.</p>
                            <p>Ces diplômes vous permettent d’acquérir des compétences techniques et managériales vous préparant à occuper des postes de décideurs dans le secteur des technologies de l’information et de la communication.<p>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> DES Opportunités de carrières</h5>
                        <div class="content-text-list-points-content alinea no-display">
                            <p>Le secteur des TIC souffre aujourd’hui d’une pénurie mondiale de personnels qualifiés. Nos diplômes sont donc porteurs sur le marché du travail et garantissent à nos diplômés des progressions de carrières rapides et des salaires élevés. Nos étudiants acquièrent des compétences de management essentielles aux leaders informatiques du monde du travail compétitif actuel. </p>
                        </div>
                        <h5 class="content-text-list-points-title click"><i>+</i> NOS POINTS FORTS</h5>
                        <div class="content-text-list-points-content no-display">
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-share-alt"></i></div>
                                <p>Le PUF entretient des liens étroits avec l’Europe au travers de partenariats industriels, de collaborations de recherche et d’échanges d’étudiants et d'enseignants.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-graduation-cap"></i></div>
                                <p>Les formations du PUF sont assurées par une équipe expérimentée d’enseignants et de professionnels du secteur des TIC.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-book"></i></div>
                                <p>Nos pratiques pédagogiques allient cours magistraux, travaux dirigés, travaux pratiques et travaux en « groupe projet » pour vous permettre de développer une
                                véritable expérience pratique et vous préparer au mieux à une insertion professionnelle optimale.</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-flag"></i></div>
                                <p>Avec une formation délivrée en français ou en anglais vous pourrez acquérir les compétences linguistiques indispensables à votre réussite en milieu
                                professionnel..</p>
                            </div>
                            <div class="content-text-list-points-content-point">
                                <div class="point-circle"><i class="fa fa-briefcase"></i></div>
                                <p>Vous bénéficierez enfin de nos liens étroits avec les entreprises nationales et internationales du secteur des TIC afin de préparer votre future carrière.</p>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section class="content-text-parteners">
                    <h4 class="content-text-title"><span>Partenaires</span></h4>
                    <ul class="content-text-text">
                        <li><a href="http://www.ambafrance-vn.org/Poles-Universitaires-Francais-et" target="_blank"><img src="images/partenaire-puf.png" alt="Logo du PUF"></a></li>
                        <li><a href="http://www.u-bordeaux.fr/" target="_blank"><img src="images/partenaire-ubordeaux.png" alt="Logo de l'université de Bordeaux"></a></li>
                        <li><a href="http://www.upmc.fr/" target="_blank"><img src="images/partenaire-upmc.png" alt="Logo de l'UPMC"></a></li>
                        <li><a href="http://www.vnuhcm.edu.vn/" target="_blank"><img src="images/partenaire-daihocquaocgia.png" alt="Logo de l'université nationale d'Ho Chi Minh"></a></li>
                    </ul>
                </section>
            </div>
        </div>

        <?php pFooter(); ?>
    </div>

<?php
pDoctype("end");
$result = 	ob_get_contents();
			ob_end_clean();
htmlCleaner::make($result);
?>
