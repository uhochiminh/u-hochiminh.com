/* Slider JS */
/* @Author: Charles-Eddy Parpet */

var slider = {
	nbSlide : 0,                                                                                           // le nombre de slide
	nbCurrent : 1,                                                                                         // le numero de slide courant
	elemCurrent : null,                                                                                    // element courant
	elem : null,
	timer : null,

	init : function(elem){                                                                                 // initialise l'élément en executant la fonction
			
			this.nbSlide = elem.find(".slide").length;                                                     // nombre de slides ici elem (la div id=slider)

			//création de la pagination
			elem.append('<div class="pagination"></div>');
			for(var i=1;i<=this.nbSlide;i++){ elem.find(".pagination").append('<i data-slider-position="'+i+'" class="fa fa-circle"></i>'); }

            elem.find(".pagination i").click(function(){slider.changeSlide($(this).attr("data-slider-position"));}) //pour chaque click sur la pagination on lance la fonction qui change le slide en mettant celui correspondant

			elem.find("#prec .fleche-gauche").click(function(){slider.prev();})	                           // si on clique sur le bouton précédent alors on revient au slide d'avant
			elem.find("#suiv .fleche-droite").click(function(){slider.next();})

			//init du slider 
			this.elem=elem;
			elem.find(".slide").hide();                                                                    // le slider commence par le dernier slide donc on le cache
			elem.find(".slide:first").show();                                                              // on montre le premier slide
			this.elemCurrent=elem.find(".slide:first");                                                    // on définit l'élément courant comme étant la première slide
			elem.find(".pagination i:first").addClass("active");                                           // la premiere slide est activé seule, donc on active la premiere pagination

			//création du timer
			slider.play();                                                                                 // lance la fonction qui crée le timer
			//stopper le timer quand on passe la souris sur le slider
			elem.mouseover(slider.stop);
			//le faire reprendre quand on ressort
			elem.mouseout(slider.play); 

	},

	changeSlide : function(num){                                                                           // num = le numero de la slide / fonction qui permet de changer de slide

		//quand on clique sur un des chiffres de la pagination faire :
		if(num == this.nbCurrent){return false;}                                                           // si on apelle la slide qui est déjà affiché, alors return false

		this.elemCurrent.fadeOut();                                                                        // cacher l'élément sur lequel on est 

		this.elem.find("#slide"+num).fadeIn();                                                             // afficher l'élément qu'on demande

		//gerer le style de la pagination avec les fonctions js permettant de rajotuer la class active 
		this.elem.find(".pagination i").removeClass("active");                                            // on supprime les classes pour ne pas qu'elles soient tout le temps "active"
		this.elem.find(".pagination i:eq("+(num-1)+")").addClass("active");                               // on rajoute la classe active a notre i qui correspond a numéro de l'image

		this.nbCurrent = num;                                                                             // le numero du slide devient donc le num du i (de la pagination)
		this.elemCurrent = this.elem.find("#slide"+num);                                                  // element courant est donc le slide "numéro du i"
	},

	next : function(){
		var num= parseInt(this.nbCurrent) + 1;                                                            //parseInt pour éviter tout problème de concaténation
		if(num>this.nbSlide){                                                                             // si le nombre en entrée est supérieur au nombre de slide, alors on revient au début c-a-d 1
			num=1;
		}
		this.changeSlide(num);                                                                            // on affiche le slide correspodant
	},

	prev : function(){
		var num=parseInt(this.nbCurrent) - 1;
		if(num<1){
			num=this.nbSlide;
		}
		this.changeSlide(num);
	},

	stop : function(){
		window.clearInterval(slider.timer);
	},

	play : function(){
		window.clearInterval(slider.timer);                                                              // dabord on enleve le timer, si il y en a un, pour éviter certains bugs graphiques
		slider.timer= window.setInterval("slider.next()",3500);                                          // les slides sont en auto-play 
	}


}
slider.init($(".slider"));

$( function() {
            $("h4 span").on("click", function(){
                var actual_value = $(this).text();
                //alert(actual_value);
                $(this).replaceWith("<textarea>");
                $("textarea").val(actual_value);

                if($("textarea").mouseout(function(){
                    var textarea = $("textarea").val();
                    $(this).replaceWith("<span>");
                    $("span").text(textarea);
                }));
            });
});

$(document).ready(function(){
	$('.paulund_modal').paulund_modal_box();
});