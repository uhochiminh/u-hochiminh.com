<?php  	
/* @Author: ROBIN Benoit -  */
require("_all.php"); 

ob_start();
pDoctype("start", array("Accueil"));
include("parts/no-script.php");
?>

<div class="script-direct">
    <?php pHeader(); 
        header('Status : 500 Internal Server Error');
        header('HTTP/1.0 500 Internal Server Error');
	?>	
	
    <div id="erreur" class="content">
        <div class="situation">
                <h2>Erreur 500</h2>
                <h3>Une erreur s'est produite durant le chargement de la page demandée.</h3>
        </div>
        <div class="position">
            <i class="fa fa-sitemap blue"></i>
            <a href="Accueil.html">Accueil</a>
            <i class="fa fa-angle-right"></i>
            Erreur 500
        </div>

        <div class="content-text">
            <section class="content-text-erreur">
                <div class="title">500</div>
                <div class="texte">Notre service est momentanément indisponible.</div>
            </section>
        </div>
    </div>

    <?php pFooter(); ?>
</div>

<?php
pDoctype("end"); 

$result = ob_get_contents();
ob_end_clean();
htmlCleaner::make($result);
?>
